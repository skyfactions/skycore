package fr.floxiik.skycore.utils;

import com.Zrips.CMI.CMI;
import it.unimi.dsi.fastutil.Hash;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StaffMode {

    private static HashMap<Player, String> inMode = new HashMap();
    private static HashMap<Player, Location> pos = new HashMap<>();
    private static HashMap<Player, Location> freezed = new HashMap<>();

    public static void enterStaffMode(Player p) {
        if (inMode.containsKey(p)) {
            p.sendMessage("§c§lModération§f│ §eVous venez de quitter le §aStaffMode §e!");
            p.getInventory().clear();
            p.chat("/vanish");
            try {
                p.getInventory().setContents(BukkitSerialization.itemStackArrayFromBase64(inMode.get(p)));
            } catch (IOException ecp) {
                p.sendMessage("Une erreur est survenue");
            }
            p.updateInventory();
            inMode.remove(p);
            p.teleport(pos.get(p));
            pos.remove(p);
            if(p.hasPermission("skycore.staffmode.gm1")) {
                p.setGameMode(GameMode.CREATIVE);
            } else if(p.hasPermission("skycore.staffmode.gm0")) {
                p.setGameMode(GameMode.SURVIVAL);
                CMI.getInstance().getPlayerManager().getUser(p).setFlying(false);
            }
        } else {
            inMode.put(p, BukkitSerialization.playerInventoryToBase64(p.getInventory()));
            pos.put(p, p.getLocation());
            p.getInventory().clear();
            giveItems(p);
            p.sendMessage("§c§lModération§f│ §eVous venez d'entrer en §aStaffMode §e!");
            p.chat("/vanish");
            if(p.hasPermission("skycore.staffmode.gm1")) {
                p.setGameMode(GameMode.CREATIVE);
            } else if(p.hasPermission("skycore.staffmode.gm0")) {
                p.setGameMode(GameMode.SURVIVAL);
                CMI.getInstance().getPlayerManager().getUser(p).setFlying(true);
            }
        }
    }

    public static boolean isStaffMode(Player p) {
        return inMode.containsKey(p);
    }

    public static boolean isFreeze(Player p) {
        return freezed.containsKey(p);
    }

    public static void freeze(Player p) {
        if(freezed.containsKey(p)) {
            freezed.remove(p);
        } else {
            freezed.put(p, p.getLocation());
        }
    }

    public static Location freezeLoc(Player p) {
        return freezed.get(p);
    }

    private static void giveItems(Player p) {
        ItemStack itemStack = new ItemStack(Material.STICK, 1);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.addEnchant(Enchantment.KNOCKBACK, 2, true);
        itemMeta.setDisplayName("§eBâton de §6§lKB");
        itemStack.setItemMeta(itemMeta);
        p.getInventory().addItem(itemStack);

        itemStack = new ItemStack(Material.BLAZE_ROD, 1);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§eBâton de §b§lFreeze");
        itemStack.setItemMeta(itemMeta);
        p.getInventory().addItem(itemStack);

        itemStack = new ItemStack(Material.COMPASS, 1);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§eBoussole de §a§lTéléportation");
        itemStack.setItemMeta(itemMeta);
        p.getInventory().addItem(itemStack);

        itemStack = new ItemStack(Material.BOOK, 1);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§eMenu des §c§lSanctions");
        itemStack.setItemMeta(itemMeta);
        p.getInventory().addItem(itemStack);

        itemStack = new ItemStack(Material.BARRIER, 1);
        itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§cQuitter le StaffMode");
        itemStack.setItemMeta(itemMeta);
        p.getInventory().setItem(8, itemStack);
    }
}
