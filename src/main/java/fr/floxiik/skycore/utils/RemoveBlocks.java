package fr.floxiik.skycore.utils;

import fr.floxiik.skycore.SkyCore;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class RemoveBlocks extends BukkitRunnable {

    private Block block;
    private int timer;

    public RemoveBlocks(Location location) {
        block = location.getBlock();
        timer = 45;
        this.runTaskTimer(SkyCore.getInstance(), 0, 20L);
    }

    public RemoveBlocks(Location location, int timer) {
        block = location.getBlock();
        this.timer = timer;
        this.runTaskTimer(SkyCore.getInstance(), 0, 20L);
    }

    @Override
    public void run() {
        if(timer == 45) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)13);
        } else if(timer == 25) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)5);
        } else if(timer == 18) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)4);
        } else if(timer == 10) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)1);
        } else if(timer == 5) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)14);
        } else if(timer == 3) {
            block.setType(Material.STAINED_CLAY);
            block.setData((byte)15);
        } else if(timer == 0) {
            block.setType(Material.AIR);
            this.cancel();
        }
        timer--;
    }
}
