package fr.floxiik.skycore.utils;

public enum ChatFormat {
    ADMIN("{prefix}{displayName}%deluxetags_tag% &8»&c {message}"),
    MODO("{prefix}{displayName}%deluxetags_tag% &8»&a {message}"),
    BASICSTAFF("{prefix}{displayName}%deluxetags_tag% &8»&f {message}"),
    VIP("&8[&7#&e%zfactionranking_me_rank%&8] %rel_factionsuuid_relation_color%%factionsuuid_player_role%%factionsuuid_faction_name% {prefix}{displayName}%deluxetags_tag% &8»&f {message}"),
    DEFAULT("&8[&7#&e%zfactionranking_me_rank%&8] %rel_factionsuuid_relation_color%%factionsuuid_player_role%%factionsuuid_faction_name% {prefix}{displayName}%deluxetags_tag% &8»&7 {message}");

    private final String text;

    ChatFormat(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}