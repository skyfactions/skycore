package fr.floxiik.skycore.utils;

import java.util.Random;

    public class RandomGenerator {

    public static int smallerThan(int maxValue) {
        Random randGen = new Random();
        int randNum = randGen.nextInt(maxValue);
        return randNum;
    }

    public static int between(int minValue, int maxValue) {
        Random randGen = new Random();
        int max = maxValue - minValue + 1;
        int randNum = randGen.nextInt(max);
        randNum += minValue;
        return randNum;
    }

    public static int range(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static double floatVal(int precision) {
        Random randGen = new Random();

        double randNum = randGen.nextDouble();
        double p = (double) Math.pow(10, precision);
        double value = randNum * p;
        double tmp = Math.round(value);
        return (double) tmp / p;
    }
}
