package fr.floxiik.skycore.utils;

import fr.floxiik.skycore.SkyCore;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.List;

public class MessageAnnouncer {

    int i = 0;

    public MessageAnnouncer(int timer) {
        fillMessages();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                i++;
                broadCastAnnounce(i);
                if(i >= 4) i=0;
            }
        }, 20, timer * 20);
    }

    private void broadCastAnnounce(int message) {
        if(message == 1) {
            for(String s : vote) {
                if(s.startsWith("[JSONVOTE]")) {
                    TextComponent textComponent = new TextComponent();
                    textComponent.setText(getCenteredMessage(null, "&2&l【&a➲ Accèder au vote&2&l】 &8(&e☀ Cliquez&8)"));
                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§eClique pour allez sur skyfactions.fr/vote").create()));
                    textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://skyfactions.fr/vote"));
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        p.spigot().sendMessage(textComponent);
                    }
                } else {
                    if(s.contains("[VOTES]")) {
                        String votes = s.replace("[VOTES]", String.valueOf(SkyCore.getInstance().getNeeded_votes() - SkyCore.getInstance().getActual_votes()));
                        sendCenteredMessage(null, votes);
                    } else {
                        String votes = PlaceholderAPI.setPlaceholders(Bukkit.getOfflinePlayer("Floxiik"), s);
                        sendCenteredMessage(null, votes);
                    }
                }
            }
        } else if(message == 2) {
            for(String s : boutique) {
                if(s.startsWith("[JSONBOUTIQUE]")) {
                    TextComponent textComponent = new TextComponent();
                    textComponent.setText(getCenteredMessage(null, "&3&l【&b➲ Accèder à la boutique&3&l】 &8(&e☀ Cliquez&8)"));
                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§eClique pour allez sur skyfactions.fr/store").create()));
                    textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://skyfactions.fr/store/"));
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        p.spigot().sendMessage(textComponent);
                    }
                } else {
                    sendCenteredMessage(null, s);
                }
            }
        } else if(message == 3) {
            for(String s : discord) {
                if(s.startsWith("[JSONDISCORD]")) {
                    TextComponent textComponent = new TextComponent();
                    textComponent.setText(getCenteredMessage(null, "&1&l【&9➲ Accèder au Discord&1&l】 &8(&e☀ Cliquez&8)"));
                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§eClique pour allez sur discord.gg/X9vT33d").create()));
                    textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/X9vT33d"));
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        p.spigot().sendMessage(textComponent);
                    }
                } else {
                    sendCenteredMessage(null, s);
                }
            }
        } else if(message == 4) {
            for(String s : wiki) {
                if(s.startsWith("[JSONWIKI]")) {
                    TextComponent textComponent = new TextComponent();
                    textComponent.setText(getCenteredMessage(null, "&4&l【&c➲ Accèder au Wiki&4&l】 &8(&e☀ Cliquez&8)"));
                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§eClique pour allez sur skyfactions.fr/wiki").create()));
                    textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://skyfactions.fr/wiki"));
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        p.spigot().sendMessage(textComponent);
                    }
                } else {
                    sendCenteredMessage(null, s);
                }
            }
        }
    }

    private ArrayList<String> vote = new ArrayList<>();
    private ArrayList<String> boutique = new ArrayList<>();
    private ArrayList<String> discord = new ArrayList<>();
    private ArrayList<String> wiki = new ArrayList<>();

    private void fillMessages() {
        vote.add("");
        vote.add("&8•&7•&f•  ▏ &a&lVote &f▏  &f•&7•&8•");
        vote.add("");
        vote.add("&e&oN'oubliez pas de voter &6&otous les jours");
        vote.add("&e&opour obtenir des &b&oclés &e&ode &d&oVote&e&o.");
        vote.add("&e&oIl manque &c&l&o[VOTES] &c&ovotes &e&opour le &5&o&lVoteParty &e&o!");
        vote.add("");
        vote.add("&8(%progress_bar_{skycore_votes}_c:&a|_p:&e|||_r:&7|_l:85_m:120%&8)");
        vote.add("");
        vote.add("[JSONVOTE]");
        vote.add("");

        boutique.add("");
        boutique.add("&8•&7•&f•  ▏ &b&lBoutique &f▏  &f•&7•&8•");
        boutique.add("");
        boutique.add("&e&oVous avez besoin de &d&ocrédits &e&o?");
        boutique.add("&e&oAchètes-en &6&orapidement &e&osur le site !");
        boutique.add("&e&oActuellement &a&l&o-20% &e&osur les &d&l&ocredits &e&o!");
        boutique.add("");
        boutique.add("[JSONBOUTIQUE]");
        boutique.add("");

        discord.add("");
        discord.add("&8•&7•&f•  ▏ &9&lDiscord &f▏  &f•&7•&8•");
        discord.add("");
        discord.add("&e&oRejoint notre &a&ocommunautée &e&oet parle");
        discord.add("&e&oavec d'autres joueurs !");
        discord.add("&e&oN'oublie pas le &9&o&l/discord link &e&opour");
        discord.add("&e&oavoir &a&oaccès &e&oà du &6&ocontenu supplémentaire &e&o!");
        discord.add("");
        discord.add("[JSONDISCORD]");
        discord.add("");

        wiki.add("");
        wiki.add("&8•&7•&f•  ▏ &c&lWiki &f▏  &f•&7•&8•");
        wiki.add("");
        wiki.add("&e&oVous ne &6&osavez pas &e&oencore tout sur");
        wiki.add("&e&ole serveur ? Vous pouvez aller voir");
        wiki.add("&e&ole &c&owiki &e&opour &6&otout savoir &e&oet être &a&omeilleur &e&o!");
        wiki.add("");
        wiki.add("[JSONWIKI]");
        wiki.add("");
    }

    private final int CENTER_PX = 154;

    private String getCenteredMessage(Player player, String message) {
        if(message == null || message.equals("")) player.sendMessage("");
        message = ChatColor.translateAlternateColorCodes('&', message);
        message = PlaceholderAPI.setPlaceholders(player, message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()){
            if(c == '§'){
                previousCode = true;
                continue;
            }else if(previousCode == true){
                previousCode = false;
                if(c == 'l' || c == 'L'){
                    isBold = true;
                    continue;
                }else isBold = false;
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate){
            sb.append(" ");
            compensated += spaceLength;
        }
        return sb.toString() + message;
    }

    private void sendCenteredMessage(Player player, String message){
        if(message == null || message.equals("")) {
            Bukkit.broadcastMessage("");
            return;
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        message = PlaceholderAPI.setPlaceholders(player, message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()){
            if(c == '§'){
                previousCode = true;
                continue;
            }else if(previousCode == true){
                previousCode = false;
                if(c == 'l' || c == 'L'){
                    isBold = true;
                    continue;
                }else isBold = false;
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate){
            sb.append(" ");
            compensated += spaceLength;
        }
        Bukkit.broadcastMessage(sb.toString() + message);
    }
}