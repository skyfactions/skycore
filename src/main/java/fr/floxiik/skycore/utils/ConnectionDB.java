package fr.floxiik.skycore.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionDB {

    private HikariDataSource hikariDataSource;
    private static String driver = "com.mysql.jdbc.Driver";
    private static String db = "skyfactions";
    private static String url = "jdbc:mysql://localhost:3306/";
    private static String user = "skyfactionsDB";
    private static String passwd = "2xRVW7yFbZ&[emt7B4!%243){";
    private static Connection connect;

    public ConnectionDB() {
        initDatabase();
    }

    public Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }

    private void initDatabase() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/" + db + "?autoReconnect=true");
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(passwd);
        hikariConfig.setMaxLifetime(600000L);
        hikariConfig.setLeakDetectionThreshold(300000L);
        hikariConfig.setConnectionTimeout(10000L);

        hikariDataSource = new HikariDataSource(hikariConfig);

        createTables();
    }

    private void createTables() {
        String crew = "CREATE TABLE IF NOT EXISTS skycore_crew (\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "faction VARCHAR(30),\n"
                + "level INT(4),\n"
                + "plot INT(255),\n"
                + "posX INT(255),\n"
                + "posZ INT(255),\n"
                + "need_update BOOLEAN DEFAULT false,\n"
                + "die BOOLEAN DEFAULT false,\n"
                + "creation_date DATETIME)";
        String dead_crew = "CREATE TABLE IF NOT EXISTS skycore_dead_crew (\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "faction VARCHAR(30),\n"
                + "level INT(4),\n"
                + "plot INT(255),\n"
                + "posX INT(255),\n"
                + "posZ INT(255),\n"
                + "death_date DATETIME)\n";
        String vote = "CREATE TABLE IF NOT EXISTS skycore_vote (\n"
                + "id INT PRIMARY KEY,\n"
                + "nbVotes INT,\n"
                + "neededVotes INT)";
        String initVotes = "INSERT INTO skycore_vote VALUES (1, 0, 120)";
        String user_vote = "CREATE TABLE IF NOT EXISTS skycore_user_vote (\n"
                + "username VARCHAR(30) PRIMARY KEY,\n"
                + "nbVotes INT)";
        String tel = "CREATE TABLE IF NOT EXISTS skycore_tel (\n"
                + "username VARCHAR(30) PRIMARY KEY,\n"
                + "phone VARCHAR(10),\n"
                + "verify VARCHAR(50))";
        String ranking = "CREATE TABLE IF NOT EXISTS skycore_ranking (\n"
                + "faction VARCHAR(30) PRIMARY KEY,\n"
                + "points DOUBLE(255, 1))";
        String event = "CREATE TABLE IF NOT EXISTS skycore_event (\n"
                + "id INT PRIMARY KEY AUTO_INCREMENT,\n"
                + "world VARCHAR(40),\n"
                + "x DOUBLE(255, 1),\n"
                + "y DOUBLE(255, 1),\n"
                + "z DOUBLE(255, 1),\n"
                + "yaw REAL,\n"
                + "pitch REAL)";
        String pillages = "CREATE TABLE IF NOT EXISTS skycore_pillages (\n"
                + "id INT PRIMARY KEY,\n"
                + "nbPillages INT)";
        try {
            Connection c = getConnection();
            Statement s = c.prepareStatement(crew);
            s.executeUpdate(crew);
            s = c.prepareStatement(dead_crew);
            s.executeUpdate(dead_crew);
            s = c.prepareStatement(initVotes);
            s.executeUpdate(initVotes);
            s = c.prepareStatement(vote);
            s.executeUpdate(vote);
            s = c.prepareStatement(user_vote);
            s.executeUpdate(user_vote);
            s = c.prepareStatement(tel);
            s.executeUpdate(tel);
            s = c.prepareStatement(ranking);
            s.executeUpdate(ranking);
            s = c.prepareStatement(event);
            s.executeUpdate(event);
            s = c.prepareStatement(pillages);
            s.executeUpdate(pillages);
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}