package fr.floxiik.skycore.utils;

public class TelNumber {

    private String owner;
    private String number;
    private String requestId;

    public TelNumber(String owner, String number, String requestId) {
        this.owner = owner;
        this.number = number;
        this.requestId = requestId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
