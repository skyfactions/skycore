package fr.floxiik.skycore.utils;

import fr.floxiik.skycore.SkyCore;
import me.devtec.theapi.TheAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class WallsBlocks extends BukkitRunnable {

    private Block block;
    private Location location1;
    private Location location2;
    private boolean interupted1 = false;
    private boolean interupted2 = false;
    private boolean sand = false;

    public WallsBlocks(Location location) {
        if(location.getBlock().getType() == Material.SAND) {
            block = location.getBlock();
            location.getBlock().setType(Material.AIR);
            sand = true;
            location1 = new Location(location.getWorld(), location.getX(), location.getWorld().getHighestBlockYAt(location), location.getZ());
            this.runTaskTimer(SkyCore.getInstance(), 0, 4L);
        } else {
            block = location.getBlock();
            this.location1 = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
            this.location2 = new Location(location.getWorld(), location.getX(), location.getY(), location.getZ());
            this.runTaskTimer(SkyCore.getInstance(), 0, 4L);
        }
    }

    @Override
    public void run() {
        if(sand) {
            if(location1.add(0, 1, 0).getBlock().getType() == Material.AIR) {
                location1.getBlock().setType(Material.SAND);
            } else {
                this.cancel();
            }
        } else {
            if (location1.add(0, 1, 0).getBlock().getType() == Material.AIR && !interupted1) {
                location1.getBlock().setType(block.getType());
            } else {
                interupted1 = true;
            }
            if (location2.add(0, -1, 0).getBlock().getType() == Material.AIR && !interupted2) {
                location2.getBlock().setType(block.getType());
            } else {
                interupted2 = true;
            }
            if (interupted1 && interupted2) {
                this.cancel();
            }
        }
     }
}
