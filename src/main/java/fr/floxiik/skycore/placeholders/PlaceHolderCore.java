package fr.floxiik.skycore.placeholders;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.crew.Crews;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.events.DragonManager;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class PlaceHolderCore extends PlaceholderExpansion {

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Floxiik";
    }

    @Override
    public String getIdentifier() {
        return "skycore";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }


    @Override
    public String onPlaceholderRequest(Player p, String identifier) {

        if (identifier.equals("dragon_health")) {
            return String.valueOf(round(DragonManager.health, 2));
        }
        if (identifier.equals("dragon_points_me")) {
            if (!DragonManager.points.containsKey(p)) {
                return "?";
            } else {
                return String.valueOf(DragonManager.points.get(p));
            }
        }
        if (identifier.equals("dragon_points_1")) {
            if (DragonManager.getClassementPlayer().size() > 0) {
                return String.valueOf(DragonManager.points.get(DragonManager.getClassementPlayer().get(0)));
            } else {
                return "---";
            }
        }
        if (identifier.equals("dragon_points_2")) {
            if (DragonManager.getClassementPlayer().size() > 1) {
                return String.valueOf(DragonManager.points.get(DragonManager.getClassementPlayer().get(1)));
            } else {
                return "---";
            }
        }
        if (identifier.equals("dragon_points_3")) {
            if (DragonManager.getClassementPlayer().size() > 2) {
                return String.valueOf(DragonManager.points.get(DragonManager.getClassementPlayer().get(2)));
            } else {
                return "---";
            }
        }
        if (identifier.equals("dragon_player_1")) {
            if (DragonManager.getClassementPlayer().size() > 0) {
                return DragonManager.getClassementPlayer().get(0).getName();
            } else {
                return "---";
            }
        }
        if (identifier.equals("dragon_player_2")) {
            if (DragonManager.getClassementPlayer().size() > 1) {
                return DragonManager.getClassementPlayer().get(1).getName();
            } else {
                return "---";
            }
        }
        if (identifier.equals("dragon_player_3")) {
            if (DragonManager.getClassementPlayer().size() > 2) {
                return DragonManager.getClassementPlayer().get(2).getName();
            } else {
                return "---";
            }
        }
        if (identifier.equals("votes")) {
            return SkyCore.getInstance().getActual_votes() + "";
        }
        if (identifier.equals("player_votes")) {
            return SkyCore.getInstance().getPlayerVote(p.getName()) + "";
        }
        if (identifier.equals("max_votes")) {
            return SkyCore.getInstance().getNeeded_votes() + "";
        }
        if (identifier.equals("action")) {
            if (Action.getDayAction().equalsIgnoreCase("pause")) {
                return "&aRepos ⚒";
            } else if (Action.getDayAction().equalsIgnoreCase("pillage")) {
                return "&cPillage ⚔";
            } else {
                return "&cErreur";
            }
        }
        if (identifier.equals("timer")) {
            return "" + (Action.getHours() > 6 ? (30 - Action.getHours()) : 6 - Action.getHours());
        }
        if (identifier.equals("level")) {
            FPlayer fp = FPlayers.getInstance().getByPlayer(p);
            if (fp.hasFaction()) {
                for (Crew c : Crews.getCrews()) {
                    if (c.getFaction().equals(fp.getFaction().getTag())) {
                        switch (c.getLevel()) {
                            case 1:
                                return "§cDébutant ♟";
                            case 2:
                                return "§ePro ♜";
                            case 3:
                                return "§bExpert ♚";
                            case 4:
                                return "§aLegende ♛";
                            default:
                                return "§cErreur";
                        }
                    }
                }
            }
            return "§c✖";
        }
        return "§c✖";
    }

        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            long factor = (long) Math.pow(10, places);
            value = value * factor;
            long tmp = Math.round(value);
            return (double) tmp / factor;
        }
    }