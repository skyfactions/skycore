package fr.floxiik.skycore.bonus;

import com.Zrips.CMI.Modules.Economy.Economy;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.help.PlayerHelp;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Spectator {

    private static Location spawn = new Location(Bukkit.getWorld("faction"), 4.5, 197.0, -2.5);

    private static final double cost = 100000;

    private static HashMap<Player, Boolean> spectators = new HashMap<>();
    private static HashMap<Player, Integer> cooldowns = new HashMap<>();
    private static HashMap<Player, Location> lastPos = new HashMap<>();

    public static void setSpectator(Player p, int time) {
        if(Economy.getBalance(Bukkit.getOfflinePlayer(p.getUniqueId())) >= cost) {
            if (p.getWorld().getName().contains("faction_crew")) {
                if(!isSpectator(p)) {
                    setSpectator(p, true);
                    setCooldown(p, time);
                    lastPos.put(p, p.getLocation());
                    p.setGameMode(GameMode.SPECTATOR);
                    Economy.withdrawPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()), cost);
                    p.sendMessage(PlayerHelp.prefix);
                    p.sendMessage("");
                    p.sendMessage("§l➢ §eVous êtes passé en mode §cspectateur");
                    p.sendMessage("§l➢ §eavec §asuccès §e!");
                    p.sendMessage("§l➢ §eSi vous §6changez §ede monde, ou si vous");
                    p.sendMessage("§l➢ §evous §6déconnectez §eil vous sera §cretiré §e!");
                    p.sendMessage("");
                    p.sendMessage(PlayerHelp.suffix);
                } else {
                    p.sendMessage("§c§lErreur §8» §eVous êtes déjà en spectateur !");
                }
            } else {
                p.sendMessage("§c§lErreur §8» §eVous devez être dans un monde de §6factions§e pour effectuer cette commande !");
            }
        } else {
            p.sendMessage("§c§lErreur §8» §eVous n'avez pas les fonds nécessaires ! §8(§ePrix: §c100 000$§8)");
        }
    }

    public static void setSpectator(Player p, boolean b) {
        if(spectators.containsKey(p)) {
            spectators.replace(p, b);
            if(!b) {
                p.teleport(lastPos.get(p));
                p.setGameMode(GameMode.SURVIVAL);
                p.sendMessage(PlayerHelp.prefix);
                p.sendMessage("");
                p.sendMessage("§l➢ §eVous n'êtes plus §6spectateur§e !");
                p.sendMessage("§l➢ §eVous avez été §atéléporté§e à votre§e !");
                p.sendMessage("§l➢ §edernière position§e !");
                p.sendMessage("");
                p.sendMessage(PlayerHelp.suffix);
            }
        } else {
            spectators.put(p, b);
        }
    }

    public static void setCooldown(Player p, int i) {
        if(cooldowns.containsKey(p)) {
            cooldowns.replace(p, i);
        } else {
            cooldowns.put(p, i);
        }
    }

    public static int getCooldown(Player p) {
        return cooldowns.get(p);
    }

    public static boolean isSpectator(Player p) {
        return spectators.get(p);
    }

    public static void spectatorScheduler() {
        Bukkit.getScheduler().runTaskTimer(SkyCore.getInstance(), new Runnable() {
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    if(isSpectator(p)) {
                        setCooldown(p, getCooldown(p)-5);
                        if(getCooldown(p) == 15) {
                            p.sendMessage("§c§lInfo §8» §eIl vous reste plus que §a15 secondes§e de §6spectateur §e!");
                        } else if(getCooldown(p) == 5) {
                            p.sendMessage("§c§lInfo §8» §eIl vous reste plus que §a5 secondes§e de §6spectateur §e!");
                        } else if(getCooldown(p)<=0) {
                            setSpectator(p, false);
                            setCooldown(p, 0);
                        }
                    }
                }
            }
        }, 0L, 20*5);
    }

    private static Location getSpawn() {
        return spawn;
    }
}
