package fr.floxiik.skycore.dao;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.utils.ConnectionDB;
import fr.floxiik.skycore.utils.TelNumber;
import fr.maxlego08.zfactionsranking.stats.StatsManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RankingDAO {

    Connection connection;

    public RankingDAO() {}

    public void setRanking() {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("TRUNCATE table skycore_ranking");
            for(String str : StatsManager.rankings.keySet()) {
                s.executeUpdate("insert into skycore_ranking values ('" + str + "', '" + StatsManager.rankings.get(str) + "')");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void CloseConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
