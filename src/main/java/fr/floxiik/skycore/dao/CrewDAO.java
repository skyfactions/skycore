package fr.floxiik.skycore.dao;

import com.massivecraft.factions.FPlayers;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.crew.Crews;
import fr.floxiik.skycore.utils.ConnectionDB;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CrewDAO {

    private Connection connection;

    public CrewDAO() {
    }

    public void updateCrew(Crew c) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("update skycore_crew set faction='" + c.getFaction() + "', level=" + c.getLevel() + ", plot=" + c.getPlot() + ", posX=" + c.getPositionX() + ", posZ=" + c.getPositionZ() + ", need_update='" + (String.valueOf(c.isUpdate()).equals("false") ? 0 : 1) + "', die='" + (String.valueOf(c.isDie()).equals("false") ? 0 : 1) + "' where id=" + c.getId());
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addCrew(Crew c) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("insert into skycore_crew values ('" + c.getId() + "', '" + c.getFaction() + "', '" + c.getLevel() + "', '" + c.getPlot() + "', '" + c.getPositionX() + "', '" + c.getPositionZ() + "', '" + (String.valueOf(c.isUpdate()).equals("false") ? 0 : 1) + "', '" + (String.valueOf(c.isDie()).equals("false") ? 0 : 1) + "', NOW())");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addDeadCrew(Crew c) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("insert into skycore_dead_crew values ('" + 0 + "', '" + c.getFaction() + "', '" + c.getLevel() + "', '" + c.getPlot() + "', '" + c.getPositionX() + "', '" + c.getPositionZ() + "', NOW())");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getCrewNewId() {
        int id = 0;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select max(id) as newId from skycore_crew");
            while (r.next()) {
                id = r.getInt("newId") + 1;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return id;
    }

    public int getCrewNewPlot() {
        int plot = 0;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select max(plot) as newPlot from skycore_crew");
            while (r.next()) {
                plot = r.getInt("newPlot") + 1;
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return plot;
    }

    public Crew getPlayerCrew(Player p) {
        String faction = FPlayers.getInstance().getByPlayer(p).getFaction().getTag();
        Crew crew = null;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where faction='" + faction + "'");
            while (r.next()) {
                crew = resultSetToCrew(r);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crew;
    }

    public ArrayList<Crew> getAliveCrews() {
        ArrayList<Crew> crews = new ArrayList<>();
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where die='0'");
            while (r.next()) {
                crews.add(resultSetToCrew(r));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crews;
    }

    public ArrayList<Crew> getDyingCrews() {
        ArrayList<Crew> crews = new ArrayList<>();
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where die='1'");
            while (r.next()) {
                crews.add(resultSetToCrew(r));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crews;
    }

    public void deleteDyingCrew(Crew c) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("delete from skycore_crew where id=" + c.getId());
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Crew> getWaitingUpdateCrews() {
        ArrayList<Crew> crews = new ArrayList<>();
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where need_update='1'");
            while (r.next()) {
                crews.add(resultSetToCrew(r));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crews;
    }

    public ArrayList<Crew> getAllCrews() {
        ArrayList<Crew> crews = new ArrayList<>();
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew");
            while (r.next()) {
                crews.add(resultSetToCrew(r));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crews;
    }

    public Crew getCrew(int i) {
        Statement s = null;
        Crew crew = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where id=" + i);
            while (r.next()) {
                crew = resultSetToCrew(r);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crew;
    }

    public Crew getCrew(String f) {
        Statement s = null;
        Crew crew = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_crew where faction='" + f + "'");
            while (r.next()) {
                crew = resultSetToCrew(r);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return crew;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Crew resultSetToCrew(final ResultSet rset) throws SQLException {
        int id = rset.getInt("id");
        String faction = rset.getString("faction");
        int level = rset.getInt("level");
        int plot = rset.getInt("plot");
        int posX = rset.getInt("posX");
        int posZ = rset.getInt("posZ");
        boolean update = rset.getBoolean("need_update");
        boolean die = rset.getBoolean("die");

        Crew crew = new Crew(id, faction, level, plot, posX, posZ, update, die);
        return crew;
    }
}
