package fr.floxiik.skycore.dao;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.utils.ConnectionDB;
import fr.floxiik.skycore.utils.TelNumber;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EventDAO {

    Connection connection;

    public EventDAO() {}

    public void addPos(Location location) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("insert into skycore_event (world, x, y, z, yaw, pitch)\n"
                    + "values ('" + location.getWorld().getName() + "', '" + location.getX() + "', '" + location.getY() + "', '" + location.getZ() + "', '" + location.getYaw() + "', '" + location.getPitch() + "')");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<Location> getAllPos() {
        ArrayList<Location> locations = new ArrayList<>();
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            ResultSet r = s.executeQuery("select * from skycore_event");
            while(r.next()) {
                locations.add(new Location(Bukkit.getWorld(r.getString("world")), r.getDouble("x"), r.getDouble("y"), r.getDouble("z"), r.getFloat("yaw"), r.getFloat("pitch")));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return locations;
    }

    public void CloseConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
