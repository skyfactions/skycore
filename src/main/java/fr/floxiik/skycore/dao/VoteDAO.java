package fr.floxiik.skycore.dao;

import com.boydti.fawe.installer.JSystemFileChooser;
import com.massivecraft.factions.FPlayers;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.utils.ConnectionDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class VoteDAO {

    Connection connection;

    public VoteDAO() {}

    public void updateVotes(int i, int j) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("update skycore_vote set nbVotes='" + i + "', neededVotes='" + j + "' where id=1");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateNeededVotes(int i) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("update skycore_vote set neededVotes='" + i + "' where id=1");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void resetVotes() {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("update skycore_user_vote set nbVotes=0");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addPlayerVote(String str) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("insert into skycore_user_vote values ('" + str + "', 1) on duplicate key update nbVotes=nbVotes+1");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public HashMap<String, Integer> getPlayerVotes() {
        HashMap<String, Integer> playersVotes = new HashMap<>();
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_user_vote order by nbVotes desc");
            while(r.next()) {
                String pseudo = r.getString("username");
                int votes = r.getInt("nbVotes");
                playersVotes.put(pseudo, votes);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return playersVotes;
    }

    public int getPlayerVote(String str) {
        int nbVotes = 0;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_user_vote where username='" + str + "'");
            while (r.next()) {
                nbVotes = r.getInt("nbVotes");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return nbVotes;
    }

    public int getNbVotes() {
        int nbVotes = 0;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_vote");
            while (r.next()) {
                nbVotes = r.getInt("nbVotes");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return nbVotes;
    }

    public int getNeededVotes() {
        int neededVotes = 0;
        Statement s = null;
        ResultSet r = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            r = s.executeQuery("select * from skycore_vote");
            while(r.next()) {
                neededVotes = r.getInt("neededVotes");
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (r != null) {
                try {
                    r.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return neededVotes;
    }

    public void CloseConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
