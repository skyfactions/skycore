package fr.floxiik.skycore.dao;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.utils.ConnectionDB;
import fr.floxiik.skycore.utils.TelNumber;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AlertDAO {

    Connection connection;

    public AlertDAO() {}

    public void setTel(String pseudo, String numero, String requestId) {
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            s.executeUpdate("insert into skycore_tel \n"
                    + "values ('" + pseudo + "', '" + numero + "', '" + requestId + "') \n"
                    + "on duplicate key update phone='" + numero + "', verify='" + requestId + "'");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public TelNumber getTel(String pseudo) {
        TelNumber tel = null;
        Statement s = null;
        try {
            connection = SkyCore.getInstance().getConnection();
            s = connection.createStatement();
            ResultSet r = s.executeQuery("select * from skycore_tel where username='" + pseudo + "'");
            while(r.next()) {
                tel = new TelNumber(r.getString("username"), r.getString("phone"), r.getString("verify"));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return tel;
    }

    public void CloseConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
