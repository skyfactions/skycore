package fr.floxiik.skycore.actions;

import com.Zrips.CMI.CMI;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import fr.floxiik.skycore.SkyCore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collections;

public class Pillage {

    private static int timer = 1 * 10;

    public static void startPillage() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (timer > 0) {
                    for (Player player : Bukkit.getOnlinePlayers())
                        CMI.getInstance().getActionBarManager().send(player, "§c§lLe serveur vas passer en phase &c&l&nPillage&c&l dans §e§l" + timer + " §c§lsecondes !");
                } else if (timer == 0) {
                    removeWarzone();
                }
                timer--;
            }
        }, 0, 20);
    }

    public static void removeWarzone() {
        Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                long timeMilli = System.currentTimeMillis();
                Board board = Board.getInstance();
                int size = Board.getInstance().getAllClaims(Factions.getInstance().getWarZone().getId()).size();
                for (FLocation fLocation : Board.getInstance().getAllClaims(Factions.getInstance().getWarZone().getId())) {
                    if (fLocation.getWorldName().equals("faction_crew")) {
                        Bukkit.getScheduler().runTask(SkyCore.getInstance(), new Runnable() {
                            @Override
                            public void run() {
                                board.setFactionAt(Factions.getInstance().getWilderness(), fLocation);
                            }
                        });
                        try {
                            Thread.sleep(0, 50);
                            System.out.println(size--);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //board.unclaimAllInWorld(Factions.getInstance().getWarZone().getId(), Bukkit.getWorld("faction_crew"));
                long newtimeMilli = System.currentTimeMillis();
                System.out.println("§eWarzone supprimée des maps factions ! En " + (newtimeMilli - timeMilli) + "ms");
                Bukkit.getScheduler().runTask(SkyCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            CMI.getInstance().getTitleMessageManager().send(p, "§cATTENTION !", "§6Les pillages sont désormais actifs !", 3, 60, 10);
                        }
                    }
                });
            }
        });
    }
}