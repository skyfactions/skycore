package fr.floxiik.skycore.actions;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.CrewsMoving;
import fr.floxiik.skycore.dao.RankingDAO;
import fr.floxiik.skycore.dao.VoteDAO;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.time.LocalDateTime;

public class Action {

    private static String pauseLabel = "pause";
    private static String pillageLabel = "pillage";
    private static String dayAction;
    private static int day;
    private static int hours;
    private static int minutes;
    private static int seconds;

    public static void findDayAction() {
        LocalDateTime date = LocalDateTime.now();
        day = date.getDayOfYear();
        hours = date.getHour();
        minutes = date.getMinute();
        seconds = date.getSecond();
        if (day % 2 == 0) {
            if (hours >= 6) {
                dayAction = pillageLabel;
            } else {
                dayAction = pauseLabel;
            }
        } else {
            if (hours >= 6) {
                dayAction = pauseLabel;
            } else {
                dayAction = pillageLabel;
            }
        }
    }

    public static void setDayAction(String action) {
        dayAction = action;
    }

    public static String getDayAction() {
        return dayAction;
    }

    public static boolean isPause() {
        return dayAction.equalsIgnoreCase(pauseLabel);
    }

    public static boolean isPillage() {
        return dayAction.equalsIgnoreCase(pillageLabel);
    }

    public static int getDay() {
        return day;
    }

    public static int getHours() {
        return hours;
    }

    public static int getMinutes() {
        return minutes;
    }

    public static int getSeconds() {
        return seconds;
    }

    @SuppressWarnings("deprecation")
    public static void startActionVerifTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        if(!CrewsMoving.isMoving()) {
                            SkyCore.getInstance().saveData();
                            Action.findDayAction();
                            System.err.println("[SKYCORE] TOUTES LES DONNEES ON ETE SAUVEGUARDES !");
                            RankingDAO rankingDAO = new RankingDAO();
                            rankingDAO.setRanking();
                            VoteDAO voteDAO = new VoteDAO();
                            SkyCore.getInstance().setPlayers_votes(voteDAO.getPlayerVotes());
                        }
                    }
                });
            }
        }, 0, 1200);
    }

    public static void startServerRestart() {

    }
}