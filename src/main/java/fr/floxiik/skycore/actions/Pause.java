package fr.floxiik.skycore.actions;

import com.Zrips.CMI.CMI;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.crew.CrewsMoving;
import fr.floxiik.skycore.help.PlayerHelp;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Pause {

    private static int timer = 1 * 20;

    public static void safeStop() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (timer > 0) {
                    for (Player player : Bukkit.getOnlinePlayers())
                        CMI.getInstance().getActionBarManager().send(player, "§c§lLe serveur vas redémarrer dans §e§l" + timer + " §c§lsecondes ! §7(Les pillages seront finis)");
                } else if (timer == 0) {
                    kickPlayers();
                    claimWarzone();
                } else if (timer == -10) {
                    CrewsMoving.move();
                } else if (timer == -180) {
                    Bukkit.shutdown();
                }
                timer--;
            }
        }, 0, 20);
    }

    public static void claimWarzone() {
        Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                Bukkit.broadcastMessage("§eDémarrage du claim de la warzone !");
                DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099L).sendMessage("Démarrage du claim de la warzone !").queue();
                long timeMilli = System.currentTimeMillis();
                CrewDAO crewDAO = new CrewDAO();
                int claims = 0;
                for (int i = -30; i < /*(Math.sqrt(crewDAO.getCrewNewPlot()) +*/ 500; i++) {
                    for (int j = -30; j < /*(Math.sqrt(crewDAO.getCrewNewPlot()) +*/ 500; j++) {
                        FLocation floc = new FLocation("faction_crew", i, j);
                        Bukkit.getScheduler().runTask(SkyCore.getInstance(), new Runnable() {
                            @Override
                            public void run() {
                                Board.getInstance().setFactionAt(Factions.getInstance().getWarZone(), floc);
                            }
                        });
                        claims++;
                    }
                }
                long newtimeMilli = System.currentTimeMillis();
                Bukkit.broadcastMessage("§eClaim des warzones terminé !");
                DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099L).sendMessage("Claim des warzones terminé ! (" + claims + ") En " + (newtimeMilli - timeMilli) + "ms").queue();
            }
        });
    }

    public static void removeSafeZone() {
        Board board = Board.getInstance();
        board.unclaimAllInWorld(Factions.getInstance().getSafeZone().getId(), Bukkit.getWorld("faction_crew"));
        Bukkit.broadcastMessage("§eSafezone supprimée des maps factions !");
    }

    private static void kickPlayers() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.teleport(PlayerHelp.spawn);
            p.kickPlayer("§cLe serveur redémarre ! Re-connectez vous.");
        }
        CMI.getInstance().getConfigManager().setMaintenance(true);
        CMI.getInstance().getConfigManager().setMaintenanceMessage("&cLe serveur redémarre &8(&eFin des pillages&8)");
    }
}