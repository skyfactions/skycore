package fr.floxiik.skycore;

import com.Zrips.CMI.CMI;
import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.bonus.Spectator;
import fr.floxiik.skycore.commands.Register;
import fr.floxiik.skycore.commands.VoteCommand;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.crew.Crews;
import fr.floxiik.skycore.dao.EventDAO;
import fr.floxiik.skycore.dao.VoteDAO;
import fr.floxiik.skycore.drop.DropChests;
import fr.floxiik.skycore.events.EventScheduler;
import fr.floxiik.skycore.tech.FactionData;
import fr.floxiik.skycore.tech.SkyTech;
import fr.floxiik.skycore.utils.ConnectionDB;
import fr.floxiik.skycore.events.EventManager;
import fr.floxiik.skycore.utils.MessageAnnouncer;
import fr.floxiik.skycore.worlds.Position;
import fr.floxiik.skycore.listeners.RegisterListeners;
import fr.floxiik.skycore.placeholders.PlaceHolderCore;
import fr.floxiik.skycore.worlds.WorldCreation;
import me.TechsCode.UltraPunishments.UltraPunishments;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

public class SkyCore extends JavaPlugin {

    private File file;
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    private static SkyCore instance;
    private int actual_votes;
    private int needed_votes;
    private HashMap<String, Integer> players_votes = new HashMap<>();
    private ConnectionDB sql;

    @Override
    public void onEnable() {

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        instance = this;

        sql = new ConnectionDB();

        RegisterListeners.Events();
        Register.Commands();

        new PlaceHolderCore().register();

        WorldCreation.init();

        Position.createPositions(1);

        CMI.getInstance().getConfigManager().setMaintenance(false);

        Action.startActionVerifTask();

        SkyTech.enable();

        for (Player p : Bukkit.getOnlinePlayers()) {
            Spectator.setCooldown(p, 0);
            Spectator.setSpectator(p, false);
        }

        Spectator.spectatorScheduler();

        loadData();

        new MessageAnnouncer(120);
        new EventScheduler();

        setupSpawn();


    }

    @Override
    public void onDisable() {
        DropChests.removeAllDrops();
        this.saveData();
    }

    public void setupSpawn() {
        World world = Bukkit.getWorld("faction");
        world.setTime(22550);
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setWeatherDuration(0);
    }

    public void loadData() {
        System.out.println("[SkyCore] Chargement des crews en cours !");

        CrewDAO crewDAO = new CrewDAO();
        ArrayList<Crew> crews = crewDAO.getAliveCrews();
        Crews.getCrews().addAll(crews);

        System.out.println("[SkyCore] " + crews.size() + " crews ont été chargés !");

        VoteDAO voteDAO = new VoteDAO();
        setActual_votes(voteDAO.getNbVotes());
        setNeeded_votes(voteDAO.getNeededVotes());

        EventManager.setLocations(new EventDAO().getAllPos());

        players_votes.clear();
        for (String s : voteDAO.getPlayerVotes().keySet()) {
            players_votes.put(s, voteDAO.getPlayerVotes().get(s));
        }

        if (getActual_votes() >= getNeeded_votes()) {
            VoteCommand.startVoteParty();
            voteDAO.updateVotes(getActual_votes(), getNeeded_votes());
        }

        CMI.getInstance().getConfigManager().setMaintenance(false);

        saveData();
    }


    public void saveData() {
        FactionData.saveAll();
        for (Crew c : Crews.getCrews()) {
            CrewDAO crewDAO = new CrewDAO();
            crewDAO.updateCrew(c);
        }
    }

    public int getActual_votes() {
        return actual_votes;
    }

    public void setActual_votes(int actual_votes) {
        this.actual_votes = actual_votes;
    }

    public int getNeeded_votes() {
        return needed_votes;
    }

    public void setNeeded_votes(int needed_votes) {
        this.needed_votes = needed_votes;
    }

    public void addPlayerVote(String player, int first) {
        if (first == 1) {
            players_votes.put(player, 0);
        } else {
            if (players_votes.containsKey(player)) {
                players_votes.replace(player, players_votes.get(player) + 1);
            } else {
                players_votes.put(player, 1);
            }
        }
    }

    public void setPlayers_votes(HashMap<String, Integer> votes) {
        players_votes.clear();
        players_votes.putAll(votes);
    }

    public int getPlayerVote(String player) {
        return players_votes.getOrDefault(player, 0);
    }

    public static SkyCore getInstance() {
        return instance;
    }

    public Connection getConnection() throws SQLException {
        return sql.getConnection();
    }
}
