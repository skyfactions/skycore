package fr.floxiik.skycore.help;

import fr.floxiik.skycore.utils.DefaultFontInfo;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerHelp {

    public static String prefix = "§f§l§m--------------§f§l< §c§lINFORMATION §f§l>§m--------------";
    public static String suffix = "§f§l§m------------------------------------------";
    public static Location spawn = new Location(Bukkit.getWorld("faction"), 4.5, 197.0, -2.5, 180, 0);

    public static void startManual(Player p) {
        p.sendMessage(prefix);
        p.sendMessage("");
        p.sendMessage("§l➢ §e§lBienvenue §b§l§o" + p.getName() + " §e§lsur votre §6§lfaction §e§l!");
        p.sendMessage("");
        p.sendMessage("§l➢ §eVous voici sur votre §6île§e, ici vous allez pouvoir");
        p.sendMessage("§l➢ §6développer §evotre faction et §6batîr §evotre §cempire§e !");
        p.sendMessage("§l➢ §eVous trouvez ça §3petit §e? C'est tout à fait §anormal§e,");
        p.sendMessage("§l➢ §evotre faction est au §bniveau 1§e, ce qui fait d'elle");
        p.sendMessage("§l➢ §eune petite et fraîche §6nouvelle§e faction, possèdant");
        p.sendMessage("§l➢ §euniquement §b1 seul§e claim !");
        p.sendMessage("§l➢ §eMais j'ai une §abonne nouvelle§e pour vous, si vous");
        p.sendMessage("§l➢ §earrivez à répondre à certains §6critères§e il vous");
        p.sendMessage("§l➢ §esera possible d'§aaugmenter§e le niveau de votre");
        p.sendMessage("§l➢ §efaction et ainsi §6posséder §eplus de claims !");
        p.sendMessage("");
        p.sendMessage("§l➢ §ePour toute aide, la commande §a/aide §eest à");
        p.sendMessage("§l➢ §evotre disposition, sinon n'hésitez pas à");
        p.sendMessage("§l➢ §edemander de l'aide à notre §cStaff §e!");
        p.sendMessage("");
        p.sendMessage(suffix);
    }

    public static void startIslandGeneration(Player p) {
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§eLa machine §6génère §evotre faction...");
        sendCenteredMessage(p, "§ePatientez quelques instants.");
        sendCenteredMessage(p, "§eGénération §aeffectuée §e!");
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        sendCenteredMessage(p, "");
    }

    public static void startIslandUpdate(Player p) {
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§eLa machine §6améliore §evotre faction...");
        sendCenteredMessage(p, "§ePatientez quelques instants.");
        sendCenteredMessage(p, "§eAmélioration §aeffectuée §e!");
        sendCenteredMessage(p, "");
        sendCenteredMessage(p, "§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        sendCenteredMessage(p, "");
    }

    public static void startProtectorGeneration(Player p) {
        p.sendMessage("");
        p.sendMessage("§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        p.sendMessage("");
        p.sendMessage("§eLa machine §6génère §evotre §6Protecteur§e...");
        p.sendMessage("§ePatientez quelques instants.");
        p.sendMessage("§eGénération §aeffectuée §e!");
        p.sendMessage("");
        p.sendMessage("§6§kOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        p.sendMessage("");
    }

    public static void sendJson(Player p, String msg, String hover, String extra) {
        TextComponent tc = new TextComponent(msg);
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, extra));
        p.spigot().sendMessage(tc);
    }

    public static TextComponent getJson(String msg, String hover, String extra) {
        TextComponent tc = new TextComponent(msg);
        tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, extra));
        return  tc;
    }

    private final static int CENTER_PX = 154;

    public static void sendCenteredMessage(Player player, String message){
        if(message == null || message.equals("")) {
            player.sendMessage("");
            return;
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        message = PlaceholderAPI.setPlaceholders(player, message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()){
            if(c == '§'){
                previousCode = true;
                continue;
            }else if(previousCode == true){
                previousCode = false;
                if(c == 'l' || c == 'L'){
                    isBold = true;
                    continue;
                }else isBold = false;
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate){
            sb.append(" ");
            compensated += spaceLength;
        }
        player.sendMessage(sb.toString() + message);
    }
}
