package fr.floxiik.skycore.listeners;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class CreeperListeners implements Listener {

    @EventHandler
    public void onCharge(PlayerInteractEntityEvent e) {
        if(e.getRightClicked().getType() == EntityType.CREEPER) {
            Creeper creeper = (Creeper) e.getRightClicked();
            if(!creeper.isPowered()) {
                if (e.getPlayer().getItemInHand().hasItemMeta()) {
                    if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("§eCharge de §aCreeper")) {
                        e.setCancelled(true);
                        ItemStack inHand = e.getPlayer().getItemInHand();
                        String display = inHand.getItemMeta().getDisplayName();
                        int coups = Integer.parseInt(display.substring(27, 28));
                        if (coups > 1) {
                            ItemMeta meta = inHand.getItemMeta();
                            meta.setDisplayName("§eCharge de §aCreeper §7(§c" + (coups - 1) + " coups§7)");
                            e.getPlayer().getItemInHand().setItemMeta(meta);
                        } else {
                            e.getPlayer().setItemInHand(new ItemStack(Material.AIR, 1));
                        }
                        e.getRightClicked().getWorld().strikeLightningEffect(e.getRightClicked().getLocation());
                        creeper = (Creeper) e.getRightClicked();
                        creeper.setPowered(true);
                        creeper.setCustomName("§e§lCreeper §c§lChargé");
                    }
                }
            }
        }
    }
}
