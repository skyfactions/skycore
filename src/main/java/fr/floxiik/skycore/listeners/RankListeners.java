package fr.floxiik.skycore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class RankListeners implements Listener {

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        String[] pseudo1 = e.getInventory().getTitle().split("§");
        String target = pseudo1[pseudo1.length-1].substring(1).toLowerCase();

        if (!e.getInventory().getTitle().startsWith("§0§lRang")) return;
        e.setCancelled(true);
        final ItemStack clickedItem = e.getCurrentItem();

        // verify current item is not null
        if (clickedItem == null || clickedItem.getType() == Material.STAINED_GLASS_PANE) return;

        final Player p = (Player) e.getWhoClicked();

        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Pilote")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set default");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Sergent") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set sergent");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Major") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set major");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Aspirant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set aspirant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Lieutenant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set lieutenant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Capitaine") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set capitaine");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Amiral") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set amiral");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("You") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set youtubeur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Graphiste") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set graphiste");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Rédacteur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set rédacteur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Assistant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set assistant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Modérateur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set modérateur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Développeur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set développeur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Architecte") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set architecte");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Responsable") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set responsable");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Administrateur") && e.getWhoClicked().hasPermission("skyfactions.admin")) {
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set admin");
        } else {
            p.sendMessage("§c§lErreur §8» §eVous n'avez pas la permission !");
            p.closeInventory();
        }
    }
}
