package fr.floxiik.skycore.listeners;

import fr.floxiik.skycore.events.DragonManager;
import fr.floxiik.skycore.events.EventManager;
import fr.floxiik.skycore.utils.RandomGenerator;
import fr.maxlego08.zkoth.api.event.events.KothSpawnEvent;
import fr.maxlego08.zkoth.api.event.events.KothStopEvent;
import fr.maxlego08.zkoth.api.event.events.KothWinEvent;
import fr.maxlego08.znexus.events.NexusSpawnEvent;
import fr.maxlego08.znexus.events.NexusStopEvent;
import fr.maxlego08.znexus.events.NexusWinEvent;
import fr.maxlego08.ztotem.events.TotemSpawnEvent;
import fr.maxlego08.ztotem.events.TotemStopEvent;
import fr.maxlego08.ztotem.events.TotemWinEvent;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class DragonListener implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        Entity damager = event.getDamager();
        Player player = null;
        if (entity.getType() == EntityType.ENDER_CRYSTAL) {
            if (event.getDamager() instanceof Arrow) {
                if (((Arrow) event.getDamager()).getShooter() instanceof Player) {
                    player = (Player) ((Arrow) event.getDamager()).getShooter();
                    if (DragonManager.points.containsKey(player)) {
                        DragonManager.points.replace(player, DragonManager.points.get(player) + 50);
                    } else {
                        DragonManager.points.put(player, 50);
                    }
                }
            }
        } else if (entity instanceof EnderDragon) {
            if (event.getDamager() instanceof Player) {
                player = (Player) event.getDamager();
                if (DragonManager.points.containsKey(player)) {
                    DragonManager.points.replace(player, (int) (DragonManager.points.get(player) + event.getDamage() * 5));
                } else {
                    DragonManager.points.put(player, (int) (event.getDamage() * 5));
                }
            } else if (event.getDamager() instanceof Arrow) {
                if (((Arrow) event.getDamager()).getShooter() instanceof Player) {
                    player = (Player) ((Arrow) event.getDamager()).getShooter();
                    if (DragonManager.points.containsKey(player)) {
                        DragonManager.points.replace(player, (int) (DragonManager.points.get(player) + event.getDamage() * 5));
                    } else {
                        DragonManager.points.put(player, (int) (event.getDamage() * 5));
                    }
                }
            }
            double newDamage = event.getDamage() / 5;
            event.setDamage(newDamage);
            DragonManager.health = ((EnderDragon) entity).getHealth()*5 - newDamage;
        }
    }

    @EventHandler
    public void dragonDeath(EntityDeathEvent event) {
        if(event.getEntity() instanceof EnderDragon) {
            DragonManager.health = 0;
            DragonManager.winDragon();
        }
    }

    @EventHandler
    public void dragonHeal(EntityRegainHealthEvent event) {
        if(event.getEntity() instanceof EnderDragon) {
            DragonManager.health = ((EnderDragon) event.getEntity()).getHealth() * 5;
        }
    }

    @EventHandler
    public void endPortalCreate(EntityCreatePortalEvent event) {
        event.setCancelled(true);
    }
}
