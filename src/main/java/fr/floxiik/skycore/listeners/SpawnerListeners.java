package fr.floxiik.skycore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

public class SpawnerListeners implements Listener {

    @EventHandler
    public void blockBreak(BlockBreakEvent e) {
        if(e.getBlock().getType() == Material.MOB_SPAWNER) {
            if (e.getPlayer().getItemInHand().hasItemMeta()) {
                if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().contains("§ePioche à §aSpawners")) {
                    ItemStack inHand = e.getPlayer().getItemInHand();
                    String display = inHand.getItemMeta().getDisplayName();
                    int coups = Integer.parseInt(display.substring(27, 28));
                    if (coups > 1) {
                        ItemMeta meta = inHand.getItemMeta();
                        meta.setDisplayName("§ePioche à §aSpawners §7(§c" + (coups - 1) + " coups§7)");
                        e.getPlayer().getItemInHand().setItemMeta(meta);
                    } else {
                        e.getPlayer().setItemInHand(new ItemStack(Material.AIR, 1));
                    }
                    CreatureSpawner spawner_type = (CreatureSpawner) e.getBlock().getState();
                    e.setCancelled(true);
                    e.getBlock().setType(Material.AIR);
                    EntityType entityType = spawner_type.getSpawnedType();
                    ItemStack spawner = new ItemStack(Material.MOB_SPAWNER, 1);
                    BlockStateMeta meta = (BlockStateMeta) spawner.getItemMeta();
                    spawner_type = (CreatureSpawner) meta.getBlockState();
                    spawner_type.setSpawnedType(entityType);
                    meta.setBlockState(spawner_type);
                    meta.setDisplayName("§eSpawner §7(§c" + spawner_type.getCreatureTypeName() + "§7)");
                    spawner.setItemMeta(meta);
                    e.getPlayer().getWorld().dropItemNaturally(e.getBlock().getLocation(), spawner);
                }
            }
        }
    }

    @EventHandler
    public void onPlaceSpawner(BlockPlaceEvent event) {
        if (event.getBlock().getType() == Material.MOB_SPAWNER) {
            CreatureSpawner spawner = (CreatureSpawner) event.getBlock().getState();
            spawner.setCreatureTypeByName(event.getItemInHand().getItemMeta().getDisplayName().replace("§eSpawner §7(§c", "").replace("§7)", "").toUpperCase());
            event.getPlayer().sendMessage("§c§lFaction§f│ §eVous venez de §aposer§e un spawner à §b" + spawner.getCreatureTypeName() + " §e!");
        }
    }
}
