package fr.floxiik.skycore.listeners;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.events.EventManager;
import fr.floxiik.skycore.utils.RandomGenerator;
import fr.maxlego08.zkoth.ZKoth;
import fr.maxlego08.zkoth.ZKothPlugin;
import fr.maxlego08.zkoth.api.event.events.KothSpawnEvent;
import fr.maxlego08.zkoth.api.event.events.KothStopEvent;
import fr.maxlego08.zkoth.api.event.events.KothWinEvent;
import fr.maxlego08.znexus.NexusManager;
import fr.maxlego08.znexus.events.NexusSpawnEvent;
import fr.maxlego08.znexus.events.NexusStopEvent;
import fr.maxlego08.znexus.events.NexusWinEvent;
import fr.maxlego08.ztotem.Totem;
import fr.maxlego08.ztotem.TotemManager;
import fr.maxlego08.ztotem.ZTotemPlugin;
import fr.maxlego08.ztotem.events.TotemRegisterEvent;
import fr.maxlego08.ztotem.events.TotemSpawnEvent;
import fr.maxlego08.ztotem.events.TotemStopEvent;
import fr.maxlego08.ztotem.events.TotemWinEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class EventListener implements Listener {

    public static World eventWorld = null;

    @EventHandler
    public void onTpCommand(PlayerCommandPreprocessEvent event) {
        if(event.getMessage().startsWith("/event")) {
            event.setCancelled(true);
            if(!EventManager.getActualLocations().isEmpty()) {
                if(EventManager.getActualLocations().size() == 1) {
                    event.getPlayer().teleport(EventManager.getActualLocations().get(0));
                } else {
                    event.getPlayer().teleport(EventManager.getActualLocations().get(RandomGenerator.between(0, EventManager.getActualLocations().size() - 1)));
                }
            } else {
                event.getPlayer().sendMessage("§c§lFaction §8» §eAucun §6event §en'est en cours.");
            }
        }
    }

    @EventHandler
    public void onTotemSpawn(TotemSpawnEvent event) {
        if(eventWorld == null) {
            eventWorld = event.getLocation().getWorld();
            EventManager.setActualLocations(eventWorld);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onNexusSpawn(NexusSpawnEvent event) {
        if(eventWorld == null) {
            eventWorld = event.getLocation().getWorld();
            EventManager.setActualLocations(eventWorld);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onKothStart(KothSpawnEvent event) {
        if(eventWorld == null) {
            event.setCancelled(true);
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            Bukkit.dispatchCommand(console, "koth now " + event.getKoth().getName());
            eventWorld = event.getKoth().getCenter().getWorld();
            EventManager.setActualLocations(eventWorld);
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onTotemStop(TotemStopEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getLocation().getWorld());
    }

    @EventHandler
    public void onTotemWin(TotemWinEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getLocation().getWorld());
    }

    @EventHandler
    public void onNexusStop(NexusStopEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getLocation().getWorld());
    }

    @EventHandler
    public void onNexusWin(NexusWinEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getLocation().getWorld());
    }

    @EventHandler
    public void onKothStop(KothStopEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getKoth().getCenter().getWorld());
    }

    @EventHandler
    public void onKothWin(KothWinEvent event) {
        eventWorld = null;
        EventManager.finishEvent(event.getKoth().getCenter().getWorld());
    }
}
