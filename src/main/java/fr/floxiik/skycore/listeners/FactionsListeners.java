package fr.floxiik.skycore.listeners;

import com.massivecraft.factions.event.FactionCreateEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.event.FactionRenameEvent;
import com.sk89q.worldedit.MaxChangedBlocksException;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.CrewCreation;
import fr.floxiik.skycore.crew.CrewDemolition;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.io.File;

public class FactionsListeners implements Listener {

    private int[] fac_space = {80, 96, 112, 128};
    private int start_height = 64;
    private File plot;
    private static File empty_plot;
    private Location loc;

    @EventHandler
    public void onCreate(FactionCreateEvent e) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                try {
                    CrewCreation.init(e.getFactionTag(), e.getFPlayer().getPlayer());
                } catch (MaxChangedBlocksException | NoSuchFieldException | IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
        }, 20 * 3L);
    }

    @EventHandler
    public void onDisband(FactionDisbandEvent e) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                CrewDemolition.destroy(e.getFaction().getTag(), e.getPlayer());
            }
        }, 20 * 3L);
    }

    @EventHandler
    public void onTagChange(FactionRenameEvent e) {
        e.setCancelled(true);
        e.getfPlayer().getPlayer().sendMessage("§c§lErreur §8» §eVous ne pouvez pas changer le nom de votre faction !");
    }
}
