package fr.floxiik.skycore.listeners;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Factions;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.bukkit.event.EventPriority.*;

public class HammerListeners implements Listener {

    Map<UUID, BlockFace> lastBlockFace = new HashMap<>();

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        ItemStack i = p.getItemInHand();
        if(i.hasItemMeta()) {
            if(i.getItemMeta().hasDisplayName()) {
                if ((i.getType() == Material.DIAMOND_PICKAXE) && (i.getItemMeta().getDisplayName().contains("§c§lHammer"))) {
                    if (this.lastBlockFace.get(p.getUniqueId()) == null || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.DOWN || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.UP) {
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH_EAST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.EAST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH_EAST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH_WEST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.WEST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH_WEST), p);
                    }
                    if (this.lastBlockFace.get(p.getUniqueId()) == BlockFace.EAST || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.WEST) {
                        processBlock(i, e.getBlock().getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.DOWN), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH).getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.NORTH).getRelative(BlockFace.DOWN), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.SOUTH).getRelative(BlockFace.DOWN), p);
                    }
                    if (this.lastBlockFace.get(p.getUniqueId()) == BlockFace.NORTH || this.lastBlockFace.get(p.getUniqueId()) == BlockFace.SOUTH) {
                        processBlock(i, e.getBlock().getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.DOWN), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.EAST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.WEST), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.EAST).getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.EAST).getRelative(BlockFace.DOWN), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.WEST).getRelative(BlockFace.UP), p);
                        processBlock(i, e.getBlock().getRelative(BlockFace.WEST).getRelative(BlockFace.DOWN), p);
                    }
                }
            }
        }
    }

    @EventHandler(priority = HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.LEFT_CLICK_BLOCK)
            this.lastBlockFace.put(e.getPlayer().getUniqueId(), e.getBlockFace());
    }

    private void processBlock(ItemStack i, Block b, Player p) {
        if (Board.getInstance().getFactionAt(new FLocation(b.getLocation())) != FPlayers.getInstance().getByPlayer(p).getFaction()) {
            if (Board.getInstance().getFactionAt(new FLocation(b.getLocation())) != Factions.getInstance().getWilderness()) {
                return;
            }
        }
        if (b.isLiquid())
            return;
        if (b.getType() == Material.STONE || b.getType() == Material.IRON_ORE || b.getType() == Material.GOLD_ORE || b.getType() == Material.DIAMOND_ORE || b.getType() == Material.REDSTONE_ORE || b.getType() == Material.EMERALD_ORE || b.getType() == Material.COBBLESTONE || b.getType() == Material.COBBLESTONE_STAIRS || b.getType() == Material.MOSSY_COBBLESTONE || b.getType() == Material.RED_SANDSTONE || b.getType() == Material.SANDSTONE || b.getType() == Material.SANDSTONE_STAIRS || b.getType() == Material.RED_SANDSTONE_STAIRS || b.getType() == Material.COAL_ORE || b.getType() == Material.PRISMARINE || b.getType() == Material.NETHERRACK || b.getType() == Material.QUARTZ_ORE || b.getType() == Material.QUARTZ_BLOCK || b.getType() == Material.QUARTZ_STAIRS) {

        } else {
            return;
        }


        b.breakNaturally(i);
    }
}
