package fr.floxiik.skycore.listeners;

import com.Zrips.CMI.CMI;
import de.moritzerhard.dispatchcommands.bukkit.api.DispatchAPI;
import fr.floxiik.skycore.utils.RandomGenerator;
import fr.floxiik.skycore.utils.StaffMode;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class StaffModeListeners implements Listener {

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            StaffMode.enterStaffMode(event.getPlayer());
        }
        if(StaffMode.isFreeze(event.getPlayer())) {
            DispatchAPI.runCommandOnBungeecord("ban " + event.getPlayer().getName() + " 30 d Refus d'Optempérer");
        }
    }

    @EventHandler
    public void onDisable(PluginDisableEvent event) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (StaffMode.isStaffMode(p)) {
                StaffMode.enterStaffMode(p);
            }
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (StaffMode.isFreeze(event.getPlayer())) {
            event.getPlayer().teleport(StaffMode.freezeLoc(event.getPlayer()));
            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 5 * 20, 9));
            CMI.getInstance().getTitleMessageManager().send(event.getPlayer(), "&f❅ &bFreeze &f❅", "&eVous êtes freeze, vous êtes &6attendu &een vocal sur &9discord &e!");
            event.getPlayer().sendMessage("§c§lModération§f│ §eVous êtes §6attendu §esur §9discord §een vocal ! §adiscord.gg/skyfactions");
            event.getPlayer().sendMessage("§c§lModération§f│ §eSi vous admettez tricher, faites §c/admet §epour réduire votre peine !");
        }
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (event.getPlayer().hasPermission("skycore.staffmode.use")) {
            if (event.getMessage().startsWith("/staffmode") || event.getMessage().startsWith("/mod")) {
                event.setCancelled(true);
                StaffMode.enterStaffMode(event.getPlayer());
            }
        }
        if (event.getPlayer().hasPermission("skycore.staffmode.freeze")) {
            if (event.getMessage().startsWith("/freeze")) {
                event.setCancelled(true);
                StaffMode.freeze(Bukkit.getPlayer(event.getMessage().replace("/freeze ", "")));
            }
        }
        if (event.getPlayer().hasPermission("skycore.staffmode.randomtp")) {
            if (event.getMessage().startsWith("/randomtpsm")) {
                event.setCancelled(true);
                ArrayList<Player> players = new ArrayList<>();
                players.addAll(Bukkit.getOnlinePlayers());
                Player tp = players.get(RandomGenerator.between(0, players.size() - 1));
                event.getPlayer().teleport(tp);
                event.getPlayer().sendMessage("§c§lModération§f│ §eTéléportation §eau joueur §a" + tp.getName() + " §e..");
            }
        }
        if(event.getMessage().startsWith("/admet")) {
            if (StaffMode.isFreeze(event.getPlayer())) {
                event.setCancelled(true);
                StaffMode.freeze(event.getPlayer());
                DispatchAPI.runCommandOnBungeecord("ban " + event.getPlayer().getName() + " 15 d Aveux");
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            if (event.getRightClicked() instanceof Player) {
                if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§eBâton de §b§lFreeze")) {
                    event.setCancelled(true);
                    if (StaffMode.isFreeze((Player) event.getRightClicked())) {
                        event.getPlayer().sendMessage("§c§lModération§f│ §eVous venez d'§bunfreeze §a" + event.getRightClicked().getName() + " §e!");
                    } else {
                        event.getPlayer().sendMessage("§c§lModération§f│ §eVous venez de §bfreeze §a" + event.getRightClicked().getName() + " §e!");
                    }
                    event.getPlayer().chat("/freeze " + event.getRightClicked().getName());
                }
                if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§eMenu des §c§lSanctions")) {
                    event.setCancelled(true);
                    if (event.getPlayer().hasPermission("skycore.staffmode.ss")) {
                        event.getPlayer().chat("/ss " + event.getRightClicked().getName());
                    } else if (event.getPlayer().hasPermission("skycore.staffmode.rs")) {
                        event.getPlayer().chat("/rs " + event.getRightClicked().getName());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInteract2(PlayerInteractEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§eBoussole de §a§lTéléportation")) {
                event.setCancelled(true);
                event.getPlayer().chat("/randomtpsm");
            }
            if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§cQuitter le StaffMode")) {
                event.setCancelled(true);
                StaffMode.enterStaffMode(event.getPlayer());
            }
        }
    }

    @EventHandler
    public void onInteract3(InventoryInteractEvent event) {
        if (StaffMode.isStaffMode((Player) event.getWhoClicked())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract3(InventoryClickEvent event) {
        if (StaffMode.isStaffMode((Player) event.getWhoClicked())) {
            if (event.getClickedInventory() instanceof PlayerInventory) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInteract4(InventoryDragEvent event) {
        if (StaffMode.isStaffMode((Player) event.getWhoClicked())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract4(PlayerDropItemEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract4(PlayerPickupItemEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract4(BlockPlaceEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract4(BlockBreakEvent event) {
        if (StaffMode.isStaffMode(event.getPlayer())) {
            event.setCancelled(true);
        }
    }
}
