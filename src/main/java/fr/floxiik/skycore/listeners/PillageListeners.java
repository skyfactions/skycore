package fr.floxiik.skycore.listeners;

import com.Zrips.CMI.CMI;
import com.massivecraft.factions.*;
import com.vonage.client.VonageClient;
import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.bonus.Spectator;
import fr.floxiik.skycore.dao.AlertDAO;
import fr.floxiik.skycore.utils.TelNumber;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.events.DiscordPrivateMessageSentEvent;
import github.scarsz.discordsrv.dependencies.jda.api.EmbedBuilder;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.awt.*;
import java.util.HashMap;

import static github.scarsz.discordsrv.util.DiscordUtil.getJda;

public class PillageListeners implements Listener {

    private static HashMap<String, Float> alerts = new HashMap<>();
    private static HashMap<String, Long> pillages = new HashMap<>();

    @EventHandler
    public void onPillage(PlayerMoveEvent event) {
        if (Action.isPillage()) {
            if (!Spectator.isSpectator(event.getPlayer())) {
                if (!Board.getInstance().getFactionAt(new FLocation(event.getTo())).getTag().equals(FPlayers.getInstance().getByPlayer(event.getPlayer()).getFaction().getTag())) {
                    Faction faction = Board.getInstance().getFactionAt(new FLocation(event.getTo()));
                    if (faction.getRelationTo(FPlayers.getInstance().getByPlayer(event.getPlayer())).isEnemy()) {
                        if (alerts.containsKey(faction.getTag())) {
                            alerts.replace(faction.getTag(), alerts.get(faction.getTag()) + 0.1f);
                        } else {
                            alerts.put(faction.getTag(), 0.1f);
                        }
                        if (alerts.get(faction.getTag()) >= 130) {
                            if(pillages.containsKey(faction.getTag())) {
                                if(pillages.get(faction.getTag()) >= (System.currentTimeMillis()+21600000)) {
                                    pillages.replace(faction.getTag(), System.currentTimeMillis());
                                    sendAlert(faction);
                                }
                            } else {
                                pillages.put(faction.getTag(), System.currentTimeMillis());
                                sendAlert(faction);
                            }
                            alerts.replace(faction.getTag(), 0f);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPillageBlockBreak(ExplosionPrimeEvent event) {
        if (Action.isPillage()) {
            Bukkit.broadcastMessage("nope");
            Faction faction = Board.getInstance().getFactionAt(new FLocation(event.getEntity().getLocation()));
            if (alerts.containsKey(faction.getTag())) {
                alerts.replace(faction.getTag(), alerts.get(faction.getTag()) + 50f);
            } else {
                alerts.put(faction.getTag(), 50f);
            }
            if (alerts.get(faction.getTag()) >= 130) {
                if(pillages.containsKey(faction.getTag())) {
                    if(pillages.get(faction.getTag()) >= (System.currentTimeMillis()+21600000)) {
                        pillages.replace(faction.getTag(), System.currentTimeMillis());
                        sendAlert(faction);
                        Bukkit.broadcastMessage("nope1");
                    }
                } else {
                    pillages.put(faction.getTag(), System.currentTimeMillis());
                    sendAlert(faction);
                    Bukkit.broadcastMessage("nope2");
                }
                alerts.replace(faction.getTag(), 0f);
            }
        }
    }

    public void sendAlert(Faction f) {
        VonageClient client = new VonageClient.Builder().apiKey("79073e64").apiSecret("13pf3gL42HWLOQvw").build();
        AlertDAO alertDAO = new AlertDAO();
        TelNumber telNumber = null;

        // In-Game alert
        for(Player p : f.getOnlinePlayers()) {
            if(p.hasPermission("skycore.alert.pillage")) {
                CMI.getInstance().getTitleMessageManager().send(p, "§c§lAssault", "§eVotre §6base §ese fait §aattaquer §e!");
                p.sendMessage("§c§lFaction §8» §eVotre §6base §ese fait §aattaquer §e!");
                p.playSound(p.getLocation(), Sound.WITHER_SPAWN, 10, 1);
            }
        }

        // Discord Alert
        for(FPlayer fp : f.getFPlayers()) {
            Player p = fp.getPlayer();
            if (p.hasPermission("skycore.alert.pillage")) {
                if(DiscordSRV.getPlugin().getAccountLinkManager().getLinkedAccounts().containsValue(p.getUniqueId())) {
                    String id = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(p.getUniqueId());
                    DiscordUtil.getUserById(id).openPrivateChannel().queue((privateChannel) -> {
                        privateChannel.sendMessage(getPillageString().build()).queue((sentMessage) -> {
                            DiscordPrivateMessageSentEvent var10000 = (DiscordPrivateMessageSentEvent)DiscordSRV.api.callEvent(new DiscordPrivateMessageSentEvent(getJda(), sentMessage));
                        });
                    });
                }
            }
        }

        // Sms Alert
        /*for(FPlayer fp : f.getFPlayers()) {
            Player p = fp.getPlayer();
            if(p.hasPermission("skycore.alert.pillage")) {
                telNumber = alertDAO.getTel(p.getName());
                try {
                    String tel = telNumber.getNumber();
                    if (telNumber.getRequestId().equals("verified")) {
                        Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
                            @Override
                            public void run() {
                                TextMessage message = new TextMessage("SkyFactions", "33" + tel.substring(1),
                                        "\u26A0 Alerte Pillage \u26A0 \n\nVotre base subit probablement l'attaque d'une faction ennemie, venez défendre !", true);
                                SmsSubmissionResponse responses = client.getSmsClient().submitMessage(message);
                                for (SmsSubmissionResponseMessage responseMessage : responses.getMessages()) {
                                    System.out.println(message);
                                }
                            }
                        });
                    }
                } catch (NullPointerException ignored) {

                }
            }
        }*/
    }

    public EmbedBuilder getPillageString() {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setFooter("© SkyFactions");
        embedBuilder.setColor(Color.red);
        embedBuilder.setTitle("⚠ Alerte Pillage ⚠");
        embedBuilder.setThumbnail("https://i.ibb.co/kBXwnTk/tnt-block.png");
        embedBuilder.setDescription("Votre base subit probablement l'attaque d'une faction ennemie, venez défendre !");
        return embedBuilder;
    }
}
