package fr.floxiik.skycore.listeners;

import com.Zrips.CMI.CMI;
import com.Zrips.CMI.Modules.Permissions.PermissionsManager;
import fr.floxiik.skycore.help.PlayerHelp;
import fr.floxiik.skycore.utils.ChatFormat;
import fr.mrtigreroux.tigerreports.TigerReports;
import fr.mrtigreroux.tigerreports.objects.Report;
import me.clip.placeholderapi.PlaceholderAPI;
import me.devtec.theapi.TheAPI;
import me.devtec.theapi.cooldownapi.CooldownAPI;
import me.lucko.luckperms.bukkit.LPBukkitPlugin;
import me.lucko.luckperms.common.api.LuckPermsApiProvider;
import me.lucko.luckperms.common.cacheddata.UserCachedDataManager;
import me.lucko.luckperms.common.plugin.LuckPermsPlugin;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.context.ContextManager;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.chat.Chat;
import org.apache.logging.log4j.core.net.Priority;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ChatListeners implements Listener {

    LuckPerms api;

    @EventHandler(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent event) {
        if(event.isCancelled()) return;
        event.setCancelled(true);
        Player player = event.getPlayer();
        /*if(cooldowns.containsKey(player)) {
            if(cooldowns.get(player) + 3000 > System.currentTimeMillis()) {
                player.sendMessage("§c§lFaction §8» §eVous devez attendre §c3s §eavant de §6ré-écrire§e un message !");
                return;
            } else {
                cooldowns.remove(player);
            }
        } else {
            cooldowns.put(player, System.currentTimeMillis());
        }*/

        if(!TheAPI.getCooldownAPI(player).expired("chat")) {
            player.sendMessage("§c§lErreur§f│ §eVous devez attendre §c" + (TheAPI.getCooldownAPI(player).getTimeToExpire("chat")/40) + "s §eavant de §6ré-écrire§e un message !");
            return;
        }
        TheAPI.getCooldownAPI(player).createCooldown("chat", player.hasPermission("skycore.bypass.all") ? 0 : 3*40);

        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            api = provider.getProvider();
        }
        String message = event.getMessage();
        User user = api.getUserManager().getUser(player.getName());
        ContextManager cm = api.getContextManager();
        QueryOptions queryOptions = cm.getQueryOptions(user).orElse(cm.getStaticQueryOptions());

        //test
        
        String format;
        switch (user.getPrimaryGroup().toString()) {
            case "admin":
                format = ChatFormat.ADMIN.toString();
                break;
            case "responsable":
                format = ChatFormat.ADMIN.toString();
                break;
            case "développeur":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "manager":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "designer":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "rédacteur":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "modérateur":
                format = ChatFormat.MODO.toString();
                break;
            case "assistant":
                format = ChatFormat.MODO.toString();
                break;
            case "architecte":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "animateur":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "graphiste":
                format = ChatFormat.BASICSTAFF.toString();
                break;
            case "youtubeur":
                format = ChatFormat.VIP.toString();
                break;
            case "amiral":
                format = ChatFormat.VIP.toString();
                break;
            case "capitaine":
                format = ChatFormat.VIP.toString();
                break;
            case "lieutenant":
                format = ChatFormat.VIP.toString();
                break;
            case "aspirant":
                format = ChatFormat.VIP.toString();
                break;
            case "major":
                format = ChatFormat.VIP.toString();
                break;
            case "sergent":
                format = ChatFormat.VIP.toString();
                break;
            default:
                format = ChatFormat.DEFAULT.toString();
                break;
        }
        String[] strings = format.split("&8»");
        char chatColor = strings[1].substring(1, 2).charAt(0);
        String playerFormat = strings[0];
        playerFormat = playerFormat.replace("&8[&7#&e%zfactionranking_me_rank%&8] %rel_factionsuuid_relation_color%%factionsuuid_player_role%%factionsuuid_faction_name% ", "");
        playerFormat = playerFormat.replace("{prefix}", user.getCachedData().getMetaData(queryOptions).getPrefix());
        playerFormat = playerFormat.replace("{displayName}", player.getDisplayName());
        playerFormat = PlaceholderAPI.setPlaceholders(player, playerFormat);
        playerFormat = ChatColor.translateAlternateColorCodes('&', playerFormat);
        String infoHover = getInfos();
        infoHover = infoHover.replace("{prefix}", user.getCachedData().getMetaData(queryOptions).getPrefix());
        infoHover = infoHover.replace("{displayName}", player.getDisplayName());
        infoHover = PlaceholderAPI.setPlaceholders(player, infoHover);
        TextComponent playerInfo =
                PlayerHelp.getJson(
                        playerFormat,
                        infoHover,
                        "/msg " + player.getName());
        playerInfo.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg " + player.getName()));
        String messageFormat = strings[strings.length-1];
        messageFormat = messageFormat.replace("{message}", message);
        messageFormat = ChatColor.translateAlternateColorCodes('&', messageFormat);
        if(chatColor == '7') {
            messageFormat = ChatColor.stripColor(messageFormat);
            messageFormat = "&8»&7" + messageFormat;
        } else if(chatColor == 'f') {
            messageFormat = "&8»&f" + messageFormat;
        } else {
            messageFormat = "&8»" + messageFormat;
        }
        messageFormat = ChatColor.translateAlternateColorCodes('&', messageFormat);
        TextComponent messageReport =
                PlayerHelp.getJson(
                        messageFormat,
                        "§8(§4⚠§8) §cClique pour report !",
                        "/report " + player.getName() + " Message inapproprié"/* + message + ")"*/);
        messageReport.setColor(net.md_5.bungee.api.ChatColor.getByChar(chatColor));
        TextComponent finalMessage = new TextComponent("");
        for (Player p : Bukkit.getOnlinePlayers()) {
            String factionFormat;
            if(format.contains("%rel_")) {
                factionFormat = strings[0].replace(" {prefix}{displayName}%deluxetags_tag%", "");
                factionFormat = PlaceholderAPI.setRelationalPlaceholders(event.getPlayer(), p, factionFormat);
            } else {
                factionFormat = strings[0].replace("{prefix}{displayName}%deluxetags_tag% ", "");
            }
            factionFormat = PlaceholderAPI.setPlaceholders(event.getPlayer(), factionFormat);
            TextComponent factionInfo = new TextComponent(factionFormat);
            /*TextComponent */finalMessage = new TextComponent("");
            finalMessage.addExtra(factionInfo);
            finalMessage.addExtra(playerInfo);
            finalMessage.addExtra(messageReport);
            p.spigot().sendMessage(finalMessage);
            TigerReports.getInstance().getUsersManager().getOnlineUser(player).updateLastMessages(event.getMessage());
        }
        //CMI.getInstance().getChatFormatManager().sendMessage("console", player.getName(), finalMessage.toString());
        //CMI.getInstance().getChatFormatManager().broadcastBungeeMessage("hub", finalMessage.getFontRaw());
        for (Map.Entry<String, com.Zrips.CMI.Modules.BungeeCord.BungeeCordServer> stringBungeeCordServerEntry : CMI.getInstance().getBungeeCordManager().getServers().entrySet()) {
            Map.Entry var13 = (Map.Entry) stringBungeeCordServerEntry;
            CMI.getInstance().getBungeeCordManager().sendPublicMessage((String) var13.getKey(), player.getName(), finalMessage.toPlainText());
        }
    }

    public String getInfos() {
        return "§8§l»§7§m-----------------------§8§l«\n"
                + "\n"
                + "  §f• §eJoueur: §b{prefix}{displayName}\n"
                + "  §f• §eArgent: §a%cmi_user_balance_formated%\n"
                + "  §f• §eFaction: §c%factionsuuid_faction_name%\n"
                + "  §f• §eNiveau d'île: %skycore_level%\n"
                + "  §f• §eKills: §4%statistic_player_kills%\n"
                + "  §f• §eVotes: §d%skycore_player_votes%\n"
                + "\n"
                + " §aClique pour envoyer un message\n"
                + "\n"
                + "§8§l»§7§m-----------------------§8§l«"
                ;
    }
}
