package fr.floxiik.skycore.listeners;

import fr.floxiik.skycore.SkyCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

public class HoeListeners implements Listener {

    @EventHandler
    public void onPlantBreak(final BlockBreakEvent e) {
        Player p = e.getPlayer();
        ItemStack i = p.getItemInHand();
        if ((i.getType() == Material.DIAMOND_HOE) && (i.getItemMeta().getDisplayName().contains("§c§lHoue"))) {
            Block block = e.getBlock();
            final Material farm = e.getBlock().getType();
            if (farm == Material.CROPS || farm == Material.CARROT || farm == Material.POTATO || farm == Material.NETHER_WARTS || farm == Material.SUGAR_CANE_BLOCK) {
                    final Location farmPos = block.getLocation();
                    final String stringWorld = block.getWorld().getName();
                    final Player player = e.getPlayer();
                    Material removeMat = Material.CROPS;
                    switch (farm) {
                        case CROPS:
                            break;
                        case CARROT:
                            removeMat = Material.CARROT;
                            break;
                        case POTATO:
                            removeMat = Material.POTATO;
                            break;
                        case NETHER_WARTS:
                            removeMat = Material.NETHER_WARTS;
                            break;
                        case SUGAR_CANE_BLOCK:
                            removeMat = Material.SUGAR_CANE_BLOCK;
                            break;
                    }
                    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
                    scheduler.scheduleSyncDelayedTask(SkyCore.getInstance(), new Runnable() {
                        public void run() {
                            Bukkit.getServer().getWorld(stringWorld).getBlockAt(farmPos).setType(farm);
                        }
                    }, 20*10L);
                }
        }
    }
}