package fr.floxiik.skycore.listeners;

import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.crew.guis.UpgradeGui;
import fr.floxiik.skycore.drop.DropChests;
import fr.floxiik.skycore.tech.LTListener;
import github.scarsz.discordsrv.DiscordSRV;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class RegisterListeners {

    public static void Events() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new FactionsListeners(), SkyCore.getInstance());
        pm.registerEvents(new HammerListeners(), SkyCore.getInstance());
        pm.registerEvents(new HoeListeners(), SkyCore.getInstance());
        pm.registerEvents(new PlayersListeners(), SkyCore.getInstance());
        pm.registerEvents(new UpgradeGui(1), SkyCore.getInstance());
        pm.registerEvents(new RankListeners(), SkyCore.getInstance());
        pm.registerEvents(new GrenadeListeners(), SkyCore.getInstance());
        pm.registerEvents(new LTListener(), SkyCore.getInstance());
        pm.registerEvents(new BridgeListeners(), SkyCore.getInstance());
        pm.registerEvents(new PillageListeners(), SkyCore.getInstance());
        pm.registerEvents(new SpawnerListeners(), SkyCore.getInstance());
        pm.registerEvents(new DropChests(), SkyCore.getInstance());
        pm.registerEvents(new VoteListeners(), SkyCore.getInstance());
        pm.registerEvents(new CreeperListeners(), SkyCore.getInstance());
        pm.registerEvents(new ChatListeners(), SkyCore.getInstance());
        pm.registerEvents(new WallListener(), SkyCore.getInstance());
        pm.registerEvents(new EventListener(), SkyCore.getInstance());
        pm.registerEvents(new DragonListener(), SkyCore.getInstance());
        pm.registerEvents(new StaffModeListeners(), SkyCore.getInstance());
        DiscordSRV.api.subscribe(new DiscordSRVListener());
    }
}
