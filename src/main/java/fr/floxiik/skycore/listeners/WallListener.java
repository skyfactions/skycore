package fr.floxiik.skycore.listeners;

import com.massivecraft.factions.*;
import fr.floxiik.skycore.utils.WallsBlocks;
import fr.maxlego08.ztotem.Totem;
import fr.maxlego08.ztotem.events.TotemEvent;
import fr.maxlego08.ztotem.events.TotemRegisterEvent;
import fr.maxlego08.ztotem.events.TotemSpawnEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

public class WallListener implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (event.getBlock().getLocation().getWorld().getName().equals("faction_crew")) {
            if (event.getPlayer().getItemInHand().hasItemMeta()) {
                if (event.getPlayer().getItemInHand().getItemMeta().hasDisplayName()) {
                    if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§9§lObsidienne §eGenWall")) {
                        if (Board.getInstance().getFactionAt(new FLocation(event.getBlock().getLocation())) == FPlayers.getInstance().getByPlayer(event.getPlayer()).getFaction()) {
                            new WallsBlocks(event.getBlock().getLocation());
                        }
                    } else if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§6§lSable §eGenWall")) {
                        if (Board.getInstance().getFactionAt(new FLocation(event.getBlock().getLocation())) == FPlayers.getInstance().getByPlayer(event.getPlayer()).getFaction()) {
                            new WallsBlocks(event.getBlock().getLocation());
                        }
                    }
                }
            }
        }
    }
}
