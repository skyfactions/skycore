package fr.floxiik.skycore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GrenadeListeners implements Listener {

    private List<UUID> grenades = new ArrayList<>();

    @EventHandler
    public void launchGrenade(ProjectileLaunchEvent e) {
        if(e.getEntityType().equals(EntityType.SNOWBALL)) {
            if(e.getEntity().getShooter() instanceof Player) {
                Player p = (Player) e.getEntity().getShooter();
                if(p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("§c§lGrenade")) {
                    grenades.add(e.getEntity().getUniqueId());
                }
            }
        }
    }

    @EventHandler
    public void grenadeHit(ProjectileHitEvent e) {
        if(e.getEntity().getShooter() instanceof Player) {
            if(e.getEntityType().equals(EntityType.SNOWBALL)) {
                if(grenades.contains(e.getEntity().getUniqueId())) {
                    Location loc = e.getEntity().getLocation();
                    loc.getWorld().createExplosion(loc, 2f);
                    grenades.remove(e.getEntity().getUniqueId());
                }
            }
        }
    }
}
