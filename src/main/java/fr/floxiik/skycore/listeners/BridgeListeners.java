package fr.floxiik.skycore.listeners;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import fr.floxiik.skycore.SkyCore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BridgeListeners implements Listener {

    List<Player> bridgers = new ArrayList<Player>();

    @EventHandler
    public void onShoot(EntityShootBowEvent e) {
        /*Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {*/
        if (!e.isCancelled()) {
            if (e.getEntity() instanceof Player) {
                if (!bridgers.contains((Player) e.getEntity())) {
                    if (e.getBow().getItemMeta().getDisplayName().contains("§eArc à §aponts")) {
                        ItemStack inHand = ((Player) e.getEntity()).getItemInHand();
                        String display = inHand.getItemMeta().getDisplayName();
                        int coups = Integer.parseInt(display.substring(21, 22));
                        if (coups > 1) {
                            ItemMeta meta = inHand.getItemMeta();
                            meta.setDisplayName("§eArc à §aponts §7(§c" + (coups - 1) + " coups§7)");
                            inHand.setItemMeta(meta);
                        } else {
                            ((Player) e.getEntity()).setItemInHand(new ItemStack(Material.AIR, 1));
                        }
                        e.getBow().setDurability((short) (e.getBow().getDurability() + 128));
                        if (e.getBow().getDurability() <= 0) {
                            ((Player) e.getEntity()).setItemInHand(new ItemStack(Material.AIR));
                        }
                        bridgers.add((Player) e.getEntity());
                                /*long final_timestamp = new Timestamp(System.currentTimeMillis()).getTime() + 3000;
                                long during_timestamp = new Timestamp(System.currentTimeMillis()).getTime();
                                while (e.getProjectile().getLocation().getY() > 0 && !e.getProjectile().isOnGround() && e.getProjectile().getLocation().getChunk().isLoaded() && (during_timestamp < final_timestamp)) {
                                    during_timestamp = new Timestamp(System.currentTimeMillis()).getTime();
                                    if (Factions.getInstance().getWilderness().getId().equals(Board.getInstance().getFactionAt(new FLocation(e.getProjectile().getLocation())).getId()) && e.getProjectile().getLocation().getBlock().getType() == Material.AIR) {
                                        Location block = e.getProjectile().getLocation();
                                        Bukkit.getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
                                            @Override
                                            public void run() {
                                                block.getBlock().setType(Material.GLASS);
                                            }
                                        }, 20L);
                                    }
                                }*/

                        Arrow snowball = (Arrow) e.getProjectile();

                        BukkitRunnable bridgeTimer = new BukkitRunnable() {
                            int ticks = 0;

                            @Override
                            public void run() {
                                ticks += 2;
                                if (snowball.isDead() || snowball.getLocation().getY() < 1 || snowball.isOnGround() || !snowball.getLocation().getChunk().isLoaded()) {
                                    this.cancel();
                                    bridgers.remove((Player) e.getEntity());
                                    return;
                                }
                                if (ticks >= 60) {
                                    e.getProjectile().remove();
                                    bridgers.remove((Player) e.getEntity());
                                    this.cancel();
                                }
                                if (Board.getInstance().getFactionAt(new FLocation(snowball.getLocation())).isWilderness() && snowball.getLocation().getBlock().getType() == Material.AIR) {
                                    Location loc = snowball.getLocation();

                                    Bukkit.getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
                                        @Override
                                        public void run() {
                                            loc.add(0, -1, 0).getBlock().setType(Material.STONE);
                                            loc.add(1, 0, 0).getBlock().setType(Material.SANDSTONE);
                                            loc.add(-2, 0, 0).getBlock().setType(Material.SANDSTONE);
                                            loc.add(1, 0, 1).getBlock().setType(Material.SANDSTONE);
                                            loc.add(0, 0, -2).getBlock().setType(Material.SANDSTONE);
                                        }
                                    }, 20L);
                                }
                            }
                        };
                        bridgeTimer.runTaskTimer(SkyCore.getInstance(), 2L, 1L);
                    }
                }
            } else {
                e.setCancelled(true);
                e.getEntity().sendMessage("§c§lErreur §8» §eAttendez un peu avant de §6re-tirer §e!");
            }
        }
    }

    /*});
}*/
}
