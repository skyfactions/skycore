package fr.floxiik.skycore.listeners;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class VoteListeners implements Listener {

    @EventHandler
    public void onVote(VotifierEvent e) {
        CommandSender cs = Bukkit.getConsoleSender();
        Bukkit.dispatchCommand(cs, "vote add 1 " + e.getVote().getUsername());
    }
}
