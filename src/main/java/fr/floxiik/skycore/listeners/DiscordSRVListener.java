package fr.floxiik.skycore.listeners;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.AccountLinkedEvent;
import github.scarsz.discordsrv.api.events.DiscordGuildMessageReceivedEvent;
import github.scarsz.discordsrv.api.events.DiscordPrivateMessageReceivedEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Role;
import github.scarsz.discordsrv.dependencies.jda.api.entities.User;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;

public class DiscordSRVListener {

    public DiscordSRVListener() {}

    // SMS getContentStripped()
    // Discord getContentRaw()

    private long linkedAccounts = 816801630799265823L;
    private long allAccounts = 816804278630219777L;

    @Subscribe
    public void accountsLinked(DiscordGuildMessageReceivedEvent event) {
        if(event.getChannel().getIdLong() == 816801630799265823L) {
            for(String id : DiscordSRV.getPlugin().getAccountLinkManager().getLinkedAccounts().keySet()) {
                DiscordUtil.getUserById(id).openPrivateChannel().complete().sendMessage(event.getMessage().getContentRaw()).queue();
            }
        } else if(event.getChannel().getIdLong() == 816804278630219777L) {
           // for(event.getGuild().getMembers()
        }

        if(event.getMessage().getContentDisplay().startsWith("whitelist add ")) {
            if(event.getMember().getRoles().get(0).getName().contains("Admin") || event.getMember().getRoles().get(0).getName().contains("Commu") || event.getMember().getRoles().get(0).getName().contains("Resp")) {
                Bukkit.getOfflinePlayer(event.getMessage().getContentDisplay().replace("whitelist add ", "")).setWhitelisted(true);
                event.getMessage().getChannel().sendMessage(":white_check_mark: Joueur ajouté à la whitelist avec succès !").queue();
            } else {
                event.getMessage().getChannel().sendMessage(":negative_squared_cross_mark: Vous n'avez pas la permission !").queue();
            }
        }

        if(event.getMessage().getContentDisplay().startsWith("whitelist remove ")) {
            if(event.getMember().getRoles().get(0).getName().contains("Admin") || event.getMember().getRoles().get(0).getName().contains("Commu") || event.getMember().getRoles().get(0).getName().contains("Resp")) {
                Bukkit.getOfflinePlayer(event.getMessage().getContentDisplay().replace("whitelist remove ", "")).setWhitelisted(false);
                event.getMessage().getChannel().sendMessage(":white_check_mark: Joueur supprimé de la whitelist avec succès !").queue();
            } else {
                event.getMessage().getChannel().sendMessage(":negative_squared_cross_mark: Vous n'avez pas la permission !").queue();
            }
        }
    }
}
