package fr.floxiik.skycore.listeners;

import com.Zrips.CMI.CMI;
import com.Zrips.CMI.Modules.Economy.VaultManager;
import com.Zrips.CMI.Modules.Permissions.VaultHandler;
import com.boydti.fawe.bukkit.util.VaultUtil;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Factions;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.actions.Pause;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.bonus.Spectator;
import fr.floxiik.skycore.commands.ItemsCommands;
import fr.floxiik.skycore.crew.CrewUpgrade;
import fr.floxiik.skycore.crew.CrewsMoving;
import fr.floxiik.skycore.dao.EventDAO;
import fr.floxiik.skycore.help.PlayerHelp;
import fr.floxiik.skycore.events.EventManager;
import fr.floxiik.skycore.utils.RandomGenerator;
import fr.floxiik.skycore.utils.RemoveBlocks;
import fr.floxiik.skycore.utils.StaffMode;
import me.TechsCode.UltraPunishments.UltraPunishments;
import me.TechsCode.UltraPunishments.storage.types.IndexedPlayer;
import me.devtec.theapi.TheAPI;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.context.ContextManager;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.Vault;
import net.minecraft.server.v1_8_R3.BlockPistonMoving;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftChest;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;
import java.util.List;

public class PlayersListeners implements Listener {

    private Location spawn = new Location(Bukkit.getWorld("faction"), 4.5, 197.0, -2.5, 180, 0);
    private RemoveBlocks rb[] = new RemoveBlocks[3000];
    private HashMap<Player, Integer> glaive = new HashMap<>();
    private ArrayList<String> reportsMsg = new ArrayList<>();
    private int toRemove = 0;
    LuckPerms api;

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.teleport(spawn);
        if (player.getGameMode() == GameMode.SPECTATOR) {
            player.setGameMode(GameMode.SURVIVAL);
        }
        Spectator.setSpectator(player, false);
        Spectator.setCooldown(player, 0);
        if (!player.hasPlayedBefore()) {
            SkyCore.getInstance().addPlayerVote(player.getName(), 1);
        }

        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
        CMI.getInstance().getPlayer(player.getName()).setWalkSpeed(0.2f);

        if(player.hasPermission("skycore.joinmessage")) {
            RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
            if (provider != null) {
                api = provider.getProvider();
            }
            User user = api.getUserManager().getUser(player.getName());
            ContextManager cm = api.getContextManager();
            QueryOptions queryOptions = cm.getQueryOptions(user).orElse(cm.getStaticQueryOptions());
            Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', user.getCachedData().getMetaData(queryOptions).getPrefix() + player.getDisplayName() + " &6à rejoint le faction !"));
        }
    }

    @EventHandler
    public void onCloseDrop(InventoryCloseEvent event) {
        if(event.getPlayer().getLocation().getWorld().getName().contains("warzone")) {
            if (event.getInventory().getHolder() instanceof CraftChest) {
                if (Board.getInstance().getFactionAt(new FLocation(event.getPlayer().getLocation())).isWarZone()) {
                    ((CraftChest) event.getInventory().getHolder()).getBlock().breakNaturally();
                    Bukkit.broadcastMessage("§c§lLargage §8» §eUn §alargage §evient d'être §6récupéré §e!");
                }
            }
        }
    }

    @EventHandler
    public void onAttack(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            if (e.getEntity().getLocation().getWorld().getName().equals("faction_crew") && Action.getDayAction().equals("pause")) {
                e.setCancelled(true);
                e.getDamager().sendMessage("§c§lFaction §8» §eVous ne pouvez pas attaquer de joueurs ici en phase de §cPause §e!");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDeath(PlayerDeathEvent e) {
        if (e.getEntity().getKiller() != null) {
            e.setDeathMessage("§8(§e" + e.getEntity().getName() + " §7s'est fait tuer par §6" + e.getEntity().getKiller().getName() + "§8)");
        } else {
            e.setDeathMessage(null);
        }
        int c = 0;
        List<ItemStack> list = e.getDrops();
        Iterator<ItemStack> i = list.iterator();
        while (i.hasNext()) {
            ItemStack item = i.next();
            if (item.hasItemMeta()) {
                if (item.getItemMeta().hasDisplayName()) {
                    if (item.getItemMeta().getDisplayName().equals("§cGlaive")) {
                        i.remove();
                        c++;
                    }
                }
            }
        }
        if (c > 0) {
            glaive.put(e.getEntity(), c);
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        if (glaive.containsKey(e.getPlayer())) {
            for (int i = 0; i < glaive.get(e.getPlayer()); i++) {
                ItemsCommands.addGlaive(e.getPlayer());
            }
            glaive.remove(e.getPlayer());
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if(e.getBlock().getWorld().getName().equals("faction_crew")) {
            if(e.getBlock().getY() > 150) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§lFACTION§f│ §eVous ne pouvez pas §6poser §ede blocks au dessus de la couche §c150 §e!");
            }
        }
        if (Board.getInstance().getFactionAt(new FLocation(e.getBlock().getLocation())).isSafeZone() && Action.getDayAction().equals("pause")) {
            if (!e.getPlayer().isOp() || !e.getPlayer().hasPermission("skyfac.bypass.all")) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§lErreur §» §eVous ne pouvez pas poser de blocks ici ! §7(Attendez la phase de pillage)");
            }
        } else if (Board.getInstance().getFactionAt(new FLocation(e.getBlock().getLocation())) == Factions.getInstance().getSafeZone() && Action.getDayAction().equals("pillage")) {
            if (e.getPlayer().getItemInHand().hasItemMeta()) {
                if (e.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals("§c§lBlock d'Assault") && e.getPlayer().getWorld().getName().equals("faction_crew")) {
                    if (toRemove == 3000) {
                        toRemove = 0;
                        rb[toRemove] = new RemoveBlocks(e.getBlock().getLocation());
                    } else {
                        rb[toRemove] = new RemoveBlocks(e.getBlock().getLocation());
                        toRemove++;
                    }
                    e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount() - 1);
                } else {
                    if (!e.getPlayer().isOp() || !e.getPlayer().hasPermission("skyfac.bypass.all")) {
                        e.getPlayer().sendMessage("§c§lFaction §8» §eIl vous faut des blocks d'assault pour poser ici §7(/boutique) §e!");
                        e.setCancelled(true);
                    }
                }
            } else {
                if (!e.getPlayer().isOp() || !e.getPlayer().hasPermission("skyfac.bypass.all")) {
                    e.getPlayer().sendMessage("§c§lFaction §8» §eIl vous faut des blocks d'assault pour poser ici §7(/boutique) §e!");
                    e.setCancelled(true);
                }
            }
        }
        if (Board.getInstance().getFactionAt(new FLocation(e.getBlock().getLocation())).isWarZone()) {
            if (!e.getPlayer().isOp() || !e.getPlayer().hasPermission("skyfac.bypass.all")) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§lErreur §» §eVous ne pouvez pas poser de blocks ici !");
            }
        }
    }

    @EventHandler
    public void onPiston(BlockPistonExtendEvent event) {
        if(event.getBlock().getWorld().getName().equals("faction_crew")) {
            for(Block b : event.getBlocks()) {
                if(b.getY() > 149) {
                    b.setType(Material.AIR);
                }
            }
        }
    }

    @EventHandler
    public void onDrink(PlayerItemConsumeEvent e) {
        if (e.getItem().getType().equals(Material.MILK_BUCKET) && e.getPlayer().getLocation().getWorld().getName().equals("faction_crew")) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if(Board.getInstance().getFactionAt(new FLocation(event.getPlayer().getLocation())).isSafeZone() || Board.getInstance().getFactionAt(new FLocation(event.getBlockClicked().getLocation())).isSafeZone()) {
            event.setCancelled(true);
        }

    }
    @EventHandler
    public void onBlockFromTo(BlockFromToEvent event) {
        int id = event.getBlock().getTypeId();
        if(event.getBlock().getLocation().getWorld().getName().equals("faction_crew")) {
            if(Board.getInstance().getFactionAt(new FLocation(event.getToBlock().getLocation())).isSafeZone()) {
                if (id == 8 || id == 9 || id == 10 || id == 11) {
                    event.setCancelled(true);
                }
            }
            if(Board.getInstance().getFactionAt(new FLocation(event.getBlock().getLocation())).isSafeZone()) {
                event.getBlock().setType(Material.AIR);
            }
        }
    }

    public Location getSpawn() {
        return this.spawn;
    }

    @EventHandler
    public void test(PlayerCommandPreprocessEvent e) throws Exception {
        if (e.getMessage().startsWith("/ph") || e.getMessage().startsWith("/pluginhider")) {
            e.setCancelled(true);
        }
        if (e.getMessage().startsWith("/help") || e.getMessage().startsWith("/aide") || e.getMessage().startsWith("/wiki")) {
            e.setCancelled(true);
            TextComponent wiki = PlayerHelp.getJson("§c§lAide §8» §eClique ici pour §aaccéder §eau §cWiki §e!", "§aClique pour aller sur skyfactions.fr/wiki", "");
            wiki.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://skyfactions.fr/wiki"));
            e.getPlayer().spigot().sendMessage(wiki);
        }
        if(e.getMessage().startsWith("/addposevent")) {
            if(e.getPlayer().hasPermission("skycore.addeventpos")) {
                e.setCancelled(true);
                new EventDAO().addPos(e.getPlayer().getLocation());
                EventManager.addLocation(e.getPlayer().getLocation());
            }
        }
        if(e.getMessage().startsWith("/minage")) {
            e.setCancelled(true);
            e.getPlayer().performCommand("server minage");
        }
        if(e.getMessage().startsWith("/invest")) {
            e.setCancelled(true);
            e.getPlayer().performCommand("server invest");
        }
        if(e.getMessage().startsWith("/test")) {
            e.setCancelled(true);
            Bukkit.broadcastMessage(FPlayers.getInstance().getByPlayer(e.getPlayer()).getRole().getRoleCapitalized());
        }
        if(e.getMessage().startsWith("/updatetest")) {
            e.setCancelled(true);
            CrewUpgrade.upgrade(e.getPlayer());
        }
        if(e.getMessage().startsWith("/motest")) {
            e.setCancelled(true);
            Pause.safeStop();
        }
        /*if(e.getMessage().startsWith("/report")) {
            String[] strings = e.getMessage().split(" Message#");
            if(reportsMsg.contains(strings[1]+strings[2])) {
                e.getPlayer().sendMessage("§c§lReport §8» §eCe message a §6déjà §eété report !");
                e.setCancelled(true);
            } else {
                reportsMsg.add(strings[1]+strings[2]);
            }
        }*/
    }

    @EventHandler
    public void onEatGolden(PlayerItemConsumeEvent event) {
        if(!event.isCancelled()) {
            if (event.getItem().getType() == Material.GOLDEN_APPLE) {
                if (event.getItem().getData().getData() == 1) {
                    Player p = event.getPlayer();
                    if(!TheAPI.getCooldownAPI(p).expired("gapple")) {
                        CMI.getInstance().getActionBarManager().send(p, "§eAttendez §c" + (TheAPI.getCooldownAPI(p).getTimeToExpire("gapple")/40) + "s §eavant de §6reprendre§e une §dPomme Cheat §e!");
                        p.sendMessage("§c§lErreur§f│ §eAttendez §c" + (TheAPI.getCooldownAPI(p).getTimeToExpire("gapple")/40) + "s §eavant de §6reprendre§e une §dPomme Cheat §e!");
                        event.setCancelled(true);
                    } else {
                        TheAPI.getCooldownAPI(p).createCooldown("gapple", p.hasPermission("skycore.bypass.all") ? 0 : 60*40);
                        event.setCancelled(true);
                        p.updateInventory();
                        int as = event.getItem().getAmount() - 1;
                        ItemStack apple = new ItemStack(Material.GOLDEN_APPLE, as);
                        int dura = event.getItem().getDurability(); /* To see if it is an Enchanted golden apple or not */
                        apple.setDurability((short) dura);
                        p.getInventory().setItemInHand(apple);
                        p.updateInventory();
                        p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 1200, 0));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 4));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600, 0));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 6000, 0));
                    }

                }
            }
        }
    }

    @EventHandler
    public void onEnderPearl(ProjectileLaunchEvent event) {
        if(event.getEntity() instanceof EnderPearl) {
            if(event.isCancelled()) return;
            Player p = (Player) event.getEntity().getShooter();
            if(!TheAPI.getCooldownAPI(p).expired("pearl")) {
                CMI.getInstance().getActionBarManager().send(p, "§eAttendez §c" + (TheAPI.getCooldownAPI(p).getTimeToExpire("pearl")/40) + "s §eavant d'§6utiliser§e une §dEnderPearl §e!");
                p.sendMessage("§c§lErreur§f│ §eAttendez §c" + (TheAPI.getCooldownAPI(p).getTimeToExpire("pearl")/40) + "s §eavant d'§6utiliser§e une §dEnderPearl §e!");
                event.setCancelled(true);
                p.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 1));
            } else {
                TheAPI.getCooldownAPI(p).createCooldown("pearl", p.hasPermission("skycore.bypass.all") ? 0 : 20 * 40);
            }
        }
    }

    @EventHandler
    public void onDrink(PlayerInteractEvent event) {
        org.bukkit.event.block.Action action = event.getAction();
        Player player = event.getPlayer();
        if (action == org.bukkit.event.block.Action.RIGHT_CLICK_AIR || action == org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) {
            ItemStack it = player.getItemInHand();
            if ((it.getType() == Material.POTION && it.getDurability() == 8233) || it.getDurability() == 16425) {
                Potion potion = Potion.fromItemStack(it);
                PotionEffectType effecttype = potion.getType().getEffectType();
                if (effecttype == PotionEffectType.INCREASE_DAMAGE) {
                    event.setCancelled(true);
                    player.sendMessage("§c§lFaction §8» §eLes potions de §cForce II §esont désactivés !");
                }
            }
        }
    }

    @EventHandler
    public void onThorn(EntityDamageEvent event) {
        if(event.getEntity() instanceof Player) {
            if(event.getCause().equals(EntityDamageEvent.DamageCause.THORNS)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void worldChange(PlayerChangedWorldEvent e) {
        if (e.getPlayer().getGameMode() == GameMode.SPECTATOR) {
            e.getPlayer().setGameMode(GameMode.SURVIVAL);
            Spectator.setCooldown(e.getPlayer(), 0);
            Spectator.setSpectator(e.getPlayer(), false);
        }
    }

    /*@EventHandler
    public void playerTeleport(PlayerTeleportEvent e) {
        if(!e.isCancelled()) {
            Player p = e.getPlayer();
            if (e.getTo().getWorld().getName().equals(spawn.getWorld().getName())) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 99999999, 0, false, false));
                CMI.getInstance().getPlayer(p.getName()).setWalkSpeed(0.5f);
            } else {
                p.removePotionEffect(PotionEffectType.NIGHT_VISION);
                CMI.getInstance().getPlayer(p.getName()).setWalkSpeed(0.2f);
            }
        }
    }*/
}