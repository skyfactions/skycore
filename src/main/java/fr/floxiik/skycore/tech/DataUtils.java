package fr.floxiik.skycore.tech;

import net.minecraft.server.v1_8_R3.NBTCompressedStreamTools;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.io.*;

public class DataUtils {
    public static void writeItemStack(OutputStream outputStream, ItemStack itemStack) throws IOException {
        if (outputStream instanceof DataOutputStream)
            writeItemStack((DataOutputStream) outputStream,itemStack);
        else
            writeItemStack(new DataOutputStream(outputStream),itemStack);
    }

    public static void writeItemStack(DataOutputStream outputStream, ItemStack itemStack) throws IOException {
        NBTTagCompound nbtTagCompound = new NBTTagCompound();
        CraftItemStack.asNMSCopy(itemStack).save(nbtTagCompound);
        NBTCompressedStreamTools.a(nbtTagCompound,(DataOutput) outputStream);
    }

    public static ItemStack readItemStack(InputStream inputStream) throws IOException {
        if (inputStream instanceof DataInputStream)
            return readItemStack((DataInputStream) inputStream);
        else
            return readItemStack(new DataInputStream(inputStream));
    }

    public static ItemStack readItemStack(DataInputStream inputStream) throws IOException {
        return CraftItemStack.asBukkitCopy(net.minecraft.server.v1_8_R3.ItemStack.createStack(NBTCompressedStreamTools.a(inputStream)));
    }

    /*public static void writeLocation(OutputStream outputStream, Location location) throws IOException {
        if (outputStream instanceof DataOutputStream)
            writeLocation((DataOutputStream) outputStream,location);
        else
            writeLocation(new DataOutputStream(outputStream),location);
    }

    public static void writeLocation(DataOutputStream outputStream, Location location) throws IOException {
        outputStream.writeUTF(location.getWorld().getName());
        outputStream.writeDouble(location.getX());
        outputStream.writeDouble(location.getY());
        outputStream.writeDouble(location.getZ());
        outputStream.writeFloat(location.getYaw());
        outputStream.writeFloat(location.getPitch());
    }

    public static Location readLocation(InputStream inputStream) throws IOException {
        if (inputStream instanceof DataInputStream)
            return readLocation((DataInputStream) inputStream);
        else
            return readLocation(new DataInputStream(inputStream));
    }

    public static Location readLocation(DataInputStream inputStream) throws IOException {
        String worldName = inputStream.readUTF();

        double x = inputStream.readDouble();
        double y = inputStream.readDouble();
        double z = inputStream.readDouble();
    }*/

    public static void writeBlockPos(OutputStream outputStream, Location location) throws IOException {
        if (outputStream instanceof DataOutputStream)
            writeBlockPos((DataOutputStream) outputStream,location);
        else
            writeBlockPos(new DataOutputStream(outputStream),location);
    }

    public static void writeBlockPos(DataOutputStream outputStream, Location location) throws IOException {
        outputStream.writeUTF(location.getWorld().getName());
        outputStream.writeInt(location.getBlockX());
        outputStream.writeInt(location.getBlockY());
        outputStream.writeInt(location.getBlockZ());
    }

    public static Location readBlockPos(InputStream inputStream) throws IOException {
        if (inputStream instanceof DataInputStream)
            return readBlockPos((DataInputStream) inputStream);
        else
            return readBlockPos(new DataInputStream(inputStream));
    }

    public static Location readBlockPos(DataInputStream inputStream) throws IOException {
        String worldName = inputStream.readUTF();
        World world = Bukkit.getWorld(worldName);
        int x = inputStream.readInt();
        int y = inputStream.readInt();
        int z = inputStream.readInt();
        if (world==null) return null;
        return new Location(world,x,y,z);
    }
}
