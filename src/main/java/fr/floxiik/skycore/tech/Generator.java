package fr.floxiik.skycore.tech;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Generator implements IMachine {
    //public static HashMap<Vector,FactionData> generators = new HashMap<>();

    public static final int MAX_BUFFER = 1728;
    public static final boolean DEBUG = true;

    public GenType genType;
    public long timeStamp;
    public final FactionData factionData;
    public Location location;

    public Generator(FactionData factionData,Location location,GenType type,long lastTimestamp) {
        this.factionData = factionData;
        this.location = location;
        this.genType = type;
        this.timeStamp = lastTimestamp;
    }

    public static Generator checkGenerate(Block block, Player player) {
        Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
        boolean bypass = player.hasPermission("skytech.admin");
        if (faction==null||!faction.isNormal()||!SkyTech.overworld.contains(block.getWorld())) return null;
        FactionData factionData = FactionData.getFactionData(faction);
        if (!factionData.canCreateNewGenerator()&&!bypass) return null;
        Direction direction=null;
        Location vector = null;
        if (block.getType()==Material.HOPPER) {
            vector = block.getLocation();
            direction = checkStructureBase(vector);
        } else if (block.getType()==Material.CHEST) {
            if (block.getRelative(BlockFace.EAST).getType()==Material.HOPPER) {
                vector = block.getRelative(BlockFace.EAST).getLocation();
                if (!SkyTech.machines.containsKey(vector))
                    direction = checkStructureBase(vector);
                if (direction==null) {
                    if (block.getRelative(BlockFace.WEST).getType()==Material.HOPPER) {
                        vector = block.getRelative(BlockFace.WEST).getLocation();
                        if (!SkyTech.machines.containsKey(vector))
                            direction = checkStructureBase(vector);
                        if (direction==null) {
                            if (block.getRelative(BlockFace.NORTH).getType()==Material.HOPPER) {
                                vector = block.getRelative(BlockFace.NORTH).getLocation();
                                if (!SkyTech.machines.containsKey(vector))
                                    direction = checkStructureBase(vector);
                                if (direction == null) {
                                    if (block.getRelative(BlockFace.SOUTH).getType()==Material.HOPPER) {
                                        vector = block.getRelative(BlockFace.SOUTH).getLocation();
                                        if (!SkyTech.machines.containsKey(vector))
                                            direction = checkStructureBase(vector);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } else if (block.getRelative(BlockFace.DOWN).getType()==Material.HOPPER) {
            vector = block.getRelative(BlockFace.DOWN).getLocation();
            if (!SkyTech.machines.containsKey(vector))
                direction = checkStructureBase(vector);
        }
        if (direction==null) {
            return null;
        }
        Material type = player.getWorld().getBlockAt(vector.getBlockX(),vector.getBlockY()+1,vector.getBlockZ()).getType();
        for (GenType genType:GenType.values()) {
            if (genType.typeBlock==type) {
                if (genType.canGenerate(player)) {
                    //player.sendMessage(LabTech.prefix+"Generateur créer");
                    for (Player member:factionData.getFaction().getOnlinePlayers())
                        member.sendMessage(SkyTech.prefix+"Generateur créé en X:§6"+vector.getBlockX()+"§e, Y:§6"+vector.getBlockY()+"§e, Z:§6"+vector.getBlockZ());
                    return new Generator(factionData,vector,genType,System.currentTimeMillis());
                } else {
                    player.sendMessage(SkyTech.prefix+"Grade insuffisant pour créer un generateur de "+genType.name().toLowerCase());
                    return null;
                }
            }
        }
        return null;
    }

    public static Direction checkStructureBase(Location location) {
        World world = location.getWorld();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        if (world.getBlockAt(x,y,z).getType()!=Material.HOPPER)
            return null;
        for (int rx = -1;rx <= 1;rx++) {
            for (int rz = -1;rz <= 1;rz++) {
                if (world.getBlockAt(x+rx,y-1,z+rz).getType()!=Material.STAINED_CLAY)
                    return null;
            }
        }
        if (world.getBlockAt(x+1,y,z+1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x-1,y,z+1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x+1,y,z-1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x-1,y,z-1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x+1,y+1,z+1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x-1,y+1,z+1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x+1,y+1,z-1).getType()!=Material.STAINED_CLAY)
            return null;
        if (world.getBlockAt(x-1,y+1,z-1).getType()!=Material.STAINED_CLAY)
            return null;
        //System.out.println("Basic structure OK");
        for (Direction direction:Direction.values()) {
            if (direction.checkStructureBase(world,x,y,z))
                return direction;
        }
        //System.out.println("Direction not OK");
        return null;
    }

    public boolean onClick(Player player,Block chest) {
        Direction direction = checkStructureBase(location);
        if (direction==null|| player.getWorld().getBlockAt(location.getBlockX(),location.getBlockY()+1,location.getBlockZ()).getType()!=genType.typeBlock) {
            this.destroy();
            return true;
        }
        if (genType.canGenerate(player))
            this.chestLoot(chest);
        return true;
    }


    public static enum GenType {
        COAL(256,0,Material.COAL,Material.COAL_BLOCK),
        IRON(128,0,Material.IRON_INGOT,Material.IRON_BLOCK),
        GOLD(32,30,Material.GOLD_INGOT,Material.GOLD_BLOCK),
        LAPIS(64,10,new ItemStack(Material.INK_SACK,1,(byte)4),Material.LAPIS_BLOCK),
        DIAMOND(16,60,Material.DIAMOND,Material.DIAMOND_BLOCK),
        EMERALD(16,40,Material.EMERALD,Material.EMERALD_BLOCK),
        REDSTONE(256,0,Material.REDSTONE,Material.REDSTONE_BLOCK),
        BEDROCK(86400,500,Material.DIAMOND,Material.BEDROCK);

        public final int production,itemMirrilis;
        public final int permission;
        public final Material typeBlock;
        public final ItemStack item;

        GenType(int production, int permission, Material item,Material typeBlock) {
            this(production,permission,new ItemStack(item),typeBlock);
        }

        GenType(int production, int permission, ItemStack item,Material typeBlock) {
            this.production=production;
            this.itemMirrilis=86400000/production;
            this.permission=permission;
            this.item=item;
            this.typeBlock=typeBlock;
        }


        public boolean canGenerate(Player player) {
            //LabPlayer.get(player).getRank()
            return permission==0 || LuckPermsProvider.get().getGroupManager().getGroup(LuckPermsProvider.get().getUserManager().getUser(player.getDisplayName()).getPrimaryGroup()).getWeight().getAsInt()>=permission;

        }
    }

    public void dropLoot() {
        int items = loot();
        if (items==0) return;
        int fullStacks = items/64;
        Location location = new Location(this.location.getWorld(),this.location.getBlockX(),this.location.getBlockY(),this.location.getBlockZ());
        if (fullStacks==0) {
            ItemStack itemStack = genType.item.clone();
            itemStack.setAmount(items);
            this.location.getWorld().dropItem(location,itemStack);
        } else {
            items = items-(fullStacks*64);
            ItemStack itemStack = genType.item.clone();
            itemStack.setAmount(64);
            while (fullStacks-->0) {
                this.location.getWorld().dropItem(location,itemStack);
            }
            itemStack.setAmount(items);
            this.location.getWorld().dropItem(location,itemStack);
        }
    }

    public void chestLoot(Block block) {
        if (block==null||block.getType()!=Material.CHEST) {
            dropLoot();
            return;
        }
        Inventory itemStacks = ((Chest) block.getState()).getBlockInventory();
        int items = loot();
        if (items==0) return;
        int fullStacks = items/64;
        if (fullStacks==0) {
            ItemStack itemStack = genType.item.clone();
            itemStack.setAmount(items);
            itemStacks.addItem(itemStack);
        } else {
            items = items-(fullStacks*64);
            ItemStack itemStack = genType.item.clone();
            itemStack.setAmount(64);
            while (fullStacks-->0) {
                itemStacks.addItem(itemStack);
            }
            itemStack.setAmount(items);
            itemStacks.addItem(itemStack);
        }
    }

    public int loot() {
        long waited = System.currentTimeMillis()-timeStamp;
        long items = waited/genType.itemMirrilis;
        if (items>=MAX_BUFFER) {
            this.timeStamp=System.currentTimeMillis();
            return MAX_BUFFER;
        }
        timeStamp+=items*genType.itemMirrilis;
        return (int) items;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location loc) {
        this.location = loc;
    }

    @Override
    public void destroy() {
        this.destroy((Player) null);
    }

    @Override
    public void destroy(Player player) {
        this.chestLoot(this.getChest());
        factionData.generators.remove(location);
        SkyTech.machines.remove(location);
        for (Player member:factionData.getFaction().getOnlinePlayers())
            member.sendMessage(SkyTech.prefix+"Generateur détruit en X:§6"+location.getBlockX()+"§e, Y:§6"+location.getBlockY()+"§e, Z:§6"+location.getBlockZ()+(player==null?"":" par "+player.getName()));
        genType = null;
    }

    @Override
    public void destroy(BlockBreakEvent event) {
        this.destroy(event.getPlayer());
    }

    @Override
    public boolean isValid() {
        return genType!=null&&factionData.getMachine(location)==this;
    }

    @Override
    public FactionData getFactionData() {
        return factionData;
    }

    public static enum Direction {
        NORTH(BlockFace.NORTH) {
            @Override
            boolean checkStructureBase(World world,int x,int y,int z) {
                return world.getBlockAt(x, y, z - 1).getType() == Material.CHEST && world.getBlockAt(x, y + 1, z - 1).getType() == Material.AIR && world.getBlockAt(x, y, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y + 1, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y + 1, z).getType() == Material.STAINED_CLAY;
            }

            @Override
            public Block getChest(Generator generator) {
                return generator.location.getWorld().getBlockAt(generator.location.getBlockX(),generator.location.getBlockY(),generator.location.getBlockZ()-1);
            }
        }/*-Z*/,SOUTH(BlockFace.SOUTH) {
            @Override
            boolean checkStructureBase(World world,int x,int y,int z) {
                return world.getBlockAt(x, y, z + 1).getType() == Material.CHEST && world.getBlockAt(x, y + 1, z + 1).getType() == Material.AIR && world.getBlockAt(x, y, z - 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z - 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y + 1, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y + 1, z).getType() == Material.STAINED_CLAY;
            }

            @Override
            public Block getChest(Generator generator) {
                return generator.location.getWorld().getBlockAt(generator.location.getBlockX(),generator.location.getBlockY(),generator.location.getBlockZ()+1);
            }
        }/*+Z*/,EAST(BlockFace.EAST) {
            @Override
            boolean checkStructureBase(World world,int x,int y,int z) {
                return world.getBlockAt(x + 1, y, z).getType() == Material.CHEST && world.getBlockAt(x + 1, y + 1, z).getType() == Material.AIR && world.getBlockAt(x - 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x - 1, y + 1, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z + 1).getType() == Material.STAINED_CLAY;
            }

            @Override
            public Block getChest(Generator generator) {
                return generator.location.getWorld().getBlockAt(generator.location.getBlockX()+1,generator.location.getBlockY(),generator.location.getBlockZ());
            }
        }/*+X*/,WEST(BlockFace.WEST) {
            @Override
            boolean checkStructureBase(World world,int x,int y,int z) {
                return world.getBlockAt(x - 1, y, z).getType() == Material.CHEST && world.getBlockAt(x - 1, y + 1, z).getType() == Material.AIR && world.getBlockAt(x + 1, y, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x + 1, y + 1, z).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z + 1).getType() == Material.STAINED_CLAY && world.getBlockAt(x, y + 1, z + 1).getType() == Material.STAINED_CLAY;
            }

            @Override
            public Block getChest(Generator generator) {
                return generator.location.getWorld().getBlockAt(generator.location.getBlockX()-1,generator.location.getBlockY(),generator.location.getBlockZ());
            }
        }/*-X*/;

        public BlockFace FACE;

        Direction(BlockFace blockFace) {
            this.FACE=blockFace;
        }

        abstract boolean checkStructureBase(World world,int x,int y,int z);
        abstract Block getChest(Generator generator);
    }

    public Block getChest() {
        for (Direction direction:Direction.values()) {
            Block chest = direction.getChest(this);
            if (chest.getType()==Material.CHEST)
                return chest;
        }
        return null;
    }

    public static Generator getGenerator(Location vector) {
        FactionData factionData = SkyTech.machines.get(vector);
        if (factionData==null) return null;
        return factionData.generators.get(vector);
    }

    public void checkStructure() {
        this.checkStructure(null);
    }

    public void checkStructure(Player player) {
        Direction direction = checkStructureBase(location);
        if (direction==null|| location.getWorld().getBlockAt(location.getBlockX(),location.getBlockY()+1,location.getBlockZ()).getType()!=genType.typeBlock) {
            this.destroy(player);
        }
    }
}
