package fr.floxiik.skycore.tech;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

import java.text.NumberFormat;
import java.util.Objects;

public class PlaceHolderTech extends PlaceholderExpansion {

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Floxiik";
    }

    @Override
    public String getIdentifier() {
        return "skytech";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String identifier) {
        FactionData factionData = FactionData.getFactionData(p);
        Protector protector = factionData.protector;
        if (identifier.equals("protector")) {
            if (!protector.isValid()) {
                return "&cNon posé";
            } else {
                if(protector.enabled()) {
                    return "§aActivé";
                } else {
                    return "§cDésactivé";
                }
            }
        }
        if (identifier.equals("emeralds")) {
            if (protector.isValid()) {
                NumberFormat format=NumberFormat.getInstance();
                format.setMinimumFractionDigits(1);
                return "&a" + format.format(protector.emeralds);
            } else {
                return "&4✗";
            }
        }
        return null;
    }
}