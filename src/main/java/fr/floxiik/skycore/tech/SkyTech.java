package fr.floxiik.skycore.tech;

import fr.floxiik.skycore.SkyCore;
import org.bukkit.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class SkyTech {
    public static HashMap<Location,FactionData> machines = new HashMap<>();

    public static ItemStack protectorFrame,PCB,protector;
    public static ShapedRecipe protectorRecipe;

    public static SkyTech PLUGIN = null;
    public static File dataFolder = null;
    public static Logger logger;
    public static List<World> overworld = new ArrayList<>();
    public static final String prefix = "§c§lSkyTech §8» §e";
    public static final String[] sub = new String[]{"help","faction","reset_gen"};

    public static void enable() {
        dataFolder = new File(SkyCore.getInstance().getDataFolder() + "/tech/");
        logger = SkyCore.getInstance().getLogger();
        FactionData.init();
        new BukkitRunnable() {
            @Override
            public void run() {
                for (FactionData factionData:FactionData.getAllFactionData()) {
                    factionData.update2Sec();
                }
            }
        }.runTaskTimer(SkyCore.getInstance(),40,40);
        new PlaceHolderTech().register();
    }

    public static IMachine getMachine(Location vector) {
        SkyTech.roundToBlock(vector);
        FactionData factionData = machines.get(vector);
        if (factionData==null) return null;
        return factionData.getMachine(vector);
    }

    public static boolean blockEquals(Location vector1,Location vector2) {
        return vector1.getBlockX()==vector2.getBlockX()&&vector1.getBlockY()==vector2.getBlockY()&&vector1.getBlockZ()==vector2.getBlockZ();
    }

    public static void roundToBlock(Location vector) {
        vector.setX(vector.getBlockX());
        vector.setY(vector.getBlockY());
        vector.setZ(vector.getBlockZ());
    }
}
