package fr.floxiik.skycore.tech;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.util.Vector;

public interface IMachine {
    Location getLocation();
    void destroy();
    void destroy(Player player);
    void destroy(BlockBreakEvent event);
    boolean onClick(Player player,Block block);
    boolean isValid();
    FactionData getFactionData();
}
