package fr.floxiik.skycore.tech;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.Relation;
import fr.floxiik.skycore.help.PlayerHelp;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Protector implements IMachine,InventoryHolder {
    Location loc;

    Inventory inventory;

    private static final DecimalFormat n_1 = new DecimalFormat("#.#"),n_2 = new DecimalFormat("#.##");

    final FactionData factionData;
    double emeralds = 0;
    long lastTimeStamp = 0;
    double range = 0,rawRange = 0;
    double consuption = 0;


    public Protector(FactionData factionData) {
        this.factionData = factionData;
        this.inventory = Bukkit.createInventory(this, InventoryType.HOPPER);
    }

    @Override
    public Location getLocation() {
        return loc;
    }

    public void setLocation(Location loc) {
        this.loc = loc;
    }

    @Override
    public void destroy() {
        this.destroy((Player) null);
    }

    @Override
    public void destroy(Player player) {
        SkyTech.machines.remove(loc);
        for (Player member:factionData.getFaction().getOnlinePlayers())
            member.sendMessage(SkyTech.prefix+"Protecteur détruit en X:§6"+loc.getBlockX()+"§e, Y:§6"+loc.getBlockY()+"§e, Z:§6"+loc.getBlockZ()+(player==null?"":" §epar §c"+player.getName() + "§e."));

        Location location = new Location(loc.getWorld(),loc.getBlockX(),loc.getBlockY(),loc.getBlockZ());
        for (ItemStack itemStack:inventory) {
            if (itemStack!=null&&itemStack.getType()!=Material.AIR) {
                location.getWorld().dropItemNaturally(location,itemStack);
            }
        }
        if (emeralds>=1728)
            emeralds=1728;
        while (emeralds>=1) {
            int em = Math.min(64,(int) emeralds);
            emeralds=-em;
            location.getWorld().dropItemNaturally(location,new ItemStack(Material.EMERALD,em));
        }
        this.reset();
    }


    public double getEmeralds() {
        return this.emeralds;
    }

    @Override
    public void destroy(BlockBreakEvent event) {
        this.destroy(event.getPlayer());
        Location loc = event.getBlock().getLocation();
        loc.getWorld().dropItemNaturally(loc, SkyTech.protector);
        event.getBlock().setType(Material.AIR);
        event.setCancelled(true);
    }

    @Override
    public boolean onClick(Player player, Block block) {
        if (FPlayers.getInstance().getByPlayer(player).getRelationTo(factionData.getFaction())!=Relation.MEMBER) {
            player.sendMessage(SkyTech.prefix+"Ce §6Protecteur §ene vous appartiens pas ! "+factionData.getFaction().getTag());
            return true;
        }
        ItemStack itemStack = player.getItemInHand();
        if (itemStack!=null) {
            switch (itemStack.getType()) {
                case EMERALD:
                    if (player.isSneaking()) {
                        ItemStack newItemStack = itemStack.clone();
                        newItemStack.setAmount(itemStack.getAmount()-1);
                        if (newItemStack.getAmount()==0) {
                            player.setItemInHand(new ItemStack(Material.AIR));
                        } else {
                            player.setItemInHand(newItemStack);
                        }
                        emeralds+=1;
                        player.sendMessage(SkyTech.prefix+"Vous venez d'ajouter §c1§e Emeraude au §6protecteur §e! §7(§a"+n_2.format(emeralds)+"§7)");
                    } else {
                        player.setItemInHand(new ItemStack(Material.AIR));
                        emeralds+=itemStack.getAmount();
                        player.sendMessage(SkyTech.prefix+"Vous venez d'ajouter §c"+itemStack.getAmount()+" §eEmeraudes au §6protecteur §e! §7(§a"+n_2.format(emeralds)+"§7)");
                    }
                    return true;
                case EMERALD_BLOCK:
                    if (player.isSneaking()) {
                        ItemStack newItemStack = itemStack.clone();
                        newItemStack.setAmount(itemStack.getAmount()-1);
                        if (newItemStack.getAmount()==0) {
                            player.setItemInHand(new ItemStack(Material.AIR));
                        } else {
                            player.setItemInHand(newItemStack);
                        }
                        emeralds+=1;
                        player.sendMessage(SkyTech.prefix+"Vous venez d'ajouter §c9 §eEmeraudes au §6protecteur §e! §7(§a"+n_2.format(emeralds)+"§7)");
                    } else {
                        player.setItemInHand(new ItemStack(Material.AIR));
                        emeralds+=itemStack.getAmount()*9;
                        player.sendMessage(SkyTech.prefix+"Vous venez d'ajouter §c"+(itemStack.getAmount()*9)+" §eEmeraudes au §6protecteur §e! §7(§a"+n_2.format(emeralds)+"§7)");
                    }
                    return false;
                case STICK:
                    if (player.isSneaking()) {
                        player.sendMessage(PlayerHelp.prefix);
                        player.sendMessage("§l➢ §eProtecteur: "+(this.enabled()?(ChatColor.GREEN+"Activé"):(ChatColor.RED+"Désactivé")));
                        player.sendMessage("§l➢ §eEmeraudes: "+n_2.format(emeralds)+" ("+(consuption==0?("0"):("-"+consuption))+" par jour)");
                        player.sendMessage("§l➢ §eRayon: "+n_1.format(range)+" (sqrt("+rawRange+")*Pi)");
                        player.sendMessage(PlayerHelp.suffix);
                        return true;
                    }
                default:
            }
        }
        player.openInventory(inventory);
        return true;
    }

    @Override
    public boolean isValid() {
        return loc != null;
    }

    public FactionData getFactionData() {
        return factionData;
    }

    public void updateProtection() {
        this.updateConsumption();
        this.recalcProtection0();
    }

    void initProtection() {
        this.recalcProtection0();
        this.updateConsumption();
    }

    private void recalcProtection0() {
        double c = 0;
        double r = 0;
        for (ItemStack itemStack:inventory) {
            if (itemStack!=null) {
                switch (itemStack.getType()) {
                    case IRON_BLOCK:
                        c+=itemStack.getAmount()*0.2;
                        r+=itemStack.getAmount()*0.1;
                        break;
                    case GOLD_BLOCK:
                        c+=itemStack.getAmount();
                        r+=itemStack.getAmount();
                        break;
                    case DIAMOND_BLOCK:
                        c+=itemStack.getAmount()*1.5;
                        r+=itemStack.getAmount()*2;
                        break;
                    case BEACON:
                        c+=itemStack.getAmount()*2;
                        r+=itemStack.getAmount()*3;
                        break;
                    case EMERALD_BLOCK:
                        c-=itemStack.getAmount()*0.5;
                        r+=itemStack.getAmount()*0.5;
                        break;
                    default:
                }
            }
        }

        rawRange = r;
        range = Math.sqrt(r)*Math.PI;
        consuption = 0>c?0:c;
    }

    public void updateConsumption() {
        if (emeralds==0||consuption==0) {
            lastTimeStamp = System.currentTimeMillis();
            return;
        }
        long waited = System.currentTimeMillis()-lastTimeStamp;
        double c = consuption/((double)86400000)*waited;
        if (c > emeralds) {
            emeralds = 0;
            for (Player member:factionData.getFaction().getOnlinePlayers())
                member.sendMessage(SkyTech.prefix+"La réserve d'émeraudes du §6Protecteur§e est §cvide§e !");
        } else
            emeralds -= c;
        lastTimeStamp = System.currentTimeMillis();
    }

    public boolean enabled() {
        if (System.currentTimeMillis()-lastTimeStamp>1000)
            this.updateConsumption();
        return emeralds>0;
    }

    public void save(DataOutputStream dataOutputStream) throws IOException {
        this.updateConsumption();
        ArrayList<ItemStack> itemStacks = new ArrayList<>();
        for (ItemStack itemStack:inventory.getContents())
            if (itemStack!=null&&itemStack.getType()!=Material.AIR)
                itemStacks.add(itemStack);
        dataOutputStream.writeLong(lastTimeStamp);
        dataOutputStream.writeDouble(emeralds);
        DataUtils.writeBlockPos(dataOutputStream,loc);
        dataOutputStream.writeInt(itemStacks.size());
        for (ItemStack itemStack:itemStacks)
            DataUtils.writeItemStack(dataOutputStream,itemStack);
    }


    public void load(DataInputStream dataInputStream) throws IOException {
        inventory.clear();
        lastTimeStamp = dataInputStream.readLong();
        emeralds = dataInputStream.readDouble();
        loc = DataUtils.readBlockPos(dataInputStream);
        int total = dataInputStream.readInt();
        for (int i = 0;i < total;i++)
            inventory.addItem(DataUtils.readItemStack(dataInputStream));
    }

    public void reset() {
        inventory.clear();
        emeralds=0;
        consuption=0;
        range=0;
        rawRange=0;
        loc = null;
    }

    /*public boolean inProtectionZone(Location location) {
        if (!LabTech.overworld.equals(location.getWorld()))
            return false;
        return inProtectionZone(location.toVector());
    }

    public boolean inProtectionZone(Vector vector) {
        if (!this.isValid()) return false;
        double x = Math.abs(vector.getX()-loc.getX());
        double z = Math.abs(vector.getZ()-loc.getZ());
        return Math.sqrt(x*x+z*z)<range;
    }*/

    public double getBlockAwayFromProtector(Location vector) {
        if (!this.isValid()) return -1;
        double x = Math.abs(vector.getX()-(loc.getX()+0.5D));
        double z = Math.abs(vector.getZ()-(loc.getZ()+0.5D));
        return Math.sqrt(x*x+z*z);
    }

    public void processPlayer(Player player) {
        double range = getBlockAwayFromProtector(player.getLocation());
        if (range==-1) return;
        Relation relation = factionData.getFaction().getRelationTo(FPlayers.getInstance().getByPlayer(player));
        boolean ally = (relation==Relation.ALLY||relation==Relation.TRUCE||relation==Relation.MEMBER);
        /*if (Math.abs(this.range-range)<5.5D) {
            double x = player.getLocation().getX()-(loc.getX()+0.5D);
            double z = player.getLocation().getZ()-(loc.getZ()+0.5D);
            double r = Math.sqrt(Math.pow(Math.abs(x),2)+Math.pow(Math.abs(z),2));
            Location location = new Location(LabTech.overworld,(x*this.range)/r+(loc.getX()+0.5D),player.getLocation().getY()+1.4D,(z*this.range)/r+(loc.getZ()+0.5D));
            player.spigot().playEffect(location,ally? Effect.HAPPY_VILLAGER:Effect.LAVA_POP,0,0,0,0.1F,0,0.1F,2,6);
        }*/
        if (!ally&&range<this.range) {
            player.damage(1D);
        } else {
        }
    }

    public void render(Player[] players) {
        double centerX = loc.getBlockX()+0.5D;
        double centerZ = loc.getBlockZ()+0.5D;
        int numberOfPoint = (int) Math.round(range*10*Math.PI);
        if (numberOfPoint==0) numberOfPoint = 1;
        double radPerPoint = 2*Math.PI/numberOfPoint;
        double rad = 0;
        while (numberOfPoint-->0) {
            this.renderPlayers(Math.cos(rad)*this.range+centerX,Math.sin(rad)*this.range+centerZ,players);
            rad+=radPerPoint;
        }
    }

    public void renderPlayers(double x,double z,Player[] players) {
        for (Player player:players) {
            double y = player.getLocation().getY();
            Relation relation = factionData.getFaction().getRelationTo(FPlayers.getInstance().getByPlayer(player));
            boolean ally = (relation==Relation.ALLY||relation==Relation.TRUCE||relation==Relation.MEMBER);
            if (ally) {
                player.spigot().playEffect(new Location(player.getWorld(), x, y - 0.5D, z), Effect.HAPPY_VILLAGER, 0, 0, 0, 0.1F, 0, 0.1F, 2, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 0.5D, z), Effect.HAPPY_VILLAGER, 0, 0, 0, 0.1F, 0, 0.1F, 2, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 1.5D, z), Effect.HAPPY_VILLAGER, 0, 0, 0, 0.1F, 0, 0.1F, 2, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 2.5D, z), Effect.HAPPY_VILLAGER, 0, 0, 0, 0.1F, 0, 0.1F, 2, 7);
            } else {
                player.spigot().playEffect(new Location(player.getWorld(), x, y - 0.5D, z),Effect.PORTAL, 0, 0, 0, 0.1F, 0, 0.1F, 5, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 0.5D, z),Effect.PORTAL, 0, 0, 0, 0.1F, 0, 0.1F, 5, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 1.5D, z),Effect.PORTAL, 0, 0, 0, 0.1F, 0, 0.1F, 5, 7);
                player.spigot().playEffect(new Location(player.getWorld(), x, y + 2.5D, z),Effect.PORTAL, 0, 0, 0, 0.1F, 0, 0.1F, 5, 7);

            }
        }
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }
}
