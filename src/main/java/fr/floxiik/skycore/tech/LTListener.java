package fr.floxiik.skycore.tech;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.event.LandClaimEvent;
import com.massivecraft.factions.event.LandUnclaimEvent;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.help.PlayerHelp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;

public class LTListener implements Listener {

    /*@EventHandler
    public void onDisband(FactionDisbandEvent event) {
        FactionData.onDisband(event.getFaction());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockPlace(final BlockPlaceEvent event) {
        final Block block = event.getBlockPlaced();
        if (event.getItemInHand().getType()==Material.COMMAND&&!event.getPlayer().isOp()) {
            event.setCancelled(true);
            if (event.getItemInHand().getItemMeta().hasDisplayName()&&event.getItemInHand().getItemMeta().getDisplayName().equals("Printed Circuit Board")) {
                Bukkit.getServer().broadcastMessage(SkyTech.prefix+event.getPlayer().getName()+" a cassé un Printed Circuit Board (qu'il est con) :P");
                ItemStack newItemStack = event.getItemInHand().clone();
                newItemStack.setAmount(newItemStack.getAmount()-1);
                if (newItemStack.getAmount()==0) {
                    event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
                } else {
                    event.getPlayer().setItemInHand(newItemStack);
                }
            } else {
                event.getPlayer().setItemInHand(null);
            }
        } else if (event.getItemInHand().getType()==Material.SEA_LANTERN&&event.getItemInHand().getItemMeta().hasDisplayName()&&event.getItemInHand().getItemMeta().getDisplayName().equals("§c§lProtecteur")) {
            if (!SkyTech.overworld.contains(event.getBlock().getLocation().getWorld())) {
                event.getPlayer().sendMessage(SkyTech.prefix+"Vous devez §6poser§e ceci dans votre §cbase§e !");
                event.setCancelled(true);
                return;
            }
            FactionData factionData = FactionData.getFactionData(event.getPlayer());
            if (!factionData.getFaction().isNormal()) {
                event.getPlayer().sendMessage(SkyTech.prefix+"Vous devez §6posseder §eune §cfaction §e!");
                event.setCancelled(true);
                return;
            }
            if (factionData.protector.isValid()) {
                event.getPlayer().sendMessage(SkyTech.prefix+"Limite de protecteurs §catteinte§e !");
                event.setCancelled(false);
                return;
            }
            if (!factionData.inClaim(event.getBlock())) {
                event.getPlayer().sendMessage(SkyTech.prefix+"Vous devez §6poser§e ceci dans votre §cbase§e !");
                event.setCancelled(true);
                return;
            }
            factionData.protector.reset();
            factionData.protector.loc = event.getBlock().getLocation();
            SkyTech.roundToBlock(factionData.protector.loc);
            SkyTech.machines.put(factionData.protector.loc,factionData);
            PlayerHelp.startProtectorGeneration(event.getPlayer());
            for (Player member:factionData.getFaction().getOnlinePlayers())
                member.sendMessage(SkyTech.prefix+"§aProtecteur §eposé en X:§6"+factionData.protector.loc.getBlockX()+"§e, Y:§6"+factionData.protector.loc.getBlockY()+"§e, Z:§6"+factionData.protector.loc.getBlockZ() + "§e !");
            factionData.protector.updateConsumption();
            //Crews.getPlayerCrew(event.getPlayer()).setProtector(factionData.protector);
        } else if (block.getType()==Material.HOPPER||block.getType()==Material.CHEST) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Generator generator = Generator.checkGenerate(block, event.getPlayer());
                    if (generator != null) {
                        FactionData factionData = FactionData.getFactionData(event.getPlayer());
                        factionData.generators.put(generator.location, generator);

                        //Crews.getPlayerCrew(event.getPlayer()).addGenerator(generator.location, generator);
                        SkyTech.machines.put(generator.location, factionData);
                    }
                }
            }.runTask(SkyCore.getInstance());
        } else {
            final Block phopper = block.getRelative(BlockFace.DOWN);
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (phopper.getType() == Material.HOPPER) {
                        Generator generator = Generator.checkGenerate(phopper, event.getPlayer());
                        if (generator != null) {
                            FactionData factionData = FactionData.getFactionData(event.getPlayer());
                            factionData.generators.put(generator.location, generator);
                            SkyTech.machines.put(generator.location, factionData);
                        }
                    }
                }
            }.runTask(SkyCore.getInstance());

        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent blockBreakEvent) {
        Location location = blockBreakEvent.getBlock().getLocation();
        if (!SkyTech.overworld.contains(location.getWorld())) return;
        for (Map.Entry<Location,FactionData> factionDataEntry: SkyTech.machines.entrySet()) {
            Location vector = factionDataEntry.getKey();
            if (Math.abs(vector.getBlockX()-location.getBlockX())<=1&&Math.abs(vector.getBlockY()-location.getBlockY())<=1&&Math.abs(vector.getBlockZ()-location.getBlockZ())<=1) {
                final Generator generator = factionDataEntry.getValue().generators.get(vector);
                if (generator!=null) new BukkitRunnable() {
                    @Override
                    public void run() {
                        generator.destroy();
                    }
                }.runTask(SkyCore.getInstance());
            }
        }
        IMachine machine = SkyTech.getMachine(location);
        if (machine!=null&&!(machine instanceof Generator)) {
            machine.destroy(blockBreakEvent);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction()!= Action.RIGHT_CLICK_BLOCK||!SkyTech.overworld.contains(event.getClickedBlock().getLocation().getWorld())) return;
        if (event.getClickedBlock().getType()==Material.CHEST) {
            for (Generator.Direction direction : Generator.Direction.values()) {
                Generator generator = Generator.getGenerator(event.getClickedBlock().getRelative(direction.FACE).getLocation());
                if (generator!=null)
                    generator.onClick(event.getPlayer(),event.getClickedBlock());
            }
        }
        IMachine machine = SkyTech.getMachine(event.getClickedBlock().getLocation());
        if (machine!=null&&!(machine instanceof Generator)) {
            if (!machine.onClick(event.getPlayer(),event.getClickedBlock()))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onUnclaim(LandUnclaimEvent event) {
        FactionData.getFactionData(event.getFaction()).onUnclaim(event);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onClaim(LandClaimEvent event) {
        Faction old = Board.getInstance().getFactionAt(event.getLocation());
        if (!old.isNormal()) return;
        FactionData.getFactionData(old).onOvercclaim(event);
    }

    @EventHandler
    public void onCloseInvetory(InventoryCloseEvent event) {
        InventoryHolder inventoryHolder = event.getInventory().getHolder();
        if (inventoryHolder instanceof Protector)
            ((Protector) inventoryHolder).updateProtection();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onExplosion(BlockExplodeEvent event) {
        if (!SkyTech.overworld.contains(event.getBlock().getWorld())) return;
        int maxX = event.getBlock().getX(),minX = event.getBlock().getX(),maxY = event.getBlock().getY(),minY = event.getBlock().getY(),maxZ = event.getBlock().getZ(),minZ = event.getBlock().getZ();
        for (Block block:event.blockList()) {
            maxX = Math.max(maxX,block.getX());
            minX = Math.min(minX,block.getX());
            maxY = Math.max(maxY,block.getY());
            minY = Math.min(minY,block.getY());
            maxZ = Math.max(maxZ,block.getZ());
            minZ = Math.min(minZ,block.getZ());
            IMachine machine = SkyTech.getMachine(block.getLocation());
            if (machine!=null&&!(machine instanceof Generator)) {
                machine.destroy();
            }
        }
        maxX++;
        minX--;
        maxY++;
        minY--;
        maxZ++;
        minZ--;
        for (Map.Entry<Location,FactionData> factionDataEntry: SkyTech.machines.entrySet()) {
            Location vector = factionDataEntry.getKey();
            if (vector.getX()>=minX&&vector.getX()<=maxX&&vector.getY()>=minY&&vector.getY()<=maxY&&vector.getZ()>=minZ&&vector.getZ()<=maxZ) {
                final Generator generator = factionDataEntry.getValue().generators.get(vector);
                if (generator!=null) new BukkitRunnable() {
                    @Override
                    public void run() {
                        generator.checkStructure();
                    }
                }.runTask(SkyCore.getInstance());
            }
        }
    }

    @EventHandler
    public void craft(CraftItemEvent e) {
        if(e.getInventory()!=null&&e.getInventory() instanceof CraftingInventory) {
            CraftingInventory inv = (CraftingInventory) e.getInventory();
            if (e.getRecipe().equals(SkyTech.protectorRecipe)) {
                for (int i:new int[]{0,2,6,8}) {
                    if (!inv.getItem(i).isSimilar(SkyTech.protectorFrame)||!inv.getItem(i).getItemMeta().hasDisplayName()||!inv.getItem(i).getItemMeta().getDisplayName().equals(SkyTech.protectorFrame.getItemMeta().getDisplayName())) {
                        e.setCancelled(true);
                        e.setResult(Event.Result.DENY);
                        return;
                    }
                }
                if (!inv.getItem(4).isSimilar(SkyTech.PCB)||!inv.getItem(4).getItemMeta().hasDisplayName()&&!inv.getItem(4).getItemMeta().getDisplayName().equals("Printed Circuit Board")) {
                    e.setCancelled(true);
                    e.setResult(Event.Result.DENY);
                }
            }
        }
    }*/
}
