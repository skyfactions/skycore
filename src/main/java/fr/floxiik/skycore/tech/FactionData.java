package fr.floxiik.skycore.tech;

import com.massivecraft.factions.*;
import com.massivecraft.factions.event.LandClaimEvent;
import com.massivecraft.factions.event.LandUnclaimEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class FactionData {
    private static final HashMap<String,FactionData> factionDatas = new HashMap<>();
    public static boolean debug = false;
    private static File facDataFolder;
    public HashMap<Location,Generator> generators;
    public Protector protector = new Protector(this);

    public static FactionData getFactionData(Player player) {
        return getFactionData(FPlayers.getInstance().getByPlayer(player));
    }

    public static FactionData getFactionData(FPlayer player) {
        return getFactionData(player.getFaction());
    }

    public static FactionData getFactionData(Faction faction) {
        String fac = faction.getId();
        if (!factionDatas.containsKey(fac)) {
            FactionData factionData = new FactionData(faction);
            factionDatas.put(fac,factionData);
            factionData.forceUpdate();
            return factionData;
        }
        return factionDatas.get(fac);
    }

    public static FactionData[] getAllFactionData() {
        return factionDatas.values().toArray(new FactionData[0]);
    }

    static void onDisband(Faction faction) {
        String id = faction.getId();
        if (factionDatas.containsKey(id)) {
            FactionData factionData = factionDatas.get(id);
            factionDatas.remove(id);
            factionData.softReset();
            factionData.forceUpdate();
            File file = factionData.getFile();
            if (file.exists()&&!file.delete())
                file.deleteOnExit();
        } else {
            File file = new File(facDataFolder,faction.getId());
            if (file.exists()&&!file.delete())
                file.deleteOnExit();
        }
    }

    static void init() {
        facDataFolder = new File(SkyTech.dataFolder,"fac-data");
        if (!facDataFolder.exists())
            facDataFolder.mkdirs();
        for (Faction faction: Factions.getInstance().getAllFactions()) {
            getFactionData(faction).load();
        }
    }

    public static void saveAll() {
        for (FactionData factionData:factionDatas.values())
            factionData.save();
    }

    public static void reload() {
        debug = true;
        for (FactionData factionData:factionDatas.values()) {
            factionData.save();
            factionData.softReset();
            factionData.load();
        }
        debug = false;
    }

    private final Faction faction;
    private final File file;

    private FactionData(Faction faction) {
        this.faction=faction;
        this.generators=new HashMap<>();
        this.file=new File(facDataFolder,faction.getId());
        this.load();
    }

    public Faction getFaction() {
        return faction;
    }

    public File getFile() {
        return file;
    }

    public void load() {
        if (!file.exists())
            return;
        if (faction.isNormal()) try {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(Files.readAllBytes(file.toPath())));
            int i = dataInputStream.readInt();
            while (i-->0) {
                String name = dataInputStream.readUTF();
                int len = dataInputStream.readInt();
                byte[] bytes = new byte[len];
                dataInputStream.read(bytes);
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                DataInputStream dis = new DataInputStream(bais);
                switch (name) {
                    case "":
                        break;
                    case "gen": {
                        int r = dis.readInt();
                        while (r-->0) {
                            Location location = DataUtils.readBlockPos(dis);
                            Generator.GenType genType = Generator.GenType.values()[dis.readShort()];
                            long lastTimeStamp = dis.readLong();
                            generators.put(location,new Generator(this,location,genType,lastTimeStamp));
                        }
                        break;
                    }
                    case "pro":
                        this.protector.loc=DataUtils.readBlockPos(dis);
                        this.protector.emeralds=dis.readDouble();
                        this.protector.lastTimeStamp=dis.readLong();
                        this.protector.inventory.clear();
                        int r = dis.readInt();
                        while (r-->0) {
                            this.protector.inventory.addItem(DataUtils.readItemStack(dis));
                        }
                        this.protector.initProtection();
                    default: {
                        SkyTech.logger.warning("In "+file.getName()+" unknown key \""+name+"\"");
                        break;
                    }
                }
            }
            forceUpdate();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    public void save() {
        if (faction.isNormal()) try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            int i = 0;
            if (!generators.isEmpty())
                i++;
            if (protector.isValid())
                i++;
            dataOutputStream.writeInt(i);
            if (!generators.isEmpty()) {
                i--;
                ByteArrayOutputStream byos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(byos);
                dos.writeInt(generators.size());
                for (Generator generator:generators.values()) {
                    DataUtils.writeBlockPos(dos,generator.location);
                    dos.writeShort(generator.genType.ordinal());
                    dos.writeLong(generator.timeStamp);
                }
                dataOutputStream.writeUTF("gen");
                byte[] bytes = byos.toByteArray();
                dataOutputStream.writeInt(bytes.length);
                dataOutputStream.write(bytes);
            }
            if (protector.isValid()) {
                i--;
                protector.updateConsumption();
                ByteArrayOutputStream byos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(byos);
                DataUtils.writeBlockPos(dos,protector.loc);
                dos.writeDouble(protector.emeralds);
                dos.writeLong(protector.lastTimeStamp);
                ArrayList<ItemStack> itemStacks = new ArrayList<>();
                for (ItemStack itemStack:protector.inventory) {
                    if (itemStack!=null&&itemStack.getType()!= Material.AIR) {
                        itemStacks.add(itemStack);
                    }
                }
                dos.writeInt(itemStacks.size());
                for (ItemStack itemStack:itemStacks) {
                    DataUtils.writeItemStack(dos,itemStack);
                }
                dataOutputStream.writeUTF("pro");
                byte[] bytes = byos.toByteArray();
                dataOutputStream.writeInt(bytes.length);
                dataOutputStream.write(bytes);
            }
            while (i-->0) {
                dataOutputStream.writeUTF("");
                dataOutputStream.writeInt(0);
            }
            file.createNewFile();
            Files.write(file.toPath(),byteArrayOutputStream.toByteArray());
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    public void forceUpdate() {
        for (Location vector: SkyTech.machines.keySet().toArray(new Location[SkyTech.machines.size()])) {
            if (SkyTech.machines.get(vector)==this)
                SkyTech.machines.remove(vector);
        }
        for (Location vector:generators.keySet()) {
            SkyTech.machines.put(vector,this);
        }
        if (protector!=null&&protector.isValid()) {
            SkyTech.machines.put(protector.getLocation(),this);
        }
    }

    public int maxGenerator() {
        return faction.isNormal() ? Math.min(Math.max(0,faction.getSize()-1),5):0;
    }

    public boolean canCreateNewGenerator() {
        return maxGenerator()>generators.size();
    }

    private void softReset() {
        generators.clear();
        protector.destroy();
    }

    public void reset() {
        this.softReset();
        this.forceUpdate();
        this.save();
    }

    public IMachine getMachine(Location vector) {
        if (generators.containsKey(vector))
            return generators.get(vector);
        if (protector.isValid()&& SkyTech.blockEquals(protector.loc,vector))
            return protector;
        return null;
    }

    void onUnclaim(LandUnclaimEvent event) {
        if (protector!=null&&protector.isValid()) {
            Location vector = protector.loc;
            if (event.getLocation().equals(new FLocation(new Location(vector.getWorld(),vector.getBlockX(),vector.getBlockY(),vector.getBlockZ())))) {
                protector.destroy(event.getfPlayer().getPlayer());
            }
        }
    }

    void onOvercclaim(LandClaimEvent event) {
        if (protector!=null&&protector.isValid()) {
            Location vector = protector.loc;
            if (event.getLocation().equals(new FLocation(new Location(vector.getWorld(),vector.getBlockX(),vector.getBlockY(),vector.getBlockZ())))) {
                protector.destroy(event.getfPlayer().getPlayer());
            }
        }
    }

    void update2Sec() {
        if (protector!=null&&protector.isValid()&&protector.enabled()) {
            Collection<? extends Player> players = Bukkit.getOnlinePlayers();
            for (Player player: players) {
                protector.processPlayer(player);
            }
            protector.render(players.toArray(new Player[0]));
        }
        SkyTech.overworld.clear();
        SkyTech.overworld.add(Bukkit.getWorld("faction_crew"));
    }

    public boolean inClaim(Block block) {
        return inClaim(block.getLocation());
    }

    public boolean inClaim(Location location) {
        return inClaim(new FLocation(location));
    }

    public boolean inClaim(FLocation location) {
        return Board.getInstance().getFactionAt(location).equals(faction);
    }

    public Protector getProtector() {
        return this.protector;
    }

    public void setProtector(Protector p) {
        protector = p;
    }

    public HashMap<Location, Generator> getGenerators() {
        return generators;
    }

    public void setGenerators(HashMap<Location, Generator> gens) {
        generators.clear();
        for(Location loc : gens.keySet()) {
            generators.put(loc, gens.get(loc));
        }
    }
}
