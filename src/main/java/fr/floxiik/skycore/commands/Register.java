package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.SkyCore;

public class Register {

    public static void Commands() {
        SkyCore.getInstance().getCommand("skyitem").setExecutor(new ItemsCommands());
        SkyCore.getInstance().getCommand("bonus").setExecutor(new BonusCommands());
        SkyCore.getInstance().getCommand("action").setExecutor(new ActionCommand());
        SkyCore.getInstance().getCommand("skydiscord").setExecutor(new DiscordCommand());
        SkyCore.getInstance().getCommand("skytech").setExecutor(new TechCommands());
        SkyCore.getInstance().getCommand("rang").setExecutor(new RankCommand());
        SkyCore.getInstance().getCommand("upgradefaction").setExecutor(new UpgradeCommand());
        SkyCore.getInstance().getCommand("vote").setExecutor(new VoteCommand());
        SkyCore.getInstance().getCommand("tel").setExecutor(new TelCommand());
        SkyCore.getInstance().getCommand("telverify").setExecutor(new TelVerifyCommand());
        SkyCore.getInstance().getCommand("skydrop").setExecutor(new DropCommand());
        SkyCore.getInstance().getCommand("skydragon").setExecutor(new DragonCommand());
        SkyCore.getInstance().getCommand("startevent").setExecutor(new StartEventCommand());
    }
}
