package fr.floxiik.skycore.commands;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.Role;
import fr.floxiik.skycore.tech.FactionData;
import fr.floxiik.skycore.tech.Generator;
import fr.floxiik.skycore.tech.Protector;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class TechCommands implements CommandExecutor {

    public static final String prefix = "§c§lSkyTech §8» §e";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            FactionData factionData = FactionData.getFactionData((Player) sender);
            if (args.length == 0) {
                sender.sendMessage(prefix + "/skytech help");
                return false;
            }
            switch (args[0]) {
                default:
                    sender.sendMessage(prefix + "/skytech help");
                    return false;
                case "help":
                    sender.sendMessage(prefix + "/skytech help => Montre l'aide");
                    sender.sendMessage(prefix + "/skytech f,faction => Montre les infos sur les machines");
                    sender.sendMessage(prefix + "/skytech reset_gen => Réinitialiser les générateurs");
                    if (sender.hasPermission("skytech.admin")) {
                        sender.sendMessage(prefix + "/skytech save_all => Sauvegarde toutes les donnée");
                        sender.sendMessage(prefix + "/skytech reload_fd => Recharche toutes les donnée");
                        sender.sendMessage(prefix + "/skytech give_protect => Give le protecteur");
                    }
                    return true;
                case "f":
                case "faction":
                    sender.sendMessage(prefix + "Generateurs " + factionData.generators.size() + "/" + factionData.maxGenerator());
                    Protector protector = factionData.protector;
                    if (!protector.isValid()) {
                        sender.sendMessage(prefix + "Protecteur: " + ChatColor.DARK_RED + "OFFLINE");
                    } else {
                        protector.updateConsumption();
                        if (protector.enabled()) {
                            sender.sendMessage(prefix + "Protecteur: " + ChatColor.GREEN + protector.getEmeralds());
                        } else {
                            sender.sendMessage(prefix + "Protecteur: " + ChatColor.RED + protector.getEmeralds());
                        }
                    }
                    return true;
                case "reset_gen":
                    if (FPlayers.getInstance().getByPlayer((Player) sender).getRole() == Role.LEADER) {
                        for (Generator generator : factionData.generators.values().toArray(new Generator[factionData.generators.size()]))
                            generator.destroy();
                        sender.sendMessage(prefix + "Les generateurs ont été reset");
                    } else {
                        sender.sendMessage(prefix + "Vous devez étre LEADER de la faction pour faire ça");
                    }
                case "debug_gen":
                    sender.sendMessage(prefix + "Debug gen " + factionData.generators.size() + "/" + factionData.maxGenerator());
                    for (Generator generator : factionData.generators.values()) {
                        sender.sendMessage(prefix + generator.genType.name() + " " + generator.location.getBlockX() + " " + generator.location.getBlockY() + " " + generator.location.getBlockZ() + " ");
                    }
                    return true;
                case "save_all":
                    if (sender.hasPermission("skytech.admin")) {
                        FactionData.saveAll();
                        sender.sendMessage(prefix + "All Faction data saved");
                        return true;
                    }
                    sender.sendMessage(prefix + "/labtech help");
                    return false;
                case "reload_fd":
                    if (sender.hasPermission("skytech.admin")) {
                        FactionData.reload();
                        sender.sendMessage(prefix + "All Faction data reloaded");
                        return true;
                    }
                    sender.sendMessage(prefix + "/labtech help");
                    return false;
                case "give_protect":
                    if (sender.hasPermission("skytech.admin")) {
                        ItemStack p = new ItemStack(Material.SEA_LANTERN);
                        ItemMeta itemMeta = p.getItemMeta();
                        itemMeta.setDisplayName("§c§lProtecteur");
                        itemMeta.setLore(Arrays.asList("", "§eCe bloc vous permet de", "§eproteger votre base contre", "§eles ennemis ! ", "", "§6Placez le au centre de votre base."));
                        p.setItemMeta(itemMeta);
                        ((Player) sender).getInventory().addItem(p);
                    }
            }
        } else {
            if (args.length == 1 && args[0].equals("save-all")) {
                FactionData.saveAll();
                sender.sendMessage("All Faction data saved");
                return true;
            }
            sender.sendMessage("Command reservé aux joueurs");
        }
        return false;
    }
}
