package fr.floxiik.skycore.commands;
import com.vonage.client.VonageClient;
import com.vonage.client.verify.VerifyRequest;
import com.vonage.client.verify.VerifyResponse;
import com.vonage.client.verify.VerifyStatus;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.AlertDAO;
import fr.floxiik.skycore.utils.TelNumber;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TelCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            if(args.length < 1) {
                sender.sendMessage("§c§lErreur §8» §eFaites: §6/tel <votre numéro de téléphone> §a(Uniquement pour les Français) §e!");
                return false;
            } else if(args[0].length() == 10) {
                if(args[0].startsWith("06") || args[0].startsWith("07")) {
                    AlertDAO alertDAO = new AlertDAO();
                    TelNumber tel = alertDAO.getTel(sender.getName());
                    try {
                        if(tel.getRequestId().equals("verified")) {
                            sender.sendMessage("§c§lErreur §8» §eVotre numéro de téléphone est déjà §aenregistré §e!");
                            return false;
                        }
                    } catch (NullPointerException e) {

                    }
                    Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            VonageClient client = new VonageClient.Builder()
                                    .apiKey("79073e64")
                                    .apiSecret("13pf3gL42HWLOQvw")
                                    .build();
                            VerifyResponse response = client.getVerifyClient().verify("33" + args[0].substring(1), "SkyFactions", VerifyRequest.Workflow.SMS);

                            if (response.getStatus() == VerifyStatus.OK) {
                                sender.sendMessage("§c§lInfo §8» §eVous venez de recevoir un code de vérification !");
                                sender.sendMessage("§c§lInfo §8» §eFaites: §6/telverify <code reçu par sms> §a(Pour confirmer votre numéro) §e!");
                                alertDAO.setTel(sender.getName(), args[0], response.getRequestId());
                            } else {
                                sender.sendMessage("§c§lErreur §8» §eUne erreur est §6survenue §edurant l'envoi du code de vérification §e!");
                            }
                        }
                    });
                    return true;
                } else {
                    sender.sendMessage("§c§lErreur §8» §eLe numéro de téléphone doit commencer par §a06 §eou §a07 §e!");
                    sender.sendMessage("§c§lErreur §8» §eFaites: §6/tel <votre numéro de téléphone> §a(Uniquement pour les Français) §e!");
                    return false;
                }
            } else {
                sender.sendMessage("§c§lErreur §8» §eNuméro de téléphone incorrect §6(10 Chiffres)§e!");
                sender.sendMessage("§c§lErreur §8» §eFaites: §6/tel <votre numéro de téléphone> §a(Uniquement pour les Français) §e!");
                return false;
            }
        }
        return false;
    }
}
