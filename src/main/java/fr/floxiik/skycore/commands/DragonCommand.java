package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.actions.Pause;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.events.DragonManager;
import fr.floxiik.skycore.events.EventManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DragonCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender.hasPermission("skycore.event.dragon")) {
            if(args.length > 0) {
                if(args[0].equals("start")) {
                    DragonManager.startDragon();
                    return true;
                } else if(args[0].equals("stop")) {
                    DragonManager.stopDragon();
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
}
