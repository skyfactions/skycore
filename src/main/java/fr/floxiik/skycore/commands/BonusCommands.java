package fr.floxiik.skycore.commands;

import com.Zrips.CMI.CMI;
import com.Zrips.CMI.Modules.Economy.VaultManager;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.bonus.Spectator;
import fr.floxiik.skycore.help.PlayerHelp;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.VaultEco;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class BonusCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            if(args.length < 1) {
                sender.sendMessage("§c§lErreur §8» §eUsage: §6/bonus spectateur §e!");
                return false;
            } else if(args[0].contains("spectateur")) {
                if(args.length < 2) {
                    sender.sendMessage(PlayerHelp.prefix);
                    sender.sendMessage("");
                    sender.sendMessage("§l➢ §eEtes vous §asûr §e? Le mode spectateur");
                    sender.sendMessage("§l➢ §ecoûte §c100,000$§e pour §a1 §eminutes.");
                    PlayerHelp.sendJson((Player) sender, "§l➢ §eFaites §a/bonus spectateur confirmer", "§aClique pour confirmer", "/bonus spectateur confirmer");
                    sender.sendMessage("§l➢ §epour confirmer !");
                    sender.sendMessage("");
                    sender.sendMessage(PlayerHelp.suffix);
                    return false;
                } else if(args[1].contains("confirmer")) {
                    Spectator.setSpectator((Player) sender, 60);
                    return true;
                } else {
                    sender.sendMessage("§c§lErreur §8» §eUsage: §6/bonus spectateur &a<confirmer> §e!");
                    return false;
                }
            }
            return false;
        }
        return false;
    }
}
