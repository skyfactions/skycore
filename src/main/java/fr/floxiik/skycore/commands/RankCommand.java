package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.crew.guis.RankGui;
import net.luckperms.api.LuckPermsProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

public class RankCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            if(LuckPermsProvider.get().getGroupManager().getGroup(LuckPermsProvider.get().getUserManager().getUser(sender.getName()).getPrimaryGroup()).getWeight().getAsInt() >= 997) {
                if (args.length > 0) {
                    new RankGui(args[0]).openInventory((HumanEntity) sender);
                    return true;
                } else {
                    sender.sendMessage("§c§lErreur §8» §eVous devez renseigner un nom de joueur !");
                    return false;
                }
            } else {
                sender.sendMessage("§c§lErreur §8» §eVous n'avez pas la permission !");
            }
        }
        return false;
    }
}