package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.SkyCore;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DiscordCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            if(args[0].equalsIgnoreCase("link")) {
                Bukkit.getServer().getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        Member member = DiscordUtil.getMemberById(args[1]);
                        DiscordUtil.removeRolesFromMember(member, DiscordUtil.getRole("704436655657844796"));
                        DiscordUtil.addRoleToMember(member, DiscordUtil.getRole("785970012262629428"));
                        DiscordUtil.setNickname(member, args[2]);
                    }
                }, 20 * 5);
            }
            if(args[0].equalsIgnoreCase("unlink")) {
                Bukkit.getServer().getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        Member member = DiscordUtil.getMemberById(args[1]);
                        DiscordUtil.removeRolesFromMember(member, DiscordUtil.getRole("785970012262629428"));
                        DiscordUtil.addRoleToMember(member, DiscordUtil.getRole("704436655657844796"));
                    }
                }, 20 * 5);
            }

            return true;
        }
        return false;
    }
}
