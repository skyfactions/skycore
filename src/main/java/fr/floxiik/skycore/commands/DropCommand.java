package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.actions.Pause;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.drop.DropChests;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DropCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender.hasPermission("skycore.drop")) {
            if(args[0].equals("spawn")) {
                if(args.length > 1) {
                    DropChests.spawnDrops(Integer.parseInt(args[1]));
                } else {
                    DropChests.spawnDrops(1);
                }
                return true;
            } else if(args[0].equals("clear")) {
                DropChests.removeAllDrops();
            }
            return true;
        }
        return false;
    }
}
