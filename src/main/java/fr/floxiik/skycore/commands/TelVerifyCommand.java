package fr.floxiik.skycore.commands;

import com.vonage.client.VonageClient;
import com.vonage.client.verify.CheckResponse;
import com.vonage.client.verify.VerifyStatus;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.AlertDAO;
import fr.floxiik.skycore.utils.TelNumber;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TelVerifyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                sender.sendMessage("§c§lErreur §8» §eFaites: §6/telverify <code reçu par sms> §e!");
                return false;
            } else if (args[0].length() == 4) {
                Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        AlertDAO alertDAO = new AlertDAO();
                        TelNumber tel = alertDAO.getTel(sender.getName());

                        VonageClient client = new VonageClient.Builder()
                                .apiKey("79073e64")
                                .apiSecret("13pf3gL42HWLOQvw")
                                .build();

                        String requestId = "";
                        try {
                            requestId = tel.getRequestId();
                        } catch(NullPointerException e) {
                            sender.sendMessage("§c§lErreur §8» §eVous n'avez pas §6renseigné de numéro de téléphone §e!");
                            sender.sendMessage("§c§lInfo §8» §eFaites: §6/tel <votre numéro de téléphone> §a(Uniquement pour les Français) §e!");
                        }

                        CheckResponse response = client.getVerifyClient().check(requestId, args[0]);

                        if (response.getStatus() == VerifyStatus.OK) {
                            sender.sendMessage("§c§lInfo §8» §eNuméro de téléphone §avalidé §eavec succès !");
                            alertDAO.setTel(tel.getOwner(), tel.getNumber(), "verified");
                        } else {
                            System.out.printf("ERROR! " + response.getStatus() + ":" +  response.getErrorText());
                            sender.sendMessage("§c§lErreur §8» §eCode de vérification §cérroné ou §cexpiré §e!");
                        }

                    }
                });
                return true;
            } else {
                sender.sendMessage("§c§lErreur §8» §eLe code de §avérification doit contenir §c4 §echiffres §e!");
                sender.sendMessage("§c§lErreur §8» §eFaites: §6/telverify <code reçu par sms> §e!");
                return false;
            }
        }
        return false;
    }
}
