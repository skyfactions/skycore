package fr.floxiik.skycore.commands;

import com.Zrips.CMI.CMI;
import com.Zrips.CMI.Modules.Economy.Economy;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import fr.floxiik.skycore.crew.Crew;
import fr.floxiik.skycore.crew.CrewUpgrade;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.crew.guis.UpgradeGui;
import fr.floxiik.skycore.dao.VoteDAO;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

public class UpgradeCommand implements CommandExecutor {

    private int[] members = {4, 8, 10};
    private long[] money = {1000000, 50000000, 1000000000};
    private int[] votes = {50, 200, 500};

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            FPlayer fp = FPlayers.getInstance().getByPlayer(p);
            CrewDAO crewDAO = new CrewDAO();
            Crew crew = crewDAO.getPlayerCrew(p);
            if(!fp.hasFaction()) {
                p.sendMessage("§c§lErreur§f│ §eVous devez §6posséder §eune §cFaction §e!");
                return false;
            }
            if(!fp.getRole().getRoleCapitalized().equals("Leader")) {
                p.sendMessage("§c§lErreur§f│ §eVous devez être §6Chef §ede la §cFaction §e!");
                return false;
            }
            if(fp.getFaction().getFPlayers().size() < members[crew.getLevel()-1]) {
                p.sendMessage("§c§lErreur§f│ §eVous n'avez pas assez de §6membres §edans votre §cFaction §e!");
                return false;
            }
            int vote = 0;
            VoteDAO voteDAO = new VoteDAO();
            for(FPlayer fpl : fp.getFaction().getFPlayers()) {
                vote = vote + voteDAO.getPlayerVote(fpl.getName());
            }
            if(vote < votes[crew.getLevel()-1]) {
                p.sendMessage("§c§lErreur§f│ §eVotre §6faction n'as pas faite assez de §cvotes §e!");
                return false;
            }
            if(Economy.getBalance(p.getName()) < money[crew.getLevel()-1]) {
                p.sendMessage("§c§lErreur§f│ §eVous n'avez pas assez d'§cargent §e!");
                return false;
            }
            if(crew.isUpdate()) {
                p.sendMessage("§c§lErreur§f│ §eVous avez déjà une §6amélioration §een §aAttente §e!");
                return false;
            }
            Economy.withdrawPlayer(p.getName(), money[crew.getLevel()-1]);
            p.sendMessage("§c§lFaction§f│ §eVous avez §6amélioré §evotre §cFaction §eavec §asuccès §e!");
            p.sendMessage("§c§lFaction§f│ §eL'amélioration sera §aeffective§e à la fin de la §6prochaine §ephase de §cPillage§e.");
            CrewUpgrade.upgrade(p);
        }
        return false;
    }
}
