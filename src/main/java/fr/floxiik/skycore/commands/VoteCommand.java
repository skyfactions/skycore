package fr.floxiik.skycore.commands;

import com.Zrips.CMI.CMI;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.VoteDAO;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VoteCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender.hasPermission("skycore.vote")) {
            VoteDAO voteDAO = new VoteDAO();
            if(args[0].equals("add")) {
                SkyCore.getInstance().setActual_votes(SkyCore.getInstance().getActual_votes() + Integer.parseInt(args[1]));
                if(args.length > 2) {
                    for(Player p : Bukkit.getOnlinePlayers()) {
                        CMI.getInstance().getActionBarManager().send(p, "§c§lVotes §8» §a" + args[2] + " §ea §6voté §epour le serveur ! §c" + (SkyCore.getInstance().getNeeded_votes() - SkyCore.getInstance().getActual_votes()) + "§e restants, §a/vote§e.");
                    }
                    CommandSender cs = Bukkit.getConsoleSender();
                    Bukkit.dispatchCommand(cs, "crazycrate give virtual vote 1 " + args[2]);
                    voteDAO.addPlayerVote(args[2]);
                }
            } else if(args[0].equals("remove")) {
                SkyCore.getInstance().setActual_votes(SkyCore.getInstance().getActual_votes() - Integer.parseInt(args[1]));
            } else if(args[0].equals("set")) {
                SkyCore.getInstance().setActual_votes(Integer.parseInt(args[1]));
            } else if(args[0].equals("setmax")) {
                SkyCore.getInstance().setNeeded_votes(Integer.parseInt(args[1]));
                new VoteDAO().updateNeededVotes(Integer.parseInt(args[1]));
            } else if(args[0].equals("reset")) {
                new VoteDAO().resetVotes();
            }
            if(SkyCore.getInstance().getActual_votes() >= SkyCore.getInstance().getNeeded_votes()) {
                startVoteParty();
            }
            if(SkyCore.getInstance().getActual_votes() == (SkyCore.getInstance().getNeeded_votes() / 2)) {
                startMiniVoteParty();
            }
            voteDAO.updateVotes(SkyCore.getInstance().getActual_votes(), SkyCore.getInstance().getNeeded_votes());
            return true;
        }
        return false;
    }

    public static void startMiniVoteParty() {
        for(Player p : Bukkit.getOnlinePlayers()) {
            CommandSender s = Bukkit.getConsoleSender();
            Bukkit.dispatchCommand(s, "cmi money add " + p.getName() + " 100000");
            CMI.getInstance().getActionBarManager().send(p, "§7(§c!§7) §eObjectif de §6votes §eatteint ! Vous recevez §a100000§2$§e ! §7(§c!§7)");
        }
        Bukkit.broadcastMessage("§c§lVotes §8» §eObjectif de §6votes §eatteint ! Vous recevez §a100000§2$§e !");
    }

    public static void startVoteParty() {
        SkyCore.getInstance().setActual_votes(0);
        for(Player p : Bukkit.getOnlinePlayers()) {
            CommandSender s = Bukkit.getConsoleSender();
            Bukkit.dispatchCommand(s, "cmi money add " + p.getName() + " 700000");
            CMI.getInstance().getActionBarManager().send(p, "§7(§c!§7) §eObjectif de §6votes §eatteint ! Vous recevez §a700000§2$§e ! §7(§c!§7)");
        }
        Bukkit.broadcastMessage("§c§lVotes §8» §eObjectif de §6votes §eatteint ! Vous recevez §a700000§2$§e !");
    }
}
