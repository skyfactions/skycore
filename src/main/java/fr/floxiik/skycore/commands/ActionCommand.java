package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.actions.Pause;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.bonus.Spectator;
import fr.floxiik.skycore.help.PlayerHelp;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ActionCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            if(Action.getDayAction().equals("pause")) {
                Pause.safeStop();
            } else {
                Pillage.startPillage();
            }
            return true;
        }
        return false;
    }
}
