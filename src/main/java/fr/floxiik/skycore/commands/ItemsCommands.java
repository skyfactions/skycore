package fr.floxiik.skycore.commands;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemsCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender.hasPermission("skyitem.give")) {
            if (args.length < 1) {
                sender.sendMessage("§c§lSkyCore §8» §eUsage: /skyitem give <joueur> <item> <montant>");
                return false;
            } else if (args[0].contains("give")) {
                if (args.length < 2) {
                    sender.sendMessage("§c§lSkyCore §8» §eUsage: /skyitem give <joueur> <item> <montant>");
                    return false;
                } else if (Bukkit.getOnlinePlayers().contains(Bukkit.getPlayer(args[1]))) {
                    if (args.length < 3) {
                        sender.sendMessage("§c§lSkyCore §8» §eUsage: /skyitem give <joueur> <item> <montant>");
                        return false;
                    } else if (args[2].contains("hammer")) {
                        ItemStack hammer;
                        if (args.length < 5) {
                            hammer = new ItemStack(Material.DIAMOND_PICKAXE, Integer.parseInt(args[3]));
                        } else {
                            hammer = new ItemStack(Material.DIAMOND_PICKAXE, 1);
                        }
                        ItemMeta meta = hammer.getItemMeta();
                        meta.setDisplayName("§c§lHammer");
                        List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§eCette pioche vous permet");
                        lore.add("§ede miner par 3x3x1 !");
                        meta.setLore(lore);
                        meta.addEnchant(Enchantment.DURABILITY, 8, true);
                        meta.addEnchant(Enchantment.DIG_SPEED, 2, true);
                        hammer.setItemMeta(meta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(hammer);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + hammer.getAmount() + " §6Hammer(s)§e au joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + hammer.getAmount() + " §cHammer(s) §e!");
                        return true;
                    } else if (args[2].contains("houe")) {
                        ItemStack houe;
                        if (args.length < 5) {
                            houe = new ItemStack(Material.DIAMOND_HOE, Integer.parseInt(args[3]));
                        } else {
                            houe = new ItemStack(Material.DIAMOND_HOE, 1);
                        }
                        ItemMeta meta = houe.getItemMeta();
                        meta.setDisplayName("§c§lHoue");
                        List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§eCette houe vous permet");
                        lore.add("§ede planter en recoltant !");
                        meta.setLore(lore);
                        meta.addEnchant(Enchantment.DURABILITY, 5, true);
                        houe.setItemMeta(meta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(houe);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + houe.getAmount() + " §6Houe(s) §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + houe.getAmount() + " §cHoue(s) §e!");
                        return true;
                    } else if (args[2].contains("grenade")) {
                        ItemStack grenade;
                        if (args.length < 5) {
                            grenade = new ItemStack(Material.SNOW_BALL, Integer.parseInt(args[3]));
                        } else {
                            grenade = new ItemStack(Material.SNOW_BALL, 1);
                        }
                        ItemMeta meta = grenade.getItemMeta();
                        meta.setDisplayName("§c§lGrenade");
                        List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§eCette grenade permet");
                        lore.add("§ed'exploser la base ennemie !");
                        meta.setLore(lore);
                        grenade.setItemMeta(meta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(grenade);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + grenade.getAmount() + " §6Grenade(s) §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + grenade.getAmount() + " §cGrenade(s) §e!");
                        return true;
                    } else if (args[2].contains("arc")) {
                        ItemStack arc;
                        arc = new ItemStack(Material.BOW, 1);
                        ItemMeta meta = arc.getItemMeta();
                        meta.setDisplayName("§eArc à §aponts §7(§c3 coups§7)");
                        List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§eCette §6arc §evous §apermet §ede");
                        lore.add("§efaire un §cpont §edans le §2wilderness");
                        lore.add("§epour vous §adéplacer §eplus §6facilement");
                        lore.add("§elors des §cpillages §e!");
                        lore.add("");
                        lore.add("§6Cet arc peut être utilisé §63 §efois !");
                        meta.setLore(lore);
                        arc.setItemMeta(meta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(arc);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + arc.getAmount() + " §cArc à ponts §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + arc.getAmount() + " §cArc à ponts §e!");
                        return true;
                    } else if(args[2].contains("block")) {
                        ItemStack block;
                        if (args.length < 5) {
                            block = new ItemStack(Material.GLASS, Integer.parseInt(args[3]));
                        } else {
                            block = new ItemStack(Material.GLASS, 1);
                        }
                        ItemMeta itemMeta = block.getItemMeta();
                        itemMeta.setDisplayName("§c§lBlock d'Assault");
                        List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§eCe §cblock §evous §6permet");
                        lore.add("§ede vous §adéplacer §een");
                        lore.add("§6Safezone §edurant la phase");
                        lore.add("§ede §cPillage §e!");
                        itemMeta.setLore(lore);
                        block.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(block);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + block.getAmount() + " §cBlock(s) d'Assault §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + block.getAmount() + " §cBlock(s) d'Assault §e!");
                        return true;
                    }  else if (args[2].contains(":")) {
                        ItemStack potion;
                        String[] pot = args[2].split(":");
                        if (args.length < 5) {
                            potion = new ItemStack(Material.POTION, Integer.parseInt(args[3]), (short) Integer.parseInt(pot[1]));
                        } else {
                            potion = new ItemStack(Material.POTION, 1, (short) Integer.parseInt(pot[1]));
                        }
                        Bukkit.getPlayer(args[1]).getInventory().addItem(potion);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give une §6potion §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu une potion §e!");
                        return true;
                    } else if (args[2].contains("spawner")) {
                        ItemStack spawner = new ItemStack(Material.MOB_SPAWNER, Integer.parseInt(args[4]));
                        BlockStateMeta meta = (BlockStateMeta) spawner.getItemMeta();
                        CreatureSpawner spawner_type = (CreatureSpawner) meta.getBlockState();
                        switch (args[3]) {
                            case "creeper":
                                spawner_type.setSpawnedType(EntityType.CREEPER);
                                break;
                            case "skeleton":
                                spawner_type.setSpawnedType(EntityType.SKELETON);
                                break;
                            case "spider":
                                spawner_type.setSpawnedType(EntityType.SPIDER);
                                break;
                            case "zombie":
                                spawner_type.setSpawnedType(EntityType.ZOMBIE);
                                break;
                            case "slime":
                                spawner_type.setSpawnedType(EntityType.SLIME);
                                break;
                            case "ghast":
                                spawner_type.setSpawnedType(EntityType.GHAST);
                                break;
                            case "pigzombie":
                                spawner_type.setSpawnedType(EntityType.PIG_ZOMBIE);
                                break;
                            case "enderman":
                                spawner_type.setSpawnedType(EntityType.ENDERMAN);
                                break;
                            case "cavespider":
                                spawner_type.setSpawnedType(EntityType.CAVE_SPIDER);
                                break;
                            case "silverfish":
                                spawner_type.setSpawnedType(EntityType.SILVERFISH);
                                break;
                            case "blaze":
                                spawner_type.setSpawnedType(EntityType.BLAZE);
                                break;
                            case "magmacube":
                                spawner_type.setSpawnedType(EntityType.MAGMA_CUBE);
                                break;
                            case "wither":
                                spawner_type.setSpawnedType(EntityType.WITHER);
                                break;
                            case "bat":
                                spawner_type.setSpawnedType(EntityType.BAT);
                                break;
                            case "witch":
                                spawner_type.setSpawnedType(EntityType.WITCH);
                                break;
                            case "pig":
                                spawner_type.setSpawnedType(EntityType.PIG);
                                break;
                            case "sheep":
                                spawner_type.setSpawnedType(EntityType.SHEEP);
                                break;
                            case "cow":
                                spawner_type.setSpawnedType(EntityType.COW);
                                break;
                            case "chicken":
                                spawner_type.setSpawnedType(EntityType.CHICKEN);
                                break;
                            case "squid":
                                spawner_type.setSpawnedType(EntityType.SQUID);
                                break;
                            case "wolf":
                                spawner_type.setSpawnedType(EntityType.WOLF);
                                break;
                            case "mooshroom":
                                spawner_type.setSpawnedType(EntityType.MUSHROOM_COW);
                                break;
                            case "snowman":
                                spawner_type.setSpawnedType(EntityType.SNOWMAN);
                                break;
                            case "irongolem":
                                spawner_type.setSpawnedType(EntityType.IRON_GOLEM);
                                break;
                            case "horse":
                                spawner_type.setSpawnedType(EntityType.HORSE);
                                break;
                            case "ocelot":
                                spawner_type.setSpawnedType(EntityType.OCELOT);
                                break;
                            case "villager":
                                spawner_type.setSpawnedType(EntityType.VILLAGER);
                                break;
                            case "loup":
                                spawner_type.setSpawnedType(EntityType.WOLF);
                                break;
                            default:
                                spawner_type.setSpawnedType(EntityType.PIG);
                                break;
                        }
                        meta.setBlockState(spawner_type);
                        meta.setDisplayName("§eSpawner §7(§c" + spawner_type.getSpawnedType().getName() + "§7)");
                        spawner.setItemMeta(meta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(spawner);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + spawner.getAmount() + " §cSpawner(s) §eà §a" + spawner_type.getCreatureTypeName() + " §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + spawner.getAmount() + " §cSpawner(s) §eà §a" + spawner_type.getCreatureTypeName() + "§e!");
                    } else if(args[2].contains("pioche")) {
                        ItemStack pioche;
                        pioche = new ItemStack(Material.DIAMOND_PICKAXE, 1);
                        ItemMeta itemMeta = pioche.getItemMeta();
                        itemMeta.setDisplayName("§ePioche à §aSpawners §7(§c" + args[3] + " coups§7)");
                        itemMeta.setLore(Arrays.asList("§r", "§eCette pioche vous permet", "§ede §6récuperer§e les §aspawners§e."));
                        pioche.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(pioche);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + pioche.getAmount() + " §cPioche à Spawner §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + pioche.getAmount() + " §cPioche à Spawner §e!");
                        return true;
                    } else if(args[2].contains("glaive")) {
                        ItemStack glaive;
                        glaive = new ItemStack(Material.DIAMOND_SWORD, 1);
                        ItemMeta itemMeta = glaive.getItemMeta();
                        itemMeta.setDisplayName("§cGlaive");
                        itemMeta.addEnchant(Enchantment.FIRE_ASPECT, 5, true);
                        itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 7, true);
                        itemMeta.addEnchant(Enchantment.DURABILITY, 5, true);
                        itemMeta.setLore(Arrays.asList("§r", "§eVous ne §6perdrez §ejamais", "§ecette §aépée§e, même quand", "§evous §cmourrez §e!"));
                        glaive.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(glaive);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + glaive.getAmount() + " §cGlaive §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + glaive.getAmount() + " §cGlaive §e!");
                        return true;
                    } else if(args[2].contains("briquet")) {
                        ItemStack briquet;
                        briquet = new ItemStack(Material.FLINT_AND_STEEL, 1);
                        ItemMeta itemMeta = briquet.getItemMeta();
                        itemMeta.setDisplayName("§eCharge de §aCreeper §7(§c" + args[3] + " coups§7)");
                        itemMeta.addEnchant(Enchantment.KNOCKBACK, 3, true);
                        itemMeta.addEnchant(Enchantment.FIRE_ASPECT, 3, true);
                        itemMeta.setLore(Arrays.asList("§r", "§eUtilisez ce §6briquet", "§esur un §acreeper §epour en faire", "§eun creeper §cchargé §e!"));
                        briquet.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(briquet);
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + briquet.getAmount() + " §cChargeur §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + briquet.getAmount() + " §cChargeur §e!");
                        return true;
                    } else if(args[2].contains("obsi")) {
                        ItemStack obsi;
                        obsi = new ItemStack(Material.OBSIDIAN, Integer.parseInt(args[3]));
                        ItemMeta itemMeta = obsi.getItemMeta();
                        itemMeta.setDisplayName("§9§lObsidienne §eGenWall");
                        itemMeta.setLore(Arrays.asList("§r", "§eUtilisez cette §9Obsidienne §epour", "§econstruire un mur de haut en bas", "§een un instant §e!"));
                        obsi.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(addGlow(obsi));
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + obsi.getAmount() + " §cObsi GenWall §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + obsi.getAmount() + " §cObsidienne GenWall §e!");
                        return true;
                    } else if(args[2].contains("sand")) {
                        ItemStack sand;
                        sand = new ItemStack(Material.SAND, Integer.parseInt(args[3]));
                        ItemMeta itemMeta = sand.getItemMeta();
                        itemMeta.setDisplayName("§6§lSable §eGenWall");
                        itemMeta.setLore(Arrays.asList("§r", "§eUtilisez ce §6Sable §epour", "§econstruire un mur de haut en bas", "§een un instant §e!"));
                        sand.setItemMeta(itemMeta);
                        Bukkit.getPlayer(args[1]).getInventory().addItem(addGlow(sand));
                        sender.sendMessage("§c§lSkyCore §8» §eVous avez give §c" + sand.getAmount() + " §cSable GenWall §eau joueur: §6" + args[1] + " §e!");
                        Bukkit.getPlayer(args[1]).sendMessage("§c§lInfo §8» §eVous avez reçu " + sand.getAmount() + " §cSable GenWall §e!");
                        return true;
                    }
                } else {
                    Bukkit.broadcastMessage(args[1]);
                    sender.sendMessage("§c§lSkyCore §8» §eJoueur introuvable ou hors ligne !");
                    return false;
                }
            }
        }
        return false;
    }

    public static void addGlaive(Player p) {
        ItemStack glaive;
        glaive = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta itemMeta = glaive.getItemMeta();
        itemMeta.setDisplayName("§cGlaive");
        itemMeta.addEnchant(Enchantment.FIRE_ASPECT, 5, true);
        itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 7, true);
        itemMeta.addEnchant(Enchantment.DURABILITY, 5, true);
        itemMeta.setLore(Arrays.asList("§r", "§eVous ne §6perdrez §ejamais", "§ecette §aépée§e, même quand", "§evous §cmourrez §e!"));
        glaive.setItemMeta(itemMeta);
        p.getInventory().addItem(glaive);
    }

    private CraftItemStack addGlow(ItemStack stack) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
}