package fr.floxiik.skycore.commands;

import fr.floxiik.skycore.actions.Action;
import fr.floxiik.skycore.actions.Pause;
import fr.floxiik.skycore.actions.Pillage;
import fr.floxiik.skycore.utils.RandomGenerator;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class StartEventCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player) || sender.isOp()) {
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
            if(args[0].equals("nexus")) {
                Bukkit.dispatchCommand(console, "nexus spawn nexus" + RandomGenerator.between(1, 4));
            } else if(args[0].equals("totem")) {
                Bukkit.dispatchCommand(console, "totem spawn totem" + RandomGenerator.between(1, 4));
            } else if(args[0].equals("koth")) {
                Bukkit.dispatchCommand(console, "koth spawn koth" + RandomGenerator.between(1, 4));
            }
        }
        return false;
    }
}
