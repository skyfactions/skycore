package fr.floxiik.skycore.worlds;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

import java.util.ArrayList;
import java.util.List;

public class WorldCreation {

    private static String level = "faction_crew";

    public static void create(String name) {
        WorldCreator wc = new WorldCreator(name);

        wc.type(WorldType.FLAT);
        wc.generatorSettings("2;0;1;");
        wc.generateStructures(false);
        wc.createWorld();
    }

    public static void init() {
        List<World> worlds = new ArrayList<>(Bukkit.getWorlds());
        List<String> names = new ArrayList<>();
        for (World world : worlds) {
            names.add(world.getName());
        }
        if(!names.contains("faction_crew")) {
            create("faction_crew");
        }
        if(!names.contains("faction_warzone")) {
            create("faction_warzone");
        }
        if(!names.contains("faction_event_1")) {
            create("faction_event_1");
        }
        if(!names.contains("faction_event_2")) {
            create("faction_event_2");
        }
        if(!names.contains("faction_event_3")) {
            create("faction_event_3");
        }
        if(!names.contains("faction_event_4")) {
            create("faction_event_4");
        }
        if(!names.contains("faction_duel")) {
            create("faction_duel");
        }
    }
}
