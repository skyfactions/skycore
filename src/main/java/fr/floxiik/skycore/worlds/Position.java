package fr.floxiik.skycore.worlds;

import java.util.HashMap;

public class Position {

    private int id;
    private String faction;
    private int posX;
    private int posZ;
    private static HashMap<Integer, Position> positions = new HashMap<>();


    public Position(int id, int posX, int posZ) {
        this.id = id;
        this.posX = posX;
        this.posZ = posZ;
    }

    public static void createPositions(int level) {
        if (level == 1) {
            layout(3600, 60);
        }
    }

    public static void layout(int plots, int size) {
        int[] items = new int[plots];
        int[][] matrix = new int[size][size];
        for (int i = 0; i < plots; i++) {
            items[i] = i;
        }
        int length = items.length;
        int width = matrix.length;
        int height = matrix[0].length;
        int lastRow = height - 1;

        int a = 0;
        int b = 0;
        int A = 0;
        int B = 0;
        boolean firstColumn = true;

        for (int i = 0; i < length; i++) {
            matrix[a][b] = items[i];
            Position.addIdPosition(items[i], new Position(items[i], a, b));

            if (b == height - 1) {
                firstColumn = false;
            }
            b--;
            a++;
            if (firstColumn) {
                if (b < 0) {
                    a = 0;
                    B++;
                    b = B;
                }
            } else {
                if ((a == width) || ((a == width - 1) && (b < 0))) {
                    A++;
                    a = A;
                    b = height - 1;
                }
            }
        }
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getPositionX() {
        return this.posX;
    }

    public int getPositionZ() {
        return this.posZ;
    }

    public void setPositionX(int p) {
        this.posX = p;
    }

    public void setPositionZ(int p) {
        this.posZ = p;
    }

    public static HashMap<Integer, Position> getPositions() {
        return positions;
    }

    public static void addIdPosition(int id, Position pos) {
        positions.put(id, pos);
    }
}
