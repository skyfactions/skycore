package fr.floxiik.skycore.worlds;

import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;

public class WorldDelete {

    private static String[] levels = {"level_1", "level_2", "level_3", "level_4"};

    public static void delete() {
        for (String level : levels) {
            World delete = Bukkit.getWorld(level);
            if(!Bukkit.getServer().unloadWorld(delete, false)) {
                System.err.println("Erreur lors de l'unload du monde " + level + " !");
            }
            File deleteFolder = delete.getWorldFolder();
            if(!deleteWorld(deleteFolder)) {
                System.err.println("Erreur lors de la suppression du monde " + level + " !");
            }
        }
    }

    public static void unloadMap(String mapname){

    }

    public static boolean deleteWorld(File path) {
        if(path.exists()) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteWorld(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return(path.delete());
    }
}
