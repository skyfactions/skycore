package fr.floxiik.skycore.events;

import fr.floxiik.skycore.SkyCore;
import fr.maxlego08.zkoth.ZKothManager;
import fr.maxlego08.zkoth.ZKothPlugin;
import fr.maxlego08.zkoth.api.KothManager;
import fr.maxlego08.znexus.NexusManager;
import fr.maxlego08.ztotem.TotemManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class EventScheduler {

    public EventScheduler() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                verifEvent();
            }
        }, 20, 10 * 20);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                sendAlert();
            }
        }, 20, 30 * 20);
    }

    private void verifEvent() {
        if(!TotemManager.getInstance().getActivesTotems().isEmpty()) {
            EventManager.setActualLocations(TotemManager.getInstance().getActivesTotems().get(0).getLocation().getWorld());
        } else if(!NexusManager.getInstance().getActivesNexus().isEmpty()) {
            EventManager.setActualLocations(NexusManager.getInstance().getActivesNexus().get(0).getLocation().getWorld());
        } else if(!SkyCore.getPlugin(ZKothPlugin.class).getKothManager().getActiveKoths().isEmpty()) {
            EventManager.setActualLocations(SkyCore.getPlugin(ZKothPlugin.class).getKothManager().getActiveKoths().get(0).getCenter().getWorld());
        } else if(DragonManager.isSpawned()) {
            EventManager.setActualLocation(new Location(Bukkit.getWorld("faction_the_end"), 48, 163, 240, 0, 0));
        }
    }

    private void sendAlert() {
        if(!TotemManager.getInstance().getActivesTotems().isEmpty()) {
            Bukkit.broadcastMessage("§c§lTotem §8» §eUn event §cTotem §eest en cours !");
            Bukkit.broadcastMessage("§c§lTotem §8» §eFais §a/event §epour y être §6téléporté §e!");
        } else if(!NexusManager.getInstance().getActivesNexus().isEmpty()) {
            Bukkit.broadcastMessage("§c§lNexus §8» §eUn event §cNexus §eest en cours !");
            Bukkit.broadcastMessage("§c§lNexus §8» §eFais §a/event §epour y être §6téléporté §e!");
        } else if(!SkyCore.getPlugin(ZKothPlugin.class).getKothManager().getActiveKoths().isEmpty()) {
            Bukkit.broadcastMessage("§c§lKOTH §8» §eUn event §cKOTH §eest en cours !");
            Bukkit.broadcastMessage("§c§lKOTH §8» §eFais §a/event §epour y être §6téléporté §e!");
        } else if(DragonManager.isSpawned()) {
            Bukkit.broadcastMessage("§c§lDragon §8» §eUn event §cDragon §eest en cours !");
            Bukkit.broadcastMessage("§c§lDragon §8» §eFais §a/event §epour y être §6téléporté §e!");
        }
    }
}
