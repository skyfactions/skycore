package fr.floxiik.skycore.events;

import com.SirBlobman.combatlogx.CombatLogX;
import com.SirBlobman.combatlogx.manager.CombatManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class EventManager {

    private static ArrayList<Location> locations = new ArrayList<>();
    private static ArrayList<Location> actualLocations = new ArrayList<>();
    private static Location spawn = new Location(Bukkit.getWorld("faction"), 4.5, 197.0, -2.5, -180.0f, 0.0f);

    public static ArrayList<Location> getLocations() {
        return locations;
    }

    public static void setLocations(ArrayList<Location> locations) {
        EventManager.locations.addAll(locations);
    }

    public static ArrayList<Location> getWorldLocations(World world) {
        ArrayList<Location> locationsWorld = new ArrayList<>();
        for(Location location : getLocations()) {
            if(location.getWorld().getName().equals(world.getName())) {
                locationsWorld.add(location);
            }
        }
        return locationsWorld;
    }

    public static void addLocation(Location location) {
        EventManager.locations.add(location);
    }

    public static void setActualLocations(World world) {
        actualLocations.clear();
        actualLocations.addAll(getWorldLocations(world));
    }

    public static void setActualLocation(Location location) {
        actualLocations.clear();
        actualLocations.add(location);
    }

    public static ArrayList<Location> getActualLocations() {
        return actualLocations;
    }

    public static void resetActualLocations() {
        actualLocations.clear();
    }

    public static void finishEvent(World world) {
        EventManager.resetActualLocations();
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(player.getLocation().getWorld().getName().equals(world.getName())) {
                player.teleport(spawn);
                player.sendMessage("§c§lFaction §8» §eL'Event est §aterminé §e! Vous avez été §6téléporté §eau spawn.");
            }
        }
    }
}
