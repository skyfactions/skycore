package fr.floxiik.skycore.events;

import be.maximvdw.featherboard.FeatherBoard;
import be.maximvdw.featherboard.api.FeatherBoardAPI;
import fr.floxiik.skycore.help.PlayerHelp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;

import java.util.*;

public class DragonManager {

    private static boolean spawned = false;
    private static World end = Bukkit.getWorld("faction_the_end");
    private static ArrayList<Location> locations = new ArrayList<>();
    public static HashMap<Player, Integer> points = new HashMap<>();
    public static double health;

    public static boolean isSpawned() {
        return spawned;
    }

    public static void setSpawned(boolean spawned) {
        DragonManager.spawned = spawned;
    }

    public static void startDragon() {
        spawned = true;
        EventManager.setActualLocation(new Location(Bukkit.getWorld("faction_the_end"), 48, 163, 240, 0, 0));
        points.clear();
        locations.clear();
        locations.add(new Location(end, 81.5, 209, 325.5));
        locations.add(new Location(end, 78.5, 214, 355.5));
        locations.add(new Location(end, 66.5, 198, 372.5));
        locations.add(new Location(end, 97.5, 216, 382.5));
        locations.add(new Location(end, 105.5, 220, 407.5));
        locations.add(new Location(end, 136.5, 205, 361.5));
        locations.add(new Location(end, 134.5, 197, 396.5));
        locations.add(new Location(end, 110.5, 206, 338.5));

        for(Location loc : locations) {
            end.getChunkAt(loc).load();
            end.spawn(loc, EnderCrystal.class);
        }

        end.getChunkAt(new Location(end, 116.5, 182, 376.5)).load();
        end.spawn(new Location(end, 116.5, 182, 376.5), EnderDragon.class);

        health = 1000;

        for(Player player : Bukkit.getOnlinePlayers()) {
            FeatherBoardAPI.showScoreboard(player, "dragon");
            PlayerHelp.sendCenteredMessage(player, "&8&m------------------------------------");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&eL'Event &cDragon&e est &alancé &e!");
            PlayerHelp.sendCenteredMessage(player, "&eVenez rapidement dans l'end pour combattre");
            PlayerHelp.sendCenteredMessage(player, "&ele dragon et gagner des points !");
            PlayerHelp.sendCenteredMessage(player, "&6Rappel: &eUn &dEnderCrystal &edétruit vous rapporte &c50 &epoints !");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&eFaites &c/event &epour vous y rendre.");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&8&m------------------------------------");
        }
    }

    public static void winDragon() {
        spawned = false;
        for(Entity entity : end.getEntities()) {
            if(entity.getType() == EntityType.ENDER_CRYSTAL || entity.getType() == EntityType.ENDER_DRAGON) {
                entity.remove();
            }
        }
        for(Player player : Bukkit.getOnlinePlayers()) {
            FeatherBoardAPI.showScoreboard(player, "default");
            PlayerHelp.sendCenteredMessage(player, "&8&m-----------------------------------");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&eL'Event &cDragon&e est &aterminé &e!");
            PlayerHelp.sendCenteredMessage(player, "&eMerci aux &6participants&e d'avoir tué");
            PlayerHelp.sendCenteredMessage(player, "&ece mauvais &cdragon&e !");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&6Classement des meilleurs Joueurs&8:");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&a#1 &e%skycore_dragon_player_1% &8- &c%skycore_dragon_points_1% Points");
            PlayerHelp.sendCenteredMessage(player, "&a#2 &e%skycore_dragon_player_2% &8- &c%skycore_dragon_points_2% Points");
            PlayerHelp.sendCenteredMessage(player, "&a#3 &e%skycore_dragon_player_3% &8- &c%skycore_dragon_points_3% Points");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&aVotre Score&8: &c%skycore_dragon_points_me% Points");
            PlayerHelp.sendCenteredMessage(player, "");
            PlayerHelp.sendCenteredMessage(player, "&8&m-----------------------------------");
        }

        CommandSender s = Bukkit.getConsoleSender();
        int i = 0;
        for(Player player : DragonManager.getClassementPlayer()) {
            if(i < 10) {
                Bukkit.dispatchCommand(s, "cmi money add " + player.getName() + " " + String.valueOf(100000*(10-i)));
            } else {
                Bukkit.dispatchCommand(s, "cmi money add " + player.getName() + " 100000");
            }
            if(i == 0) {
                Bukkit.dispatchCommand(s, "points give " + player.getName() + " 70");
                Bukkit.dispatchCommand(s, "zpoints give " + player.getName() + " 15");
            } else if(i == 1) {
                Bukkit.dispatchCommand(s, "points give " + player.getName() + " 50");
                Bukkit.dispatchCommand(s, "zpoints give " + player.getName() + " 10");
            } else if(i == 2) {
                Bukkit.dispatchCommand(s, "points give " + player.getName() + " 30");
                Bukkit.dispatchCommand(s, "zpoints give " + player.getName() + " 5");
            }
            i++;
        }
    }

    public static void stopDragon() {
        spawned = false;
        for(Entity entity : end.getEntities()) {
            if(entity.getType() == EntityType.ENDER_CRYSTAL || entity.getType() == EntityType.ENDER_DRAGON) {
                entity.remove();
            }
        }
        Bukkit.broadcastMessage("§c§lDragon §8» §eL'event à été arrêté !");
        for(Player player : Bukkit.getOnlinePlayers()) {
            FeatherBoardAPI.showScoreboard(player, "default");
        }
    }

    public static ArrayList<Player> getClassementPlayer() {
        return new ArrayList<>(sort(points).keySet());
    }

    private static HashMap sort(HashMap map) {
        List linkedlist = new LinkedList(map.entrySet());
        Collections.sort(linkedlist, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = linkedlist.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
}
