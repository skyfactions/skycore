package fr.floxiik.skycore.crew;

import fr.floxiik.skycore.dao.CrewDAO;
import org.bukkit.entity.Player;

import java.io.File;

public class CrewUpgrade {

    private static int fac_space = 256;
    private static int start_height = 64;
    private static File plot;
    private static File empty_plot;

    public static boolean upgrade(Player p) {
        CrewDAO crewDAO = new CrewDAO();
        Crew upgraded = crewDAO.getPlayerCrew(p);
        if(upgraded.isUpdate()) {
            return false;
        }
        int level = upgraded.getLevel() + 1;
        upgraded.setLevel(level);
        upgraded.setUpdate(true);
        crewDAO.updateCrew(upgraded);

        for (Crew c : Crews.getCrews()) {
            if (c.getFaction().equals(upgraded.getFaction())) {
                c.setLevel(level);
                c.setUpdate(true);
            }
        }
        return true;
    }
}
