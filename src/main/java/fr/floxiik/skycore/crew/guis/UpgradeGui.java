package fr.floxiik.skycore.crew.guis;

import fr.floxiik.skycore.crew.CrewUpgrade;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class UpgradeGui implements Listener {
    private final Inventory inv;

    public UpgradeGui(int lvl) {
        inv = Bukkit.createInventory(null, 45, "§f│ §c§lNiveaux de Faction");
        initializeItems(lvl);
    }

    // You can call this whenever you want to put the items in
    public void initializeItems(int lvl) {
        inv.setItem(8, createGuiItem(Material.BEDROCK, "§8§l» §e§lNiveau actuel§7: §c§l" + (lvl == -1 ? "Nul" : lvl) + " §8§l«", ""));
        if(lvl == 1)
            inv.setItem(20, createGuiItem(Material.IRON_INGOT, "§8§l» §a§lNiveau §e§l2 §8§l«", "","§7»§m                        §7«", "  §eCliquez pour", "  §eprocéder à", "  §el'§aamélioration §ede", "  §evotre §6Faction §e!", "§7»§m                        §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c200 000$§e.", "§f§l➢ §eAvoir au moins §c7 §emembres.", "§f§l➢ §eAvoir effectué §c35 §evotes. §7(total)"));
        else
            inv.setItem(20, createGuiItem(Material.BARRIER, "§8§l» §a§lNiveau §e§l2 §8§l«", "", "§7»§m                                §7«", "  §cAmélioration vérouillée.", lvl == -1 ? "  §eVous avez déjà procéder" : "  §eVous ne répondez pas à",  lvl == -1 ? "  §eà cette §6amélioration§e !" : "  §etoutes les conditions !", "§7»§m                                §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c200 000$§e.", "§f§l➢ §eAvoir au moins §c7 §emembres.", "§f§l➢ §eAvoir effectué §c35 §evotes. §7(total)"));
        if(lvl == 2)
            inv.setItem(22, createGuiItem(Material.DIAMOND, "§8§l» §a§lNiveau §6§l3 §8§l«", "", "§7»§m                        §7«", "  §eCliquez pour", "  §eprocéder à", "  §el'§aamélioration §ede", "  §evotre §6Faction §e!", "§7»§m                        §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c1 000 000$§e.", "§f§l➢ §eAvoir au moins §c15 §emembres.", "§f§l➢ §eAvoir effectué §c100 §evotes. §7(total)"));
        else
            inv.setItem(22, createGuiItem(Material.BARRIER, "§8§l» §a§lNiveau §6§l3 §8§l«", "", "§7»§m                                §7«", "  §cAmélioration vérouillée.", lvl == 3 ? "  §eVous avez déjà procéder" : "  §eVous ne répondez pas à",  lvl == 3 ? "  §eà cette §6amélioration§e !" : "  §etoutes les conditions !", "§7»§m                                §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c1 000 000$§e.", "§f§l➢ §eAvoir au moins §c15 §emembres.", "§f§l➢ §eAvoir effectué §c100 §evotes. §7(total)"));
        if(lvl == 3)
            inv.setItem(24, createGuiItem(Material.NETHER_STAR, "§8§l» §a§lNiveau §c§l4 §8§l«", "", "§7»§m                        §7«", "  §eCliquez pour", "  §eprocéder à", "  §el'§aamélioration §ede", "  §evotre §6Faction §e!", "§7»§m                        §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c8 000 000$§e.", "§f§l➢ §eAvoir au moins §c20 §emembres.", "§f§l➢ §eAvoir effectué §c250 §evotes. §7(total)"));
        else
            inv.setItem(24, createGuiItem(Material.BARRIER, "§8§l» §a§lNiveau §c§l4 §8§l«", "", "§7»§m                                §7«", "  §cAmélioration vérouillée.", lvl == 4 ? "  §eVous avez déjà procéder" : "  §eVous ne répondez pas à",  lvl == 4 ? "  §eà cette §6amélioration§e !" : "  §etoutes les conditions !", "§7»§m                                §7«", "",
                    "§7Pour avoir cette amélioration vous devez:"," ", "§f§l➢ §eAvoir une §cfaction§e.", "§f§l➢ §eAvoir §c8 000 000$§e.", "§f§l➢ §eAvoir au moins §c20 §emembres.", "§f§l➢ §eAvoir effectué §c250 §evotes. §7(total)"));

        ItemStack stack = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§r ");
        stack.setItemMeta(meta);
        for(int i = 0; i < 27; i++) {
            if(i != 20 && i != 22 && i != 24 && i != 8) {
                inv.setItem(i, stack);
            }
        }
    }

    // Nice little method to create a gui item with a custom name, and description
    protected ItemStack createGuiItem(final Material material, final String name, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        // Set the name of the item
        meta.setDisplayName(name);

        // Set the lore of the item
        meta.setLore(Arrays.asList(lore));

        item.setItemMeta(meta);

        return item;
    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        ent.openInventory(inv);
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        if (!e.getInventory().getTitle().equals("§0§lAméliorer votre faction:")) return;
        e.setCancelled(true);
        final ItemStack clickedItem = e.getCurrentItem();

        // verify current item is not null
        if (clickedItem == null || clickedItem.getType() == Material.STAINED_GLASS_PANE) return;

        final Player p = (Player) e.getWhoClicked();

        if (e.getCurrentItem().getType() != Material.BARRIER && e.getCurrentItem().getType() != Material.BEDROCK) {
            if(CrewUpgrade.upgrade(p)) {
                p.sendMessage("§c§lFaction §8» §eVous venez d'§aaméliorer §evotre faction au niveau §6supérieur§e !");
                p.sendMessage("§c§lFaction §8» §eL'amélioration sera comptabilisé à la prochaine phase de §aPause§e !");
            } else {
                p.sendMessage("§c§lFaction §8» §eVous avez déjà une amélioration en attente !");
            }
            p.closeInventory();
        }
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory() == inv) {
            e.setCancelled(true);
        }
    }
}