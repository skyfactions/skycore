package fr.floxiik.skycore.crew.guis;

import fr.floxiik.skycore.crew.CrewUpgrade;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class RankGui implements Listener {
    private final Inventory inv;
    private String target;

    public RankGui(String p) {
        inv = Bukkit.createInventory(null, 54, "§0§lRang pour: §9§l" + p);
        target = p.toLowerCase();
        initializeItems();
        System.err.println("init2 - " + target);
    }

    // You can call this whenever you want to put the items in
    public void initializeItems() {
        System.err.println("init - " + target);

        ItemStack stack = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 7);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§r ");
        stack.setItemMeta(meta);

        for(int i = 0; i<=9;i++) {
            inv.setItem(i, stack);
        }

        inv.setItem(10, createGuiItem(Material.COAL, "§8§l» §eGrade - §7§l§nPilote§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(11, createGuiItem(Material.BRICK, "§8§l» §eGrade - §a§l§nSergent§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(12, createGuiItem(Material.IRON_INGOT, "§8§l» §eGrade - §3§l§nMajor§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(13, stack);

        inv.setItem(14, createGuiItem(Material.PAINTING, "§8§l» §eGrade - §d§l§nGraphiste§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(15, createGuiItem(Material.FEATHER, "§8§l» §eGrade - §5§l§nRédacteur§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(16, createGuiItem(Material.BOOK, "§8§l» §eGrade - §a§l§nAssistant§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(17, stack);
        inv.setItem(18, stack);

        inv.setItem(19, createGuiItem(Material.GOLD_INGOT, "§8§l» §eGrade - §d§l§nAspirant§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(20, createGuiItem(Material.DIAMOND, "§8§l» §eGrade - §e§l§nLieutenant§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(21, createGuiItem(Material.EMERALD, "§8§l» §eGrade - §b§l§nCapitaine§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(22, stack);

        inv.setItem(23, createGuiItem(Material.COMPASS, "§8§l» §eGrade - §2§l§nModérateur§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(24, createGuiItem(Material.REDSTONE, "§8§l» §eGrade - §3§l§nDéveloppeur§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(25, createGuiItem(Material.QUARTZ_STAIRS, "§8§l» §eGrade - §e§l§nArchitecte§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(26, stack);
        inv.setItem(27, stack);

        inv.setItem(28, createGuiItem(Material.NETHER_STAR, "§8§l» §eGrade - §6§l§nAmiral§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(30, createGuiItem(Material.EYE_OF_ENDER, "§8§l» §eGrade - §f§l§nYoutu§c§l§nbeur§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(31, stack);

        inv.setItem(32, createGuiItem(Material.FIREBALL, "§8§l» §eGrade - §c§l§nResponsable§8§l «", "", "§eClique pour lui attribuer ce rang !"));
        inv.setItem(33, createGuiItem(Material.COMMAND, "§8§l» §eGrade - §4§l§nAdministrateur§8§l «", "", "§eClique pour lui attribuer ce rang !"));

        inv.setItem(35, stack);
        inv.setItem(36, stack);
        inv.setItem(40, stack);

        for(int i = 44; i<=53;i++) {
            inv.setItem(i, stack);
        }
    }

    // Nice little method to create a gui item with a custom name, and description
    protected ItemStack createGuiItem(final Material material, final String name, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        // Set the name of the item
        meta.setDisplayName(name);

        // Set the lore of the item
        meta.setLore(Arrays.asList(lore));

        item.setItemMeta(meta);

        return item;
    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        ent.openInventory(inv);
        System.err.println("open - " + target);
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        System.err.println("click - " + target);
        if (e.getInventory() == inv) return;
        e.setCancelled(true);
        final ItemStack clickedItem = e.getCurrentItem();

        // verify current item is not null
        if (clickedItem == null || clickedItem.getType() == Material.STAINED_GLASS_PANE) return;

        final Player p = (Player) e.getWhoClicked();

        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Pilote")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set default");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Sergent") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set sergent");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Major") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set major");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Aspirant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set aspirant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Lieutenant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set lieutenant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Capitaine") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set capitaine");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Amiral") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set amiral");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("You") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set youtubeur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Graphiste") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set graphiste");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Rédacteur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set rédacteur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Assistant") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set assistant");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Modérateur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set modérateur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Développeur") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set développeur");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Architecte") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set architecte");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Responsable") && e.getWhoClicked().hasPermission("skyfactions.responsable")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set responsable");
        } else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Administrateur") && e.getWhoClicked().hasPermission("skyfactions.admin")) {
            p.closeInventory();
            Bukkit.dispatchCommand(console, "lp user " + target + " parent set admin");
        } else {
            p.sendMessage("§c§lErreur §8» §eVous n'avez pas la permission !");
            p.closeInventory();
        }
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory() == inv) {
            e.setCancelled(true);
        }
    }
}