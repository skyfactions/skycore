package fr.floxiik.skycore.crew;

import com.massivecraft.factions.FPlayers;
        import org.bukkit.entity.Player;

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;

public class Crews {

    private static List<Crew> crews = new ArrayList<>();

    public static void addCrew(Crew crew) {
        crews.add(crew);
    }

    public static List<Crew> getCrews() {
        return crews;
    }
}
