package fr.floxiik.skycore.crew;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import fr.floxiik.skycore.dao.CrewDAO;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class CrewDemolition {

    private static int fac_space = 256;
    private static int start_height = 64;

    public static void destroy(String faction, Player p) {

        Location loc;

        CrewDAO crewDAO = new CrewDAO();
        Crew disbanded = crewDAO.getCrew(faction);
        crewDAO.deleteDyingCrew(disbanded);
        crewDAO.addDeadCrew(disbanded);
        Crews.getCrews().removeIf(c -> c.getFaction().equals(faction));


        World world = Bukkit.getWorld("faction_crew");

        Board board = Board.getInstance();

        Location wLoc = new Location(world, disbanded.getPositionX() * fac_space, start_height, disbanded.getPositionZ() * fac_space);
        FLocation wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(-32, 0, 16);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(0, 0, 16);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(+16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(+16, 0, -16);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);
        wLoc.add(0, 0, +16);
        wfLoc = new FLocation(wLoc);
        board.removeAt(wfLoc);

        Location spawn = new Location(Bukkit.getWorld("faction"), 4.5, 197.0, -2.5);
        spawn.setYaw(-180.0f);
        spawn.setPitch(0.0f);

        loc = new Location(world, disbanded.getPositionX() * fac_space + 16 + 7, start_height, disbanded.getPositionZ() * fac_space + 16 + 7);
        FLocation fLoc = new FLocation(loc);

        for (Entity entity : fLoc.getChunk().getEntities()) {
            if (entity instanceof Player) {
                entity.teleport(spawn);
                entity.sendMessage("§c§lInfo §8» §eLa §6faction sur§e la-quelle §etu §ete§e trouvais §es'est §efaite §6dissoute§e, la §epacerelle §esera §esupprimée §eau §eprochain §eredémarrage.");
            }
        }
    }
}
