package fr.floxiik.skycore.crew;

import com.boydti.fawe.object.schematic.Schematic;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.util.LazyLocation;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.blocks.BlockID;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.function.pattern.BlockPattern;
import com.sk89q.worldedit.math.transform.Transform;
import com.sk89q.worldedit.patterns.Pattern;
import com.sk89q.worldedit.regions.CuboidRegion;
import de.goldengamerzone.worldreset.WorldReset;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.tech.FactionData;
import fr.floxiik.skycore.worlds.Position;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class CrewsMoving {

    private static int fac_space = 256;
    private static World map_level;
    private static File plot;
    private static boolean moving = false;
    private static HashMap<String, ConcurrentHashMap<String, LazyLocation>> warps = new HashMap<>();

    public static boolean isMoving() {
        return moving;
    }

    public static void setMoving(boolean b) {
        moving = b;
    }

    public static void move() {
        setMoving(true);
        Date date = new Date();
        final long[] timeMilli = {System.currentTimeMillis()};
        startMoving();
        final long[] newtimeMilli = {System.currentTimeMillis()};
        Bukkit.broadcastMessage("Copiage des factions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms");
        DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099L).sendMessage("Copiage des factions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms").queue();
        timeMilli[0] = System.currentTimeMillis();
        giveNewIds();
        newtimeMilli[0] = System.currentTimeMillis();
        Bukkit.broadcastMessage("Attribution des nouvelles positions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms");
        DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099l).sendMessage("Attribution des nouvelles positions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms").queue();
        Bukkit.getServer().getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                timeMilli[0] = System.currentTimeMillis();
                regenWorlds();
                newtimeMilli[0] = System.currentTimeMillis();
                Bukkit.broadcastMessage("Regeneration des maps effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms");
                DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099L).sendMessage("Regeneration des maps effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms").queue();
            }
        }, 20 * 15);

        Bukkit.getServer().getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                timeMilli[0] = System.currentTimeMillis();
                try {
                    finishMoving();
                } catch (MaxChangedBlocksException e) {
                    e.printStackTrace();
                }
                newtimeMilli[0] = System.currentTimeMillis();
                Bukkit.broadcastMessage("Collage des factions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms");
                DiscordUtil.getJda().getGuildById(694665718536339536L).getTextChannelById(840961784303321099L).sendMessage("Collage des factions effectué en " + (newtimeMilli[0] - timeMilli[0]) + "ms").queue();
                SkyCore.getInstance().saveData();
                setMoving(false);
            }
        }, 20 * 30);
    }

    private static void startMoving() {
        Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                CrewDAO crewDAO = new CrewDAO();
                ArrayList<Crew> crews = crewDAO.getAliveCrews();
                warps.clear();
                for (Crew c : crews) {
                    String faction = c.getFaction();
                    int level = c.getLevel();
                    int posX = c.getPositionX();
                    int posZ = c.getPositionZ();

                    map_level = Bukkit.getWorld("faction_crew");

                    plot = new File(SkyCore.getInstance().getDataFolder() + "/crews_plot/", faction + ".schematic");
                    Vector bot = new Vector((posX * fac_space) + 16 + (level) * 16, 0, posZ * fac_space + 16); //MUST be a whole number eg integer
                    Vector top = new Vector(posX * fac_space + 16, 255, (posZ * fac_space) + 16 + (level) * 16); //MUST be a whole number eg integer
                    CuboidRegion region = new CuboidRegion(new BukkitWorld(map_level), bot, top);
                    Schematic schem = new Schematic(region);
                    try {
                        schem.save(plot, ClipboardFormat.SCHEMATIC);
                    } catch (IOException io) {
                        io.printStackTrace();
                    }

                    Faction fac = Factions.getInstance().getByTag(faction);
                    warps.put(faction, fac.getWarps());

                    Location locs = null;
                    for (int i = 0; i < level + 2; i++) {
                        for (int j = 0; j < level + 2; j++) {
                            locs = new Location(map_level, posX * fac_space + 7.5 + (i * 16), 64, posZ * fac_space + 7.5 + (j * 16));
                            FLocation fLoc = new FLocation(locs);
                            Bukkit.getScheduler().runTask(SkyCore.getInstance(), new Runnable() {
                                @Override
                                public void run() {
                                    Board.getInstance().setFactionAt(Factions.getInstance().getWilderness(), fLoc);
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    private static void giveNewIds() {
        Bukkit.getScheduler().runTaskAsynchronously(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                CrewDAO crewDAO = new CrewDAO();
                ArrayList<Crew> crews = crewDAO.getAliveCrews();
                Collections.shuffle(crews);
                for (Crew c : crews) {
                    c.setPlot(0);
                    crewDAO.updateCrew(c);
                }
                Crews.getCrews().clear();
                for (Crew c : crews) {
                    c.setPlot(crewDAO.getCrewNewPlot());
                    c.setPositionX(Position.getPositions().get(c.getPlot()).getPositionX());
                    c.setPositionZ(Position.getPositions().get(c.getPlot()).getPositionZ());
                    crewDAO.updateCrew(c);
                    Crews.getCrews().add(c);
                }
            }
        });
    }

    private static void regenWorlds() {
        WorldReset.resetWorld("faction_crew");
    }

    private static void finishMoving() throws MaxChangedBlocksException {
        CrewDAO crewDAO = new CrewDAO();
        ArrayList<Crew> crews = crewDAO.getAliveCrews();
        for (Crew c : crews) {
            int posX, posZ;
            int level = c.getLevel();
            String faction = c.getFaction();
            map_level = Bukkit.getWorld("faction_crew");

            posX = c.getPositionX() * fac_space;
            posZ = c.getPositionZ() * fac_space;

            /*FactionData fd = FactionData.getFactionData(Factions.getInstance().getByTag(faction).getFPlayerLeader());
            if (fd.getProtector().isValid()) {
                Location ploc = fd.getProtector().getLocation();
                Location newloc = new Location(map_level, (ploc.getX() % fac_space) + posX, ploc.getY(), (ploc.getZ() % fac_space) + posZ);
                fd.getProtector().setLocation(newloc);
            }
            for (Location fg : fd.getGenerators().keySet()) {
                Location newloc = new Location(map_level, (fg.getX() % fac_space) + posX, fg.getY(), (fg.getZ() % fac_space) + posZ);
                fd.getGenerators().get(fg).setLocation(newloc);
            }*/

            Faction fac = Factions.getInstance().getByTag(faction);

            Location locs = null;
            for (int i = 0; i < level + 2; i++) {
                for (int j = 0; j < level + 2; j++) {
                    locs = new Location(map_level, posX + 7.5 + (i * 16), 64, posZ + 7.5 + (j * 16));
                    FLocation fLoc = new FLocation(locs);
                    Board.getInstance().setFactionAt(Factions.getInstance().getSafeZone(), fLoc);
                }
            }
            for (int i = 0; i < level; i++) {
                for (int j = 0; j < level; j++) {
                    locs = new Location(map_level, posX + 16 + 7.5 + (i * 16), 64, posZ + 16 + 7.5 + (j * 16));
                    FLocation fLoc = new FLocation(locs);
                    Board.getInstance().setFactionAt(fac, fLoc);
                }
            }
            Location new_home = new Location(map_level, posX + 16 + 8 + (c.isUpdate() ? 8 : 0), 64, posZ + 16 + 8 + (c.isUpdate() ? 8 : 0));
            fac.setHome(new_home);

            if (warps.containsKey(faction)) {
                for (String s : warps.get(faction).keySet()) {
                    if (warps.get(faction).get(s).getWorldName().contains("crew")) {
                        Location loc = fac.getWarps().get(s).getLocation();
                        Location newloc = new Location(map_level, (loc.getX() % fac_space) + posX + (c.isUpdate() ? 8 : 0), loc.getY(), (loc.getZ() % fac_space) + posZ + (c.isUpdate() ? 8 : 0));
                        fac.setWarp(s, new LazyLocation(newloc));
                    }
                }
            }

            plot = new File(SkyCore.getInstance().getDataFolder() + "/crews_plot/", faction + ".schematic");
            try {
                EditSession editSession = ClipboardFormats.findByFile(plot).load(plot).paste(new BukkitWorld(map_level), new Vector(posX + 16 + (c.isUpdate() ? 8 : 0), 0, posZ + 16 + (c.isUpdate() ? 8 : 0)), false, true, (Transform) null);
                Vector bot;
                Vector top;
                CuboidRegion region;
                Set<BaseBlock> bb;
                EditSession edit = new EditSession(new BukkitWorld(map_level), -1);
                if (level == 1) {
                    bot = new Vector(posX + 16 * (level + 2) - 1, 0, posZ + 16 * (level + 2) - 1); //MUST be a whole number eg integer
                    top = new Vector(posX, 0, posZ); //MUST be a whole number eg integer
                    Vector center = Vector.getMidpoint(bot, top).add(0, 64, 0);
                    fac.setHome(new Location(map_level, center.getX(), center.getY(), center.getZ()));
                    fac.confirmValidHome();
                    region = new CuboidRegion(new BukkitWorld(map_level), bot, top);
                    edit.setBlocks(region, new BaseBlock(BlockID.BARRIER, -1));
                } else {
                    Bukkit.getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            Vector bot = new Vector(posX + 16 * (level + 2) - 1, 0, posZ); //MUST be a whole number eg integer
                            Vector top = new Vector(posX, 0, posZ + 16 * (level + 2) - 1); //MUST be a whole number eg integer
                            CuboidRegion region = new CuboidRegion(new BukkitWorld(map_level), bot, top);
                            try {
                                setBarrier(region);
                            } catch (MaxChangedBlocksException e) {
                                e.printStackTrace();
                            }
                        }
                    }, 20 * 15);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (c.isUpdate()) {
                c.setUpdate(false);
                Crews.getCrews().remove(c);
                Crews.getCrews().add(c);
                Bukkit.broadcastMessage("C'est mis en false bg");
            }
        }
    }

    public static void removeBedRock(CuboidRegion r) throws MaxChangedBlocksException {
        EditSession edit = new EditSession(new BukkitWorld(map_level), -1);
        Set<BaseBlock> bb = new HashSet<>();
        bb.add(new BaseBlock(BlockID.BEDROCK, -1));
        edit.replaceBlocks(r, bb, new BaseBlock(BlockID.AIR, -1));
    }

    public static void centerBedRock(CuboidRegion r) throws MaxChangedBlocksException {
        EditSession edit = new EditSession(new BukkitWorld(map_level), -1);
        edit.center(r, (Pattern) new BlockPattern(new BaseBlock(BlockID.BEDROCK, -1)));
    }

    public static void setBarrier(CuboidRegion r) throws MaxChangedBlocksException {
        EditSession edit = new EditSession(new BukkitWorld(map_level), -1);
        edit.setBlocks(r, new BaseBlock(BlockID.BARRIER, -1));
    }
}