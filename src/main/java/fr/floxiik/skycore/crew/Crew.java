package fr.floxiik.skycore.crew;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import fr.floxiik.skycore.tech.Generator;
import fr.floxiik.skycore.tech.Protector;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Crew {

    private int id;
    private int plot;
    private String faction;
    private int level;
    private int posX;
    private int posZ;
    private boolean die;
    private boolean update;
    private Protector protector;
    private HashMap<Location, Generator> generators;


    public Crew(int id, String faction, int level, int plot,  int posX, int posZ, boolean update, boolean die) {
        this.id = id;
        this.faction = faction;
        this.level = level;
        this.plot = plot;
        this.posX = posX;
        this.posZ = posZ;
        this.update = update;
        this.die = die;
    }

    /*public Crew(int globalId, int id, String faction, int level, int posX, int posZ, boolean die, boolean update, Protector protector, HashMap<Location, Generator> generators) {
        this.globalId = globalId;
        this.id = id;
        this.faction = faction;
        this.level = level;
        this.posX = posX;
        this.posZ = posZ;
        this.die = die;
        this.update = update;
        this.protector = protector;
        this.generators = generators;

        Crews.addCrew(this);
    }

    public void setProtector(Protector p) {
        this.protector = p;
    }

    public Protector getProtector() {
        return this.protector;
    }

    public void setGenerators(HashMap<Location, Generator> g) {
        this.generators = g;
    }

    public void addGenerator(Location l, Generator g) {
        this.generators.put(l,g);
    }

    public HashMap<Location, Generator> getGenerators() {
        return this.generators;
    }*/

    public int getId() {
        return this.id;
    }

    public void setId(int g) {
        this.id = g;
    }

    public int getPlot() {
        return this.plot;
    }

    public void setPlot(int i) {
        this.plot = i;
    }

    public String getFaction() {
        return this.faction;
    }

    public void setFaction(String f) {
        this.faction = f;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public int getPositionX() {
        return this.posX;
    }

    public int getPositionZ() {
        return this.posZ;
    }

    public void setPositionX(int p) {
        this.posX = p;
    }

    public void setPositionZ(int p) {
        this.posZ = p;
    }

    public boolean isDie() {
        return this.die;
    }

    public boolean isUpdate() {
        return this.update;
    }

    public void setDie(boolean d) {
        this.die = d;
    }
    
    public void setUpdate(boolean u) {
        this.update = u;
    }

    public void print() {
        System.out.println(id + " - " + faction + " - " + level + " - " + plot + " - " + posX + " - " + posZ + " - " + die + " - " + update);
    }
}