package fr.floxiik.skycore.crew;

import com.benzimmer123.facshields.FactionShields;
import com.benzimmer123.facshields.api.objects.Shield;
import com.benzimmer123.facshields.data.ShieldData;
import com.benzimmer123.facshields.managers.ShieldManager;
import com.benzimmer123.facshields.obj.serial.MemoryShield;
import com.massivecraft.factions.*;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.blocks.BlockID;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.ClipboardFormats;
import com.sk89q.worldedit.math.transform.Transform;
import com.sk89q.worldedit.regions.CuboidRegion;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.dao.CrewDAO;
import fr.floxiik.skycore.help.PlayerHelp;
import fr.floxiik.skycore.worlds.Position;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class CrewCreation {

    private static int fac_space = 256;
    private static int start_height = 64;
    private static File plot;
    private static File empty_plot;

    public static void init(String faction, Player p) throws MaxChangedBlocksException, NoSuchFieldException, IllegalAccessException {

        Location loc;

        PlayerHelp.startIslandGeneration(p);

        CrewDAO crewDAO = new CrewDAO();

        //Création de l'objet et initialisation du crew
        int plotId = crewDAO.getCrewNewPlot();

        Crew crew = new Crew(crewDAO.getCrewNewId(), faction, 1, plotId, Position.getPositions().get(plotId).getPositionX(), Position.getPositions().get(plotId).getPositionZ(), false, false);

        crewDAO.addCrew(crew);
        Crews.addCrew(crew);

        int pastePosX = crew.getPositionX() * fac_space;
        int pastePosZ = crew.getPositionZ() * fac_space;

        //Génération de la pacerelle

        World level1 = Bukkit.getWorld("faction_crew");
        plot = new File(SkyCore.getInstance().getDataFolder(), "empty_plot.schematic");
        try {
            EditSession editSession = ClipboardFormats.findByFile(plot).load(plot).paste(new BukkitWorld(level1), new Vector(pastePosX + 16, start_height, pastePosZ + 16), false, true, (Transform) null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        plot = new File(SkyCore.getInstance().getDataFolder(), "plot.schematic");
        try {
            EditSession editSession = ClipboardFormats.findByFile(plot).load(plot).paste(new BukkitWorld(level1), new Vector(pastePosX + 16, start_height, pastePosZ + 16), false, false, (Transform) null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Faction fac = FPlayers.getInstance().getByPlayer(p).getFaction();
        Board board = Board.getInstance();

        loc = new Location(level1, pastePosX + 16 + 8, start_height, pastePosZ + 16 + 8);

        FLocation fLoc = new FLocation(loc);
        board.setFactionAt(fac, fLoc);

        Faction safeZone = Factions.getInstance().getSafeZone();
        Location wLoc = new Location(level1, pastePosX + 8, start_height, pastePosZ + 8);
        FLocation wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(-32, 0, 16);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(0, 0, 16);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(+16, 0, 0);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(+16, 0, -16);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);
        wLoc.add(0, 0, +16);
        wfLoc = new FLocation(wLoc);
        board.setFactionAt(safeZone, wfLoc);

        fac.setHome(loc);
        fac.confirmValidHome();

        EditSession edit = new EditSession(new BukkitWorld(level1), -1);
        Vector bot = new Vector(pastePosX + 16 * (1 + 2) - 1, 0, pastePosZ + 16 * (1 + 2) - 1); //MUST be a whole number eg integer
        Vector top = new Vector(pastePosX, 0, pastePosZ); //MUST be a whole number eg integer
        Vector center = Vector.getMidpoint(bot, top).add(0, 64, 0);
        fac.setHome(new Location(level1, center.getX(), center.getY(), center.getZ()));
        fac.confirmValidHome();
        CuboidRegion region = new CuboidRegion(new BukkitWorld(level1), bot, top);
        edit.setBlocks(region, new BaseBlock(BlockID.BARRIER, -1));

        new ShieldManager(FactionShields.getInstance()).chargePlayer(p, 0, "1:00", "9:00");

        Bukkit.getServer().getScheduler().runTaskLater(SkyCore.getInstance(), new Runnable() {
            @Override
            public void run() {
                if(FPlayers.getInstance().getByPlayer(p).hasFaction()) {
                    p.teleport(loc);
                    PlayerHelp.startManual(p);

                    LocalDateTime ldt = LocalDateTime.now();
                    for(Shield s : ShieldData.getInstance().getShields()) {
                        if(s.getFactionID().equals(Factions.getInstance().getByTag(faction).getId())) {
                            MemoryShield shield = (MemoryShield) s;
                            Class shieldClass = shield.getClass();
                            Field endTime = null;
                            Field startTime = null;
                            try {
                                startTime = shieldClass.getDeclaredField("startTime");
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            }
                            startTime.setAccessible(true);
                            try {
                                startTime.set(s, ZonedDateTime.of(ldt, ZoneId.of("Europe/Paris")));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            startTime.setAccessible(false);

                            ldt = ldt.plusWeeks(1);
                            try {
                                endTime = shieldClass.getDeclaredField("endTime");
                            } catch (NoSuchFieldException e) {
                                e.printStackTrace();
                            }
                            endTime.setAccessible(true);
                            try {
                                endTime.set(s, ZonedDateTime.of(ldt, ZoneId.of("Europe/Paris")));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            endTime.setAccessible(false);
                        }
                    }
                }
            }
        }, 20 * 5);
    }
}
