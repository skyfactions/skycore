package fr.floxiik.skycore.drop;

import com.Zrips.CMI.CMI;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.utils.RandomGenerator;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class Drop extends BukkitRunnable {

    private Location location;
    private ArmorStand as = null;
    private Location lastLocation;

    public Drop(Location location) {
        this.location = location;
        this.runTaskTimer(SkyCore.getInstance(), 10, 5);
    }

    @Override
    public void run() {
        if(!DropChests.dropsLoc.contains(location)) {
            as = (ArmorStand) Bukkit.getWorld("faction_warzone").spawn(location, ArmorStand.class);
            as.setGravity(true);
            as.setVisible(false);
            as.setHelmet(new ItemStack(Material.CHEST));
            DropChests.dropsLoc.add(location);
            DropChests.waitDropsLoc.remove(location);
        } else {
            if(as.getLocation().add(0, -0.5, 0).getBlock().getY() == location.getWorld().getHighestBlockAt(as.getLocation()).getY()) {
                location.getWorld().playEffect(as.getLocation(), Effect.EXPLOSION_LARGE, 15);
                as.getLocation().getBlock().setType(Material.CHEST);
                for (BlockState b : as.getLocation().getChunk().getTileEntities()) {
                    if (b instanceof Chest) {
                        Chest chest = (Chest) b;
                        Inventory inv = chest.getBlockInventory();
                        int max = RandomGenerator.between(4, 10); //item count
                        for(int j=0; j < max; j++){
                            inv.setItem(RandomGenerator.between(0, 26), new ItemStack(getItems().get(RandomGenerator.between(0, getItems().size()-1))));
                        }
                    }
                }
                lastLocation = as.getLocation();
                as.remove();
                DropChests.dropsLoc.remove(location);
                this.cancel();
            }
        }
    }

    public void remove() {
        lastLocation.getBlock().setType(Material.AIR);
    }

    public ArrayList<ItemStack> getItems() {
        ArrayList<ItemStack> items = new ArrayList<>();

        ItemStack a = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta b = a.getItemMeta();
        b.addEnchant(Enchantment.FIRE_ASPECT, RandomGenerator.between(1, 2), true);
        b.addEnchant(Enchantment.DAMAGE_ALL, RandomGenerator.between(2, 5), true);
        a.setItemMeta(b);
        items.add(a);

        a = new ItemStack(Material.DIAMOND_HELMET, 1);
        b = a.getItemMeta();
        b.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, RandomGenerator.between(1, 4), true);
        b.addEnchant(Enchantment.DURABILITY, RandomGenerator.between(1, 3), true);
        a.setItemMeta(b);
        items.add(a);

        a = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        b = a.getItemMeta();
        b.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, RandomGenerator.between(1, 4), true);
        b.addEnchant(Enchantment.DURABILITY, RandomGenerator.between(1, 3), true);
        a.setItemMeta(b);
        items.add(a);

        a = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
        b = a.getItemMeta();
        b.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, RandomGenerator.between(1, 4), true);
        b.addEnchant(Enchantment.DURABILITY, RandomGenerator.between(1, 3), true);
        a.setItemMeta(b);
        items.add(a);

        a = new ItemStack(Material.DIAMOND_BOOTS, 1);
        b = a.getItemMeta();
        b.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, RandomGenerator.between(1, 4), true);
        b.addEnchant(Enchantment.DURABILITY, RandomGenerator.between(1, 3), true);
        a.setItemMeta(b);
        items.add(a);

        a = new ItemStack(Material.BOW, 1);
        b = a.getItemMeta();
        b.addEnchant(Enchantment.ARROW_DAMAGE, RandomGenerator.between(1, 5), true);
        b.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        a.setItemMeta(b);

        items.add(new ItemStack(Material.GOLDEN_APPLE, RandomGenerator.between(3, 15)));
        items.add(new ItemStack(Material.ENDER_PEARL, RandomGenerator.between(1, 16)));
        items.add(new ItemStack(Material.OBSIDIAN, RandomGenerator.between(1, 64)));
        items.add(new ItemStack(Material.IRON_INGOT, RandomGenerator.between(1, 20)));
        items.add(new ItemStack(Material.GOLD_INGOT, RandomGenerator.between(1, 15)));
        items.add(new ItemStack(Material.DIAMOND, RandomGenerator.between(1, 10)));

        return items;
    }
}
