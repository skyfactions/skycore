package fr.floxiik.skycore.drop;

import com.Zrips.CMI.CMI;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import fr.floxiik.skycore.SkyCore;
import fr.floxiik.skycore.utils.RandomGenerator;
import net.minecraft.server.v1_8_R3.EntityFallingBlock;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class DropChests implements Listener {

    private static Set<Chunk> chunks = new HashSet<>();
    public static ArrayList<Drop> drops = new ArrayList<>();
    public static ArrayList<Location> dropsLoc = new ArrayList<>();
    public static ArrayList<Location> waitDropsLoc = new ArrayList<>();
    private static World warzone = Bukkit.getWorld("faction_warzone");
    private static int max = 238, min = -268;

    public static void spawnDrops(int i) {
        Location chest;
        FLocation isWar;
        for (int j = 0; j < i; j++) {
            chest = new Location(warzone, RandomGenerator.between(min, max), 250, RandomGenerator.between(min, max));
            isWar = new FLocation(chest);
            while (!Board.getInstance().getFactionAt(isWar).isWarZone()) {
                chest = new Location(warzone, RandomGenerator.between(min, max), 250, RandomGenerator.between(min, max));
                isWar = new FLocation(chest);
            }

            Bukkit.broadcastMessage("§c§lLargage §8» §eUn §alargage §es'effectue en X:§6" + chest.getX() + "§e Z:§6" + chest.getZ() + "§e dans la §dWarZone§e !");

            chunks.add(chest.getChunk());
            waitDropsLoc.add(chest);
        }
    }

    public static void spawnDrop(Location loc) {
        Drop drop = new Drop(loc);
        drops.add(drop);
    }

    public static void addDrop(Location d) {
        dropsLoc.add(d);
    }

    public static ArrayList<Location> getDrops() {
        return dropsLoc;
    }

    public static void removeAllDrops() {
        for (Drop d : drops) {
            d.remove();
        }
    }

    @EventHandler
    public void fez(PlayerMoveEvent event) {
        if(event.getTo().getWorld().getName().equals("faction_warzone")) {
            if(chunks.contains(event.getTo().getChunk())) {
                for (Location loc : waitDropsLoc) {
                    if (loc.getChunk() == event.getTo().getChunk()) {
                        spawnDrop(loc);
                    }
                }
                chunks.remove(event.getTo().getChunk());
            }
        }
    }
}
